# ShadowChess

[ShadowChess](https://shadow9876.itch.io/shadowchess) is a chess variant made in the Godot Game Engine (4.3); it also uses the [Rust bindings for Godot](https://godot-rust.github.io/).

Building this derivation is slightly more straightforward:

## Prerequisites:
1. [Install Rust](https://www.rust-lang.org/tools/install)
2. If you're on Windows you may need to [install some more prerequisites](https://rust-lang.github.io/rustup/installation/windows.html)
3. [Download Godot 4.3](https://godotengine.org/download/archive/4.3-stable/)
4. [Install Blender](https://www.blender.org/)
5. Music (optional): Install [Musescore](https://musescore.org/) with [Muse Hub/Muse Sounds](https://musescore.org/en/download) and [Audacity](https://www.audacityteam.org/)
6. Maybe [VLC](https://www.videolan.org/) can be useful for Videos in Ogg Theora format

## Build and Setup:

1. Navigate to the `ShadowChess/rust` folder
2. Run `cargo build --target <your build target>` to build the engine in debug mode
3. Run `cargo build --target <your build target> --release` to build the engine in release mode
4. Open Godot and set Blender path (`"Editor/Editor Settings/FileSystem/Import" -> Blender."Blender 3 path"`) to your Blender (`whereis blender`) 
5. Now enable Blender (`"Project/Project Settings/FileSystem/Import" -> Blender.Enabled = true`)
6. Restart Godot

## Cross Compilation
1. Download either [Podman](https://podman.io/) or [Docker](https://www.docker.com/)
2. Navigate to the `ShadowChess/rust` folder
3. Run `./build.sh`
4. Answer `y`, `n`, `y` ... and `n`

## Web Build
1. Run the following command(s)
```
git clone https://github.com/emscripten-core/emsdk.git
cd emsdk
./emsdk install 3.1.66
./emsdk activate 3.1.66
source emsdk_env.sh   # or perhaps emsdk_env.bat on Windows
```
2. Navigate to the `ShadowChess/rust` folder
3. Run `./build.sh`
4. Answer `y`, `n`, `n` and `y`

## Valid Build Targets:

- **Windows**: `x86_64-pc-windows-gnu`
- **Linux**: `x86_64-unknown-linux-gnu`, `aarch64-unknown-linux-gnu`
- MacOS: `x86_64-apple-darwin`, `aarch64-apple-darwin`
- Android: `aarch64-linux-android`, `x86_64-linux-android`
- iOS: `aarch64-apple-ios`, `x86_64-apple-ios`
- **Web**: `wasm32-unknown-emscripten`

Currently the engine is only built for **Windows**, **Linux** and the **Web**.

## License:

All code is licensed under the [GPL-3.0-only](https://www.gnu.org/licenses/gpl-3.0.en.html), while all assets (which are NOT present in any `sources.md` files) are licensed under the [CC-BY-SA-4.0](https://creativecommons.org/licenses/by-sa/4.0/) license.

## Modding/Committing Guidelines:

Images should be in `.png`, `.jpg/.jpeg` or `.webp` format and should not include any metadata (use a [metadata cleaner](https://flathub.org/apps/fr.romainvigier.MetadataCleaner)). For 3D models [Blender](https://www.blender.org/) should be prioritised. Music should be in either `.ogg` or `.mp3` format.

Note: Don't use shaders in two different editors at the same time; don't reset shader with git, when it's open in Godot! These might BREAK the game! (If the aforementioned event should strike, simply delete the project and copy a new one from [Codeberg](https://codeberg.org/Shadow9876/ShadowChess); though first you could save your progress)
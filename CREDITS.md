Assets Used in ShadowChess:
- `ShadowChess/godot/imports/chess/fish/`
    - [white_pawn](https://pixnio.com/fauna-animals/fishes/carp-fish); author: Duane Raver, USFWS; under [CC0](https://creativecommons.org/public-domain/cc0/)
    - [white_knight](https://commons.wikimedia.org/wiki/File:Protopterus_annectens_Aquarium_Li%C3%A8ge_30012016_1.jpg); author: Vassil; under [CC0-1.0](https://creativecommons.org/publicdomain/zero/1.0/)
    - [white_bishop](https://www.flickr.com/photos/91501748@N07/26127365246/); author: Mathias Appel; under [CC0-1.0](https://creativecommons.org/publicdomain/zero/1.0/)
    - [white_rook](https://pixnio.com/fauna-animals/seals-and-sea-lions/seal-pup-cute-marine-mammal); author: USFWS (U.S. Fish and Wildlife Service); under [CC0](https://creativecommons.org/public-domain/cc0/)
    - [white_queen](https://commons.wikimedia.org/wiki/File:Horned_Narval;_or,_Sea_Unicorn.jpg); author: Harrison, Ause, & Co.; in public domain where the copyright term is the author's life plus 100 years or fewer
    - [white_king](https://commons.wikimedia.org/wiki/File:Blue_catfish_tenn_aquarium.JPG); author: Thomsonmg2000; under [CC0-1.0](https://creativecommons.org/publicdomain/zero/1.0/)
- `ShadowChess/godot/imports/graveyard/memorial/`
    - [Tree Interaction and great_tree's background](https://pixnio.com/nature-landscapes/field/green-fields-of-grain); author: Unknown author; under [CC0](https://creativecommons.org/public-domain/cc0/)
- `ShadowChess/godot/imports/graveyard/images/`
    - [Texture for all walls](https://pixnio.com/textures-and-patterns/rock-stone-texture/stone-texture-on-wall); author: Unknown author; under [CC0](https://creativecommons.org/public-domain/cc0/)
- `ShadowChess/godot/imports/maps/iberia/mesh/`
    - [height.jpg](https://tangrams.github.io/heightmapper/); license is unclear
- `ShadowChess/godot/imports/ui/loading_screens/`
    - [Shrine, Dark Shrine and Splash's background](https://es.wikipedia.org/wiki/Archivo:Calatrava_Vieja_2.jpg); author: Mareve; under [CC-BY-SA-2.5](https://creativecommons.org/licenses/by-sa/2.5/)
- `ShadowChess/godot/imports/ui/main_menu/`
    - [Game Difficulty and Game Difficulty Deselect's background](https://pixnio.com/nature-landscapes/waterfalls/waterfalls-background); author: Jon Sullivan; under [CC0](https://creativecommons.org/public-domain/cc0/)
    - [Real Life Difficulty and Real Life Difficulty Deselect's background](https://pixnio.com/nature-landscapes/canyon/bryce-canyon-at-sunrise); author: Jon Sullivan; under [CC0](https://creativecommons.org/public-domain/cc0/)
    - [Credits](https://en.wikipedia.org/wiki/File:Copyright.svg); author: Unknown author; under [CC0](https://creativecommons.org/public-domain/cc0/)
    - **Information.svg** is a derivation of [Copyright.svg](https://en.wikipedia.org/wiki/File:Copyright.svg); author: Unknown author; under [CC0](https://creativecommons.org/public-domain/cc0/)
- `ShadowChess/godot/imports/chess/2D/`
    - [All Images](https://www.wpclipart.com/recreation/games/chess/chess_set_1/); author: ???; in public domain

Links can also be found in the respective `path/sources.md` files

Software Used:
- [Godot](https://godotengine.org/) - Game Engine
- [Blender](https://www.blender.org/) - 3D Models
- [Gimp](https://www.gimp.org/) - Image Editing
- [Inkscape](https://inkscape.org/) - Vector Graphics
- [Metadata Cleaner](https://flathub.org/apps/fr.romainvigier.MetadataCleaner) - Removing Metadata from Images
- [Musescore](https://musescore.org/) - Music
- [Audacity](https://www.audacityteam.org/) - Volume Normalisation
- [VsCodium](https://vscodium.com/) - Code Editor
- [Podman](https://podman.io/) - Virtualisation software; used in the cross compilation of ShadowChess
- [Rust](https://www.rust-lang.org/) - Complementary Programming Language
- [Rust Analyzer](https://rust-analyzer.github.io/) - Rust Extension
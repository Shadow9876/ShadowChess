#!/usr/bin/bash
read -p "First run (y/N): " first_response
read -p "Build in release? (y/N): " release_response

# Cross Build
read -p "Cross build? (Y/n): " cross_response
if [[ "$cross_response" != "n" && "$cross_response" != "N" ]]; then

    if [[ "$first_response" == "y" || "$first_response" == "Y" ]]; then
        cargo install cross
    fi;

    targets=(x86_64-unknown-linux-gnu aarch64-unknown-linux-gnu x86_64-pc-windows-gnu)

    for target in "${targets[@]}"
    do
        if [[ "$first_response" == "y" || "$first_response" == "Y" ]]; then
            rustup target add $target
        fi;
        
        if [[ "$release_response" == "y" || "$release_response" == "Y" ]]; then
            cross build --target $target --release
        else 
            cross build --target $target
        fi;
    done

fi;

# Web Build
read -p "Build for the Web? (y/N): " web_response

if [[ "$web_response" == "y" || "$web_response" == "Y" ]]; then

    if [[ "$first_response" == "y" || "$first_response" == "Y" ]]; then
        rustup target add wasm32-unknown-emscripten
        rustup toolchain install nightly
        mkdir target/wasm32-unknown-emscripten/debug
    fi;

    cargo +nightly build -Zbuild-std --target wasm32-unknown-emscripten --release

    if [[ "$release_response" != "y" && "$release_response" != "Y" ]]; then
        cp target/wasm32-unknown-emscripten/release/extension.wasm target/wasm32-unknown-emscripten/debug/extension.wasm
    fi;

fi;
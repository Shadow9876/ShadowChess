macro_rules! gen_chess_pieces {
    ($($name:tt : $chr:literal & $val:tt),*) => {
        $(
            pub const $name: (u8,i8)=($chr,$val);
        )*
    };
}

gen_chess_pieces!(
    PAWN : b'p' & 10,
    KNIGHT : b'n' & 30,
    BISHOP : b'b' & 35,
    ROOK : b'r' & 50,
    QUEEN : b'q' & 90,
    KING : b'k' & 15,
    MONSTER : b'm' & 99,
    EMPTY : b'x' & 0
);

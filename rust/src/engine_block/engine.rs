use rand::{seq::SliceRandom, thread_rng};

use super::board::{Board,Move};

impl Board {
    pub fn alpha_beta(&mut self, mut alpha: i32, mut beta: i32, depth: u8) -> i32 {
        if depth==0 { return self.eval() }
        let turn=self.player.1[self.player.0];

        let mut moves=self.movegen();
        moves.sort();
        
        for m in moves {
            let res=self.forward_move(&m);
            //Mate optimisation
            if turn && res.white_won() || !turn && res.black_won() {
                self.backward_move(m, res.promoted());
                return if turn {99999} else {-99999};
            }
            let score=if res.white_won() { 99999 }
            else if res.black_won() { -99999 }
            else { 
                self.alpha_beta(alpha, beta, depth-1)
            };
            self.backward_move(m, res.promoted());
            
            if turn {
                if score>=beta { return beta; }
                if score>alpha { alpha=score; }
            }
            else {
                if score<=alpha { return alpha; }
                if score<beta { beta=score; }
            }
            
        }
        if turn { alpha } else { beta }
    }
    pub fn get_best_move(&mut self, depth: u8) -> Option<Move> {
        let mut moves=self.movegen();
        if depth==0 { return Some(moves.choose(&mut thread_rng()).unwrap().clone()) }

        moves.sort();

        let turn=self.player.1[self.player.0];

        let mut best_move=None;
        let mut best_score=if turn {i32::MIN} else {i32::MAX};
        
        for m in moves {
            let res=self.forward_move(&m);
            if turn && res.white_won() || !turn && res.black_won() {
                self.backward_move(m.clone(), res.promoted());
                return Some(m);
            }
            let score=if res.white_won() { 99999 }
            else if res.black_won() { -99999 }
            else { 
                let mut res: i32=1000000;
                let mut alpha=i32::MIN;
                let mut beta=i32::MAX;
                for d in 1..=depth-1 {
                    loop {
                        res=self.alpha_beta(alpha, beta,d);
                        if res.abs()>=1000000 {
                            alpha-=10000;
                            beta+=10000;
                        }
                        else { break }
                    }
                    alpha=res-1000;
                    beta=res+1000;
                }
                res
            };
            self.backward_move(m.clone(), res.promoted());
            if turn && score==99999 || !turn && score==-99999 {
                return Some(m)
            }
            if turn && score>best_score || !turn && score<best_score {
                best_score=score;
                best_move=Some(m);
            }
        }
        best_move
    }
    pub fn engine_prober_alpha_beta(&mut self, mut alpha: i32, mut beta: i32, depth: u8, prober_colour: bool, engine_depth: u8) -> i32 {
        if depth==0 { return self.eval() }
        let turn=self.player.1[self.player.0];

        let moves=
        if turn==prober_colour {
            let mut m=self.movegen();
            m.sort();
            m
        }
        else {
            let m=self.get_best_move(depth);
            match m {
                Some(mv) => { vec![mv] }
                None => { vec![] }
            }
        };
        for m in moves {
            let res=self.forward_move(&m);
            //Mate optimisation
            if turn && res.white_won() || !turn && res.black_won() {
                self.backward_move(m, res.promoted());
                return if turn {99999} else {-99999};
            }
            let score=if res.white_won() { 99999 }
            else if res.black_won() { -99999 }
            else { 
                self.engine_prober_alpha_beta(alpha, beta, depth-1, prober_colour, engine_depth)
            };
            self.backward_move(m, res.promoted());
            
            if turn {
                if score>=beta { return beta; }
                if score>alpha { alpha=score; }
            }
            else {
                if score<=alpha { return alpha; }
                if score<beta { beta=score; }
            }
        }
        if turn { alpha } else { beta }
    }
    pub fn engine_prober(&mut self, depth: u8, engine_depth: u8) -> Option<Move> {
        let mut moves=self.movegen();
        if depth==0 { return Some(moves.choose(&mut thread_rng()).unwrap().clone()) }

        moves.sort();

        let turn=self.player.1[self.player.0];

        let mut best_move=None;
        let mut best_score=if turn {i32::MIN} else {i32::MAX};
        
        for m in moves {
            let res=self.forward_move(&m);
            if turn && res.white_won() || !turn && res.black_won() {
                self.backward_move(m.clone(), res.promoted());
                return Some(m);
            }
            let score=if res.white_won() { 99999 }
            else if res.black_won() { -99999 }
            else { 
                let mut res: i32=1000000;
                let mut alpha=i32::MIN;
                let mut beta=i32::MAX;
                for d in 1..=depth-1 {
                    loop {
                        res=self.engine_prober_alpha_beta(alpha, beta,d,turn, engine_depth);
                        if res.abs()>=1000000 {
                            alpha-=10000;
                            beta+=10000;
                        }
                        else { break }
                    }
                    alpha=res-1000;
                    beta=res+1000;
                }
                res
            };
            self.backward_move(m.clone(), res.promoted());
            if turn && score==99999 || !turn && score==-99999 {
                return Some(m)
            }
            if turn && score>best_score || !turn && score<best_score {
                best_score=score;
                best_move=Some(m);
            }
        }
        best_move
    }
}
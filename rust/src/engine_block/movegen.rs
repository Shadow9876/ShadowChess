#![allow(dead_code)]
use crate::move_construct;

use super::{board::{abs, Board, Move}, piece::*};


const fn add(n: usize,m: i8) -> usize {
    (n as i8+m) as usize
}
const fn add8(n: usize,m: i8) -> i8 {
    n as i8+m
}
/// Add `n` and `m` together and check whether the sum is a valid
/// position on the board. (>=0 && <upper_bound)
/// 
/// ## Examples:
/// ```
/// let n=6usize;
/// let m=1i8;
/// assert!(in_add(8, 6, 1));
/// ```
const fn in_add(upper_bound: i8, n: usize, m: i8) -> bool {
    let val=add8(n,m);
    val>=0 && val<upper_bound
}

impl Board {
    fn pawn(&self, pos: (usize,usize), moves: &mut Vec<Move>) {
        let (i,j)=pos;
        let turn = self.player.1[self.player.0];
        let sign = if turn {1} else {-1i8};

        let base_rank=if turn {1} else {self.len()-2};
        let en_passant_rank=if turn {self.len()-4} else {3};

        let promotions=[
            KNIGHT.1,
            BISHOP.1,
            ROOK.1,
            QUEEN.1,
            KING.1
        ];
        
        //Move direction is sign
        //Normal moves
        if in_add(self.len8(),i,sign) && self[(add(i,sign),j)]==0
        {
            //Normal move
            if i!=self.len()-1-base_rank {
                moves.push(move_construct!(i,j,add(i,sign),j,self[pos],0));
            }
            //Promotion with moving
            else {
                for prom in &promotions {
                    moves.push(move_construct!(i,j,add(i,sign),j,sign*prom,0));
                }
            }
            //Double moves
            if i==base_rank && add8(i,2*sign)>=0 && add(i,2*sign)<self.len() &&
            self[(add(i,2*sign),j)]==0 {
                moves.push(move_construct!(i,j,add(i,2*sign),j,self[pos],0));
            }
        }
        //Captures
        for y in [-1,1i8] {
            //Capture
            if in_add(self.len8(),j,y) && in_add(self.len8(),i,sign) && 
            self[(add(i,sign),add(j,y))].signum()==-sign
            {
                //Normal capture
                if i!=self.len()-1-base_rank {
                    moves.push(move_construct!(i,j,add(i,sign),add(j,y),self[pos],self[(add(i,sign),add(j,y))]));
                }
                //Capture with promotion
                else {
                    for prom in &promotions {
                        moves.push(move_construct!(i,j,add(i,sign),add(j,y),sign*prom,self[(add(i,sign),add(j,y))]));
                    }
                }
            }
            //En passant
            if i==en_passant_rank && in_add(self.len8(),j,y) &&
            self[(i,add(j,y))]==-sign*PAWN.1 &&
            self[(add(i,sign),add(j,y))]==0 {
                moves.push(move_construct!(i,j,add(i,sign),add(j,y),self[pos],0));
            }
        }
        
    }
    fn knight(&self, pos: (usize,usize), moves: &mut Vec<Move>) {
        let turn = self.player.1[self.player.0];
        let sign = if turn {1} else {-1i8};
        let (i,j)=pos;
        for x in [-1,1i8] {
            for y in [-2,2i8] {
                if in_add(self.len8(),i, x) && in_add(self.len8(),j, y) &&
                sign!=self[(add(i,x),add(j,y))].signum() {
                    moves.push(move_construct!(i,j,add(i,x),add(j,y),self[pos],self[(add(i,x),add(j,y))]));
                }
                if in_add(self.len8(),i, y) && in_add(self.len8(),j, x) &&
                sign!=self[(add(i,y),add(j,x))].signum() {
                    moves.push(move_construct!(i,j,add(i,y),add(j,x),self[pos],self[(add(i,y),add(j,x))]));
                }
            }
        }
        
    }
    fn bishop(&self, pos: (usize,usize), moves: &mut Vec<Move>) {
        let turn = self.player.1[self.player.0];
        let sign = if turn {1} else {-1i8};
        let (i,j)=pos;

        for x in [-1,1i8] {
            for y in [-1,1i8] {
                for l in 1..self.len() {
                    let lx=l as i8*x;
                    let ly=l as i8*y;
                    if in_add(self.len8(),i,lx) && in_add(self.len8(),j, ly)
                    && self[(add(i,lx),add(j,ly))].signum()!=sign {
                        moves.push(move_construct!(i,j,add(i,lx),add(j,ly),self[pos],self[(add(i,lx),add(j,ly))]));
                        if (self[(add(i,lx),add(j,ly))])!=0 { break }
                    }
                    else { break }
                }
            }
        }
        
    }
    fn rook(&self, pos: (usize,usize), moves: &mut Vec<Move>) {
        let turn = self.player.1[self.player.0];
        let sign = if turn {1} else {-1i8};
        let (i,j)=pos;
        
        for x in [-1,1i8] {
            //Vertical movement
            for l in 1..self.len() {
                let lx=l as i8*x;
                if in_add(self.len8(),i,lx) && self[(add(i,lx),j)].signum()!=sign {
                    moves.push(move_construct!(i,j,add(i,lx),j,self[pos],self[(add(i,lx),j)]));
                    if self[(add(i,lx),j)]!=0 { break }
                }
                else { break }
            }
            //Horizontal movement
            for l in 1..self.len() {
                let lx=l as i8*x;
                if in_add(self.len8(),j,lx) && self[(i,add(j,lx))].signum()!=sign {
                    moves.push(move_construct!(i,j,i,add(j,lx),self[pos],self[(i,add(j,lx))]));
                    if self[(i,add(j,lx))]!=0 { break }
                }
                else { break }
            }
        }
        
    }
    fn king(&self, pos: (usize,usize), moves: &mut Vec<Move>) {
        let turn = self.player.1[self.player.0];
        let sign = if turn {1} else {-1i8};
        let (i,j)=pos;

        let base_rank=if turn {0} else {self.len()-1};

        //Normal moves
        for x in -1..=1i8 {
            for y in -1..=1i8 {
                if x==0 && y==0 { continue }
                if in_add(self.len8(),i,x) && in_add(self.len8(),j,y) &&
                self[(add(i,x),add(j,y))].signum()!=sign {
                    moves.push(move_construct!(i,j,add(i,x),add(j,y),self[pos],self[(add(i,x),add(j,y))]));
                }
            }
        }

        //Castling
        if i==base_rank && (j==self.len()/2 || j==(self.len()-1)/2) {
            let rook=ROOK.1;
            //Left side
            if turn && self[(i,0)]==rook || !turn && self[(i,0)]==-rook {
                let mut x=1;
                while x<j {
                    if self[(i,j-x)]!=0 { break }
                    x+=1;
                }
                if x==j {
                    moves.push(move_construct!(i,j,i,j-2,self[pos],self[(i,j-2)]));
                }
            }
            //Right side
            if turn && self[(i,self.len()-1)]==rook || !turn && self[(i,self.len()-1)]==-rook {
                let mut x=1;
                while x<self.len()-1-j {
                    if self[(i,j+x)]!=0 { break }
                    x+=1;
                }
                if x==self.len()-1-j {
                    moves.push(move_construct!(i,j,i,j+2,self[pos],self[(i,j+2)]));
                }
            }
        }
        
    }

    pub fn movegen(&self) -> Vec<Move> {
        let mut moves=vec![];
        let turn = self.player.1[self.player.0];

        for i in 0..self.len() {
            for j in 0..self.len() {
                if self[(i,j)]==0 || turn && self[(i,j)]<0 || !turn && self[(i,j)]>0 { continue }
                let val=abs(self[(i,j)]);
                if val==PAWN.1 { self.pawn((i,j),&mut moves); }
                else if val==KNIGHT.1 { self.knight((i,j),&mut moves); }
                else if val==BISHOP.1 { self.bishop((i,j),&mut moves); }
                else if val==ROOK.1 { self.rook((i,j),&mut moves); }
                else if val==QUEEN.1 {
                    self.rook((i,j),&mut moves); 
                    self.bishop((i,j),&mut moves);
                }
                else if val==KING.1 { self.king((i,j),&mut moves); }
                else if val==MONSTER.1 {
                    self.rook((i,j),&mut moves); 
                    self.bishop((i,j),&mut moves);
                    self.knight((i,j),&mut moves);
                }
            }
        }
        moves
    }
}
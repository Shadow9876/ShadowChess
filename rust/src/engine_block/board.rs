#![allow(dead_code)]
use std::ops::{Index, IndexMut};
use std::cmp::{Ordering,Ordering::Equal,Ordering::Less,Ordering::Greater};
use super::piece::*;


/// This struct describes a move
/// 
/// A move consists of 6 values inside an array:
/// 
/// - 0-1: The first two values define where it came from
/// - 2-3: These values define the destination of the move
/// - 4: This is the value of the moved piece
/// - 5: This is the value of the captured piece
/// 
/// ## Examples:
/// ```
/// let m = Move::new([1, 2, 1, 3, 10, 0]); // A pawn moved one square
/// assert_eq!(m.source(), (1, 2));
/// assert_eq!(m.destination(), (1, 3));
/// ```
#[derive(Clone, PartialEq, Eq)]
pub struct Move {
    pub inner: [i8;6]
}
impl PartialOrd for Move {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        let multiplier=if self[4]<0 && other[4]<0 {1} else {-1};
        let fst_compare=multiplier*((self[5] as i32+self[4] as i32)-(other[5] as i32+other[4] as i32));
        let snd_compare=abs(self[5])-abs(other[5]);
        let trd_compare=-multiplier*(self[5] as i32-other[5] as i32);
        if self[5]!=0 && other[5]!=0 {
            if fst_compare==0 { return Some(Equal) }
            if fst_compare>0 { Some(Less) } else { Some(Greater) }
        }
        else if self[5]!=0 || other[5]!=0 {
            if snd_compare==0 { return Some(Equal) }
            if snd_compare>0 { Some(Less) } else { Some(Greater) }
        }
        else {
            if trd_compare==0 { return Some(Equal) }
            if trd_compare>0 { Some(Less) } else { Some(Greater) }
        }
    }
}
impl Ord for Move {
    fn cmp(&self, other: &Self) -> Ordering {
        self.partial_cmp(other).unwrap()
    }
}
impl Move {
    pub const fn new(inner: [i8;6]) -> Self {
        Self { inner: (inner) }
    }
    /// Get the starting position of the move
    /// 
    /// ## Examples:
    /// ```
    /// let m = Move::new([1, 2, 1, 3, 10, 0]); // A pawn moved one square
    /// assert_eq!(m.source(), (1, 2));
    /// m[0] = 7;
    /// assert_eq!(m.source(), (7, 2));
    /// ```
    pub const fn source(&self) -> (usize,usize) {
        (self.inner[0] as usize, self.inner[1] as usize)
    }
    /// Get the end position of the move
    /// 
    /// ## Examples:
    /// ```
    /// let m = Move::new([1, 2, 1, 3, 10, 0]); // A pawn moved one square
    /// assert_eq!(m.destination(), (1, 3));
    /// m[2] = 7;
    /// assert_eq!(m.source(), (7, 3));
    /// ```
    pub const fn destination(&self) -> (usize,usize) {
        (self.inner[2] as usize, self.inner[3] as usize)
    }
}
impl std::fmt::Display for Move {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f,"[{:?}]",self.inner)
    }
}
impl std::fmt::Debug for Move {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f,"[{:?}]",self.inner)
    }
}
impl Index<usize> for Move {
    type Output = i8;
    fn index(&self, index: usize) -> &Self::Output {
        &self.inner[index]
    }
}
impl IndexMut<usize> for Move {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        &mut self.inner[index]
    }
}

#[macro_export]
macro_rules! move_construct {
    ($a:expr,$b:expr,$c:expr,$d:expr,$e:expr,$f:expr) => {
        {
            use $crate::engine_block::board::Move;
            Move::new([$a as i8,$b as i8,$c as i8,$d as i8,$e as i8,$f as i8])
        }
    };
    ($a:expr,$b:expr,$c:expr,$d:expr,$e:expr) => {
        {
            use $crate::engine_block::board::Move;
            Move::new([$a as i8,$b as i8,$c as i8,$d as i8,$e as i8])
        }
    };
    ($arr:expr) => {
        {
            use $crate::engine_block::board::Move;
            Move::new($arr)
        }
        
    }
}

pub fn abs<T: Default + std::ops::Neg<Output = T> + std::cmp::PartialOrd>(n: T) -> T {
    if n>T::default() { n }
    else { -n }
}
pub fn abs_diff<T: Default + std::cmp::PartialOrd + std::ops::Sub<Output = T>>(n: T, m: T) -> T {
    if n>m { n-m }
    else { m-n }
}

#[derive(Debug,Default,PartialEq, Eq, PartialOrd, Ord, Clone, Copy)]
pub enum MoveOutcome {
    Error,
    BlackWon,
    WhiteWon,
    BlackWonProm,
    WhiteWonProm,
    Promotion,
    #[default]
    OnGoing
}
use MoveOutcome::*;
impl MoveOutcome {
    pub const fn game_over(&self) -> bool {
        !matches!(self, OnGoing | Promotion)
    }
    pub const fn promoted(&self) -> bool {
        matches!(self, Promotion | WhiteWonProm | BlackWonProm)
    }
    pub const fn white_won(&self) -> bool {
        matches!(self, WhiteWon | WhiteWonProm)
    }
    pub const fn black_won(&self) -> bool {
        matches!(self, BlackWon | BlackWonProm)
    }
}

pub fn parse_char(c: char) -> Option<i8> {
    let m=if c.is_uppercase() {-1} else {1};
    const P0: u8=PAWN.0;
    const N0: u8=KNIGHT.0;
    const B0: u8=BISHOP.0;
    const R0: u8=ROOK.0;
    const Q0: u8=QUEEN.0;
    const K0: u8=KING.0;
    const M0: u8=MONSTER.0;
    const E0: u8=EMPTY.0;
    match c.to_ascii_lowercase() as u8 {
        P0 => Some(m*PAWN.1),
        N0 => Some(m*KNIGHT.1),
        B0 => Some(m*BISHOP.1),
        R0 => Some(m*ROOK.1),
        Q0 => Some(m*QUEEN.1),
        K0 => Some(m*KING.1),
        M0 => Some(m*MONSTER.1),
        E0 => Some(0),
        _ => None
    }
}

pub const fn val_to_char(val: i8) -> Option<char> {
    let upper=val<0;
    const P1: i8=PAWN.1;
    const N1: i8=KNIGHT.1;
    const B1: i8=BISHOP.1;
    const R1: i8=ROOK.1;
    const Q1: i8=QUEEN.1;
    const K1: i8=KING.1;
    const M1: i8=MONSTER.1;
    const E1: i8=EMPTY.1;
    match val.abs() {
        P1 => Some(if upper {(PAWN.0 as char).to_ascii_uppercase()} else {PAWN.0 as char}),
        N1 => Some(if upper {(KNIGHT.0 as char).to_ascii_uppercase()} else {KNIGHT.0 as char}),
        B1 => Some(if upper {(BISHOP.0 as char).to_ascii_uppercase()} else {BISHOP.0 as char}),
        R1 => Some(if upper {(ROOK.0 as char).to_ascii_uppercase()} else {ROOK.0 as char}),
        Q1 => Some(if upper {(QUEEN.0 as char).to_ascii_uppercase()} else {QUEEN.0 as char}),
        K1 => Some(if upper {(KING.0 as char).to_ascii_uppercase()} else {KING.0 as char}),
        M1 => Some(if upper {(MONSTER.0 as char).to_ascii_uppercase()} else {MONSTER.0 as char}),
        E1 => Some(EMPTY.0 as char),
        _ => None
    }
}

static mut WEIGHTS: Vec<Vec<i32>>=Vec::new();

fn generate_weights(n: usize) {
    const fn distance(point: (usize,usize), from: (usize,usize)) -> usize {
        point.0.abs_diff(from.0)+point.1.abs_diff(from.1)
    }
    fn distance_from_center(point: (usize,usize), n: usize) -> i32 {
        match n%2 {
            0 => {
                let c0=if point.0>=n/2 {n/2} else {n/2-1};
                let c1=if point.1>=n/2 {n/2} else {n/2-1};
                distance(point, (c0,c1)) as i32
            },
            1 => {
                let center=(n/2,n/2);
                distance(point, center) as i32
            },
            _ => unreachable!()
        }
    }
    const fn distance_from_corner(point: (usize,usize), n: usize) -> i32 {
        let c0=if point.0<n/2 {0} else {n-1};
        let c1=if point.1<n/2 {0} else {n-1};
        distance(point, (c0,c1)) as i32
    }
    unsafe {
        WEIGHTS=vec![vec![0;n];n];
        let max=13-(n/8) as i32;
        let corner=10-(n/10) as i32;
        for i in 0..n {
            for j in 0..n {
                WEIGHTS[i][j]=(max-distance_from_center((i,j), n)).max(corner-distance_from_corner((i,j), n));
            }
        }
        for i in [0,n-1] {
            for j in [0,n-1] {
                WEIGHTS[i][j]=corner;
            }
        }
    }
}

#[derive(Debug, Default, PartialEq, Eq, Clone)]
pub struct Board {
    pub board: Vec<Vec<i8>>,
    pub player: (usize, Vec<bool>),
    pub leaders: (Vec<i8>, Vec<i8>)
}
impl std::fmt::Display for Board {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let mut out=String::new();
        for line in &self.board {
            for item in line {
                if *item!=i8::MIN {
                    if *item<10 && *item>=0 {
                        out+=&format!(" {item}  ");
                    }
                    else if *item<100 && *item>=0 || *item>-10 && *item<=0  {
                        out+=&format!(" {item} ");
                    }
                    else if *item>=0 || *item>-100 && *item<=0 {
                        out+=&format!("{item} ");
                    }
                    else {
                        out+=&format!("{item}");
                    }
                }
                else {
                    out+="    ";
                }
                out+=" ";
            }
            out+="\n";
        }
        write!(f,"{}",out)
    }
}
impl Board {
    pub fn return_chars(&self) -> String {
        let mut out=String::new();
        for i in 0..self.board.len() {
            for j in 0..self.board.len() {
                out+=&val_to_char(self.board[i][j]).unwrap().to_string();
            }
        }
        out
    }
    /// ## Parse
    /// 
    /// Parse the board `size` and a `string` into a board layout. Strings for parsing should
    /// implement the following guidelines:
    /// 
    /// - A parsed `string` should have at least `n * n` valid characters
    /// - Valid characters include: `'p', 'n', 'b', 'r', 'q', 'k', 'm'` and their uppercase
    ///   variants
    /// - 'x' is also a valid character which is parsed to an empty square
    /// - Any other characters won't get parsed, simply side stepped
    /// - If the `string` has more than `n * n` characters, the parser will check for leaders
    /// - Leaders can be given to the parser after the `n * n` valid characters following
    ///   format: `"kqMR"` (this will make the king and queen the leaders of white and the
    ///   monster and rook the leaders of black)
    /// 
    /// ## Examples:
    /// ```
    /// // use rkr | xxx | RKR ; r; R for a more understandable format
    /// let b = Board::parse(3,"rkrxxxRKRrR"); // Make a game with rooks as leaders
    /// ```
    pub fn parse(n: usize, s: &str) -> Self {
        let mut res=Self { board: (vec![vec![0;n];n]), player: (0,vec![false,true,true,false]), leaders: (vec![-KING.1,-MONSTER.1],vec![KING.1,MONSTER.1]) };
        let (mut i, mut j)=(0, 0);
        let mut iter=s.chars();
        loop {
            let char=iter.next();
            match char {
                Some(val) => {
                    let ch=parse_char(val);
                    match ch {
                        Some(val) => res.board[i][j]=val,
                        None => continue
                    }
                    j+=1;
                    if j==n {
                        j=0;
                        i+=1;
                        if i==n { break; }
                    }
                }
                None => break
            }
            
        }
        let mut new_leaders_0=vec![];
        let mut new_leaders_1=vec![];
        for c in iter {
            let ch=parse_char(c);
            if let Some(val)=ch {
                if val>0 { new_leaders_1.push(val); }
                if val<0 { new_leaders_0.push(val); }
            }
        }
        if !new_leaders_0.is_empty() {res.leaders.0=new_leaders_0}
        if !new_leaders_1.is_empty() {res.leaders.1=new_leaders_1}
        
        generate_weights(n);
        res
    }
    /// ## Change Turns
    /// 
    /// Change the move order of the game. Requires a `Vec` of `bool`s in which
    /// `true` means white to move, `false` means black to move. Turns are defined
    /// as `vec![false,true,true,false]` by default.
    /// 
    /// ## Examples:
    /// ```
    /// let mut b = Board::parse(3,"rkrxxxRKRrR");
    /// b.change_turns(vec![true,false]); // Change turns to fit Chess
    /// ```
    pub fn change_turns(&mut self, turns: Vec<bool>) {
        self.player.0=0;
        self.player.1=turns;
    }
    /// ## Change Leaders
    /// 
    /// Change leaders of a game. A leader is for example a king in Chess. If it dies
    /// the opponent wins. In ShadowChess multiple leaders can be present on the board
    /// at the same time, and the opponent only wins if they manage to capture all of them.
    /// 
    /// Leaders are defined as `w_leader=vec![KING.1,MONSTER.1]`, `b_leader=vec![-KING.1,-MONSTER.1]`
    /// where `KING.1` and `MONSTER.1` are constants representing the numeric value of their respective
    /// pieces.
    /// 
    /// This API is of course is much easier. One only has to provide the pieces' valid characters
    /// (characters that can be parsed).
    /// 
    /// ## Examples:
    /// ```
    /// let mut b=Board::parse(3,"rkrxxxRKRrR");
    /// b.change_leaders(Some(vec!['K']),None); // Change the leader of the black pieces to king
    /// ```
    pub fn change_leaders(&mut self, b_leader: Option<Vec<char>>, w_leader: Option<Vec<char>>) {
        let mut change_black=b_leader.is_some();
        let mut change_white=w_leader.is_some();
        let black=b_leader.unwrap_or_default().into_iter().map(|c| parse_char(c).unwrap_or(0)).collect::<Vec<_>>();
        let white=w_leader.unwrap_or_default().into_iter().map(|c| parse_char(c).unwrap_or(0)).collect::<Vec<_>>();
        change_black=change_black && !black.contains(&0);
        change_white=change_white && !white.contains(&0);
        if change_black { self.leaders.0=black; }
        if change_white { self.leaders.1=white; }
    }
    /// ## Move
    /// 
    /// Execute `Move` on the board. Returns a `MoveOutcome`. Does not check whether game
    /// has ended before executing move.
    /// 
    /// ## Examples:
    /// ```
    /// let mut b=Board::parse(3,"rkrxxxRKRrR");
    /// // Move black rook from the bottom to the top left corner
    /// b.forward_move(&move_construct!(2,0,0,0,-50,50));
    /// ```
    pub fn forward_move(&mut self, m: &Move) -> MoveOutcome {
        let (f1,f2)=m.source();
        let (t1,t2)=m.destination();

        // Executing the move
        // En passant
        if abs(m[4])==PAWN.1 && abs_diff(f1,t1)==1 && abs_diff(f2,t2)==1 && m[5]==0 {
            self[(f1,t2)]=0;
        }
        //Promotion
        let promotion=m[4]!=self[m.source()];
        // Castling
        if abs(m[4])==15 && abs_diff(f2,t2)==2 {
            let ch1=if f2>t2 {0} else {self.len()-1};
            let ch2=(t2 as i32+(if f2>t2 {1} else {-1i32})) as usize;

            self[(f1,ch2)]=self[(f1,ch1)];
            self[(f1,ch1)]=0;
        }
        // Normal moves
        self[(t1,t2)]=m[4];
        self[(f1,f2)]=0;

        // Updating turns
        self.player.0=(self.player.0+1)%self.player.1.len();

        //Outcome of the move
        if self.leaders.0.contains(&m[5]) || self.leaders.1.contains(&m[5]) {
            let black=self.board.iter().any(|x| x.into_iter().any(|y| *y!=0 && self.leaders.0.contains(y)));
            let white=self.board.iter().any(|x| x.into_iter().any(|y| *y!=0 && self.leaders.1.contains(y)));
            if !white && !black { return Error; }
            if !white { 
                if !promotion { return BlackWon; }
                else { return BlackWonProm; }
            } 
            if !black {
                if !promotion { return WhiteWon; }
                else { return WhiteWonProm; }
            } 
        }

        if promotion { Promotion }
        else { OnGoing }
    }
    /// ## Undo Move
    /// 
    /// Undo `Move` on board. Is almost always valid (except if original move was not executed
    /// on the board).
    /// 
    /// ## Examples:
    /// ```
    /// let mut b=Board::parse(3,"rkrxxxRKRrR");
    /// let m=move_construct!(2,0,0,0,-50,50);
    /// b.forward_move(&m);
    /// b.backward_move(m,false); // Revert move
    /// ```
    pub fn backward_move(&mut self, mut m: Move, prom: bool) {
        let (f1,f2)=m.source();
        let (t1,t2)=m.destination();
        //En passant
        if abs(m[4])==PAWN.1 && abs_diff(f1,t1)==1 && abs_diff(f2,t2)==1 && m[5]==0 {
            self[(f1,t2)] = -m[4];
        }

        //Promotion
        if prom {
            m[4]=if m[4]>0 {PAWN.1} else {-PAWN.1};
        }

        //Castling
        if abs(m[4])==15 && abs_diff(f2,t2)==2
        {
            let ch1=if f2>t2 {0} else {self.len()-1};
            let ch2=(t2 as i32+(if f2>t2 {1} else {-1i32})) as usize;

            self[(f1,ch1)]=self[(f1,ch2)];
            self[(f1,ch2)]=0;
        }

        //Normal moves
        self[(t1,t2)]=m[5];
        self[(f1,f2)]=m[4];

        //Reverting turns
        self.player.0=(self.player.0+self.player.1.len()-1)%self.player.1.len();
    }
    /// ## Evaluate
    /// 
    /// Evaluate the position at depth 0.
    /// 
    /// ## Examples:
    /// ```
    /// let b=Board::parse("rkrxxxRKR");
    /// assert_eq!(b.eval(),0);
    /// ```
    pub fn eval(&self) -> i32 {
        self.board.iter().enumerate()
        .map(|(i,x)| 
        x.into_iter().enumerate()
        .map(|(j,y)| *y as i32*unsafe{WEIGHTS[i][j]})
        .sum::<i32>())
        .sum()
    }
    /// Returns the size of the board (n).
    ///
    /// # Examples
    ///
    /// ```
    /// let b = Board::parse(3,"xxxxxxxxx");
    /// assert_eq!(b.len(), 3);
    /// ```
    pub fn len(&self) -> usize {
        self.board.len()
    }
    /// Returns the size of the board (n) as a i8.
    ///
    /// # Examples
    ///
    /// ```
    /// let b = Board::parse(3,"xxxxxxxxx");
    /// assert_eq!(b.len8(), 3i8);
    /// ```
    pub fn len8(&self) -> i8 {
        self.board.len() as i8
    }

    pub fn forward_move_instructions(&self, m: &Move) -> Vec<Move> {
        let mut out=vec![];
        let (f1,f2)=m.source();
        let (t1,t2)=m.destination();

        // En passant
        if abs(m[4])==PAWN.1 && abs_diff(f1,t1)==1 && abs_diff(f2,t2)==1 && m[5]==0 {
            out.push(move_construct!(f1,t2,-1,-1,self[(f1,t2)],0));
        }
        // Castling
        if abs(m[4])==15 && abs_diff(f2,t2)==2 {
            let ch1=if f2>t2 {0} else {self.len()-1};
            let ch2=(t2 as i32+(if f2>t2 {1} else {-1i32})) as usize;

            out.push(move_construct!(f1,ch1,f1,ch2,self[(f1,ch1)],0));
        }
        out.push(m.clone());
        out.push(move_construct!(t1,t2,-1,-1,self[(t1,t2)],0));

        out
    }

    pub fn backward_move_instructions(&self, mut m: Move, prom: bool) -> Vec<Move> {
        let mut out=vec![];
        let (f1,f2)=m.source();
        let (t1,t2)=m.destination();
        //En passant
        if abs(m[4])==PAWN.1 && abs_diff(f1,t1)==1 && abs_diff(f2,t2)==1 && m[5]==0 {
            out.push(move_construct!(-1,-1,f1,t2,-m[4],0));
        }

        //Promotion
        if prom {
            m[4]=if m[4]>0 {PAWN.1} else {-PAWN.1};
        }

        //Castling
        if abs(m[4])==15 && abs_diff(f2,t2)==2
        {
            let ch1=if f2>t2 {0} else {self.len()-1};
            let ch2=(t2 as i32+(if f2>t2 {1} else {-1i32})) as usize;

            out.push(move_construct!(f1,ch2,f1,ch1,self[(f1,ch2)],0));
        }

        out.push(move_construct!(-1,-1,t1,t2,m[5],0));
        out.push(move_construct!(t1,t2,f1,f2,m[4],0));
        

        out
    }
}
impl Index<(usize,usize)> for Board {
    type Output = i8;
    fn index(&self, index: (usize,usize)) -> &Self::Output {
        &self.board[index.0][index.1]
    }
}
impl IndexMut<(usize,usize)> for Board {
    fn index_mut(&mut self, index: (usize,usize)) -> &mut Self::Output {
        &mut self.board[index.0][index.1]
    }
}
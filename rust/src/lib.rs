use std::str::FromStr;

use engine_block::board::{parse_char, val_to_char, Move};
use engine_block::piece::{BISHOP, EMPTY, KING, KNIGHT, MONSTER, QUEEN, ROOK};
use godot::prelude::*;

pub mod engine_block;
use crate::engine_block::piece::PAWN;

use crate::engine_block::board::Board;

/// A Type to differentiate between Godot and Rust integer values
/// 
/// Is an alias for `i32`.
#[allow(non_camel_case_types)]
type int=i32;

struct EngineExt;

#[gdextension]
unsafe impl ExtensionLibrary for EngineExt {}

macro_rules! vec_impl {
    ($Name:ident, $Len:literal) => {
        /// A wrapper around arrays with specific sizes
        /// 
        /// It mainly encodes information about moves
        #[derive(GodotClass,PartialEq,Eq,Debug)]
        #[class(init)]
        struct $Name {
            inner: [i8;$Len]
        }

        #[godot_api]
        impl $Name {
            /// Make an Array uninitialised
            /// Example:
            /// ```
            /// var arr:=Vec4.new()
            /// arr.uninit()
            /// assert(arr.is_uninit())
            /// ```
            #[func]
            pub fn uninit(&mut self) {
                self.inner=[127;$Len]
            }
            /// Construct a new Array
            /// 
            /// It's almost equivalent with calling `Vec.new()`, but it also makes all values uninitialised in the array.
            /// Example:
            /// ```
            /// var arr:=Vec4.constr()
            /// assert(arr.is_uninit())
            /// ```
            #[func]
            pub fn constr() -> Gd<Self> {
                let mut obj=Self { inner: ([0;$Len]) }; obj.uninit();
                Gd::from_object(obj)
            }
            /// Get the `index`th element of the array
            /// 
            /// Array indexing starts from `0` and in this case negative values **cannot** be used to index the array. If an invalid index is given the function returns `128`.
            /// Example:
            /// ```
            /// var arr:=Vec4.from_arr([1,2,3,4])
            /// assert(arr.get_at(0)==1)
            /// assert(arr.get_at(1)==2)
            /// assert(arr.get_at(2)==3)
            /// assert(arr.get_at(3)==4)
            /// ```
            #[func]
            pub fn get_at(&self, index: int) -> int {
                if index<0 || index as usize>=self.inner.len() {
                    if index<0 {godot_error!("Error [128]: UnderIndexing in function 'get_at' of Vec{0}! Index ({index}) was too small for Vec{0}! Minimum index is 0! (Array indexing starts from 0; you can't use negative numbers as indices here)",self.inner.len())}
                    else {godot_error!("Error [128]: OverIndexing in function 'get_at' of Vec{0}! Index ({index}) was too big for Vec{0}! Maximum index is {1}! (Array indexing starts from 0)", self.inner.len(),self.inner.len()-1)}
                    128
                }
                else {self.inner[index as usize] as int}
            }
            /// Set the `index`th element of the array to `val` and return with old value
            /// 
            /// Array indexing starts from `0` and in this case negative values **cannot** be used to index the array. If an invalid index is given the function returns `129`.
            /// `val` must be between `-128..127`. If an invalid value is given the function returns `130`.
            /// Example:
            /// ```
            /// var arr:=Vec4.from_arr([1,2,3,4])
            /// Util.discard(arr.set_at(2,116))
            /// assert(arr.get_at(2)==116)
            /// ```
            #[func]
            pub fn set_at(&mut self, index: int, val: int) -> int {
                if index>=0 && (index as usize)<self.inner.len() {
                    if val<=i8::MAX as i32 && val>=i8::MIN as i32 {
                        let tmp=self.inner[index as usize];
                        self.inner[index as usize]=val as i8;
                        tmp as int
                    }
                    else {
                        if val>i8::MAX as i32 {godot_error!("Error [130]: Value OverFlow in function 'set_at' of Vec{}! Integer value {val} does not fit into range -128..127!",self.inner.len());}
                        else {godot_error!("Error [130]: Value UnderFlow in function 'set_at' of Vec{}! Integer value {val} does not fit into range -128..127!",self.inner.len());}
                        130
                    }
                }
                else {
                    if index>0 {godot_error!("Error [129]: OverIndexing in function 'set_at' of Vec{0}! Index ({index}) was too big for Vec{0}! Maximum index is {1}! (Array indexing starts from 0)",self.inner.len(),self.inner.len()-1)}
                    else {godot_error!("Error [129]: UnderIndexing in function 'set_at' of Vec{0}! Index ({index}) was too small for Vec{0}! Minimum index is 0! (Array indexing starts from 0; you can't use negative numbers as indices here)",self.inner.len())}
                    129
                }
            }
            /// Creates a new Vec from an `Array[int]`
            /// 
            /// `arr` must have a minimum size, which is indicated in Vec. If size of `arr` is smaller than that, the function will return with an uninitialised Vec.
            /// Values of `arr` must be between `-128..127`. If an invalid value is inside the array, the function will return with an uninitialised Vec.
            /// Example:
            /// ```
            /// var example:=Vec4.from_arr([1,2,3,4])
            /// assert(example.get_at(1)==2)
            /// ```
            #[func]
            pub fn from_arr(arr: Array<int>) -> Gd<Self> {
                if arr.len()<4 {
                    godot_error!("Error [127.{0}]: OverIndexing in function 'from_arr' of Vec{0}! Array ({arr}) has size {1} while 'from_arr' needs an array with length {0} or more!",$Len,arr.len());
                    let mut obj=Self { inner: ([0;$Len]) }; obj.uninit();
                    Gd::from_object(obj)
                }
                else {
                    let mut safe=true;
                    let mut val=0;
                    for i in 0..$Len {
                        let tmp=arr.get(i).unwrap();
                        if tmp>i8::MAX as int || tmp<i8::MIN as int {safe=false; val=tmp;}
                    }
                    if safe {
                        let mut inner=[0;$Len];
                        for i in 0..inner.len() { inner[i]=i }
                        Gd::from_object( Self { inner: (inner.map(|i| arr.get(i).unwrap() as i8)) } ) 
                    }
                    else {
                        if val>i8::MAX as i32 {godot_error!("Error [127.{0}]: Value OverFlow in function 'from_arr' of Vec{0}! Integer value {val} does not fit into range -128..127!",arr.len());}
                        else {godot_error!("Error [127.{0}]: Value UnderFlow in function 'from_arr' of Vec{0}! Integer value {val} does not fit into range -128..127!",arr.len());}
                        let mut obj=Self { inner: ([0;$Len]) }; obj.uninit();
                        Gd::from_object(obj)
                    }
                }
            }
            /// Creates a new Vec from a `PackedInt32Array`
            /// 
            /// `packed` must have a minimum size, which is indicated in Vec. If size of `packed` is smaller than that, the function will return with an uninitialised Vec.
            /// Values of `packed` must be between `-128..127`. If an invalid value is inside the array, the function will return with an uninitialised Vec.
            /// Example:
            /// ```
            /// var example:=Vec4.from_packed_arr(PackedInt32Array([1,2,3,4]))
            /// assert(example.get_at(1)==2)
            /// ```
            #[func]
            pub fn from_packed_arr(packed: PackedInt32Array) -> Gd<Self> {
                if packed.len()<$Len {
                    godot_error!("Error [127.{0}]: OverIndexing in function 'from_packed_arr' of Vec{0}! Array ({packed}) has size {1} while 'from_packed_arr' needs an array with length {0} or more!",$Len,packed.len());
                    let mut obj=Self { inner: ([0;$Len]) }; obj.uninit();
                    Gd::from_object(obj)
                }
                else {
                    let mut safe=true;
                    let mut val=0;
                    for i in 0..$Len {
                        let tmp=packed.get(i).unwrap();
                        if tmp>i8::MAX as int || tmp<i8::MIN as int {safe=false; val=tmp;}
                    }
                    if safe {
                        let mut inner=[0;$Len];
                        for i in 0..inner.len() { inner[i]=i }
                        Gd::from_object( Self { inner: (inner.map(|i| packed.get(i).unwrap() as i8)) } ) 
                    }
                    else {
                        if val>i8::MAX as i32 {godot_error!("Error [127.{0}]: Value OverFlow in function 'from_packed_arr' of Vec{0}! Integer value {val} does not fit into range -128..127!",packed.len());}
                        else {godot_error!("Error [127.{0}]: Value UnderFlow in function 'from_packed_arr' of Vec{0}! Integer value {val} does not fit into range -128..127!",packed.len());}
                        let mut obj=Self { inner: ([0;$Len]) }; obj.uninit();
                        Gd::from_object(obj)
                    }
                }
            }
            /// Clone an Vec, returning a new deep copy of it
            /// 
            /// Example:
            /// ```
            /// var arr:=Vec4.constr()
            /// var arr_clone:=arr.clone()
            /// assert(arr.eq(arr_clone))
            /// ```
            #[func]
            pub fn clone(&self) -> Gd<Self> {
                Gd::from_object(Self { inner: (self.inner.clone()) })
            }
            /// Count uninitialised elements in Vec
            /// 
            /// Example:
            /// ```
            /// var arr:=Vec4.constr()
            /// assert(arr.count_uninit()==4)
            /// arr.set_at(2,116)
            /// assert(arr.count_uninit()==3)
            /// ```
            #[func]
            pub fn count_uninit(&self) -> int {
                self.inner.iter().filter(|&&i| i==127).count() as int
            }
            /// Check if Vec is uninitialised
            /// 
            /// Example:
            /// ```
            /// var uninit_arr:=Vec4.constr()
            /// assert(uninit_arr.is_uninit())
            /// var init_arr:=Vec4.from_arr([1,2,3,4])
            /// assert(!init_arr.is_uninit())
            /// ```
            #[func]
            pub fn is_uninit(&self) -> bool {
                self.count_uninit()==self.inner.len() as int
            }
            /// Display Vec
            /// 
            /// Converts Vec into `String`
            /// Example:
            /// ```
            /// var arr:=Vec4.from_arr([1,2,3,4])
            /// print(arr.display()) # Prints "[1, 2, 3, 4]"
            /// ```
            #[func]
            pub fn display(&self) -> GString {
                format!("{:?}",self.inner).replace("127", "uninit").to_godot()
            }
            /// Check if two Vec (of the same size) are identical
            /// 
            /// Example:
            /// ```
            /// var arr1:=Vec4.from_arr([1,2,3,4])
            /// var arr2:=arr1.clone()
            /// var arr3:=Vec4.from_arr([1,2,3,5])
            /// assert(arr1.eq(arr2))
            /// assert(!arr1.eq(arr3))
            /// ```
            #[func]
            pub fn eq(&self, other: Gd<Self>) -> bool {
                self.inner==other.bind().inner
            }
            /// Get size of Vec
            /// 
            /// Example:
            /// ```
            /// var arr4:=Vec4.constr()
            /// assert(arr4.len()==4)
            /// var arr8:=Vec8.constr()
            /// assert(arr8.len()==8)
            /// ```
            #[func]
            pub fn len(&self) -> int {
                self.inner.len() as int
            }
            /// Construct an Vec from an Vec4
            /// 
            /// If `arr.len() > 4` it leaves elements with associated indices greater than 3 uninitialised.
            /// Example:
            /// ```
            /// var arr4:=Vec4.from_arr([1,2,3,4])
            /// var arr6:=Vec6.from4(arr4)
            /// assert(arr6.get_at(0)==1)
            /// assert(arr6.count_uninit()==2)
            /// ```
            #[func]
            pub fn from4(arr: Gd<Vec4>) -> Gd<Self> {
                let mut out=Self { inner: ([0;$Len]) };
                use std::cmp::min;
                for i in 0..min(4,$Len) {
                    out.inner[i]=arr.bind().inner[i];
                }
                Gd::from_object(out)
            }
            /// Construct an Vec from an Vec6
            /// 
            /// If `arr.len() > 6` it leaves elements with associated indices greater than 5 uninitialised.
            /// Example:
            /// ```
            /// var arr6:=Vec6.from_arr([1,2,3,4,5,6])
            /// var arr8:=Vec8.from6(arr6)
            /// assert(arr8.get_at(0)==1)
            /// assert(arr8.count_uninit()==2)
            /// ```
            #[func]
            pub fn from6(arr: Gd<Vec6>) -> Gd<Self> {
                let mut out=Self { inner: ([0;$Len]) };
                use std::cmp::min;
                for i in 0..min(6,$Len) {
                    out.inner[i]=arr.bind().inner[i];
                }
                Gd::from_object(out)
            }
            /// Construct an Vec from an Vec8
            /// 
            /// If `arr.len() > 8` it leaves elements with associated indices greater than 7 uninitialised.
            /// Example:
            /// ```
            /// var arr8:=Vec8.from_arr([1,2,3,4,5,6,7,8])
            /// var arr6:=Vec6.from8(arr8)
            /// assert(arr6.get_at(0)==1)
            /// assert(arr6.get_at(5)==6)
            /// ```
            #[func]
            pub fn from8(arr: Gd<Vec8>) -> Gd<Self> {
                let mut out=Self { inner: ([0;$Len]) };
                use std::cmp::min;
                for i in 0..min(8,$Len) {
                    out.inner[i]=arr.bind().inner[i];
                }
                Gd::from_object(out)
            }
            #[func]
            pub fn to_packed(&self) -> PackedInt32Array {
                let mut out=PackedInt32Array::new();
                for i in self.inner {
                    out.push(i as int);
                }
                out
            }
        }
    };
}

vec_impl!(Vec4,4);
vec_impl!(Vec6,6);
vec_impl!(Vec8,8);

#[derive(GodotClass)]
#[class(init)]
struct Char {
    inner: char
}

#[godot_api]
impl Char {
    /// Get value of `Char`
    /// 
    /// Returns a `String` of length `1`.
    /// Example:
    /// ```
    /// var char:=Char.from_string('S')
    /// assert(char.get_char()=='S')
    /// ```
    #[func]
    pub fn get_char(&self) -> GString {
        GString::from_str(&self.inner.to_string()).unwrap()
    }
    /// Set value of `Char` to `val`
    /// 
    /// `String` must have a length of 1 or more to successfully assign anything to `Char`. It assigns the first character of the `String` to `Char`.
    /// If `val` is empty it'll return with an error, if `String` has more than 1 characters it'll show a warning.
    /// Example:
    /// ```
    /// var char:=Char.new()
    /// char.set_char('C')
    /// assert(char.get_char()=='C')
    /// ```
    #[func]
    pub fn set_char(&mut self, val: GString) {
        let chars=val.chars();
        if chars.is_empty() {godot_error!("Error [112]: Length of Char must be precisely 1! Got empty String!"); return}
        if chars.len()>1 {godot_warn!("Warning [0]: Length of Char must be precisely 1! Tried to assign string of length {} to Char. Assigning first character!",chars.len())}
        self.inner=chars[0];
    }
    /// Creates a new `Char` from a `String`
    /// 
    /// `string` must have a length of 1 or more to successfully assign anything to `Char`. It assigns the first character of the `String` to `Char`.
    /// If `string` is empty it'll return with an error, if it has more than 1 characters it'll show a warning.
    /// Examples:
    /// ```
    /// var char:=Char.from_string("l")
    /// assert(char.get_char()=='l')
    /// ```
    /// or
    /// ```
    /// var char:=Char.from_string("String") # Will show a warning
    /// assert(char.get_char()=='S')
    /// ```
    #[func]
    pub fn from_string(string: GString) -> Gd<Self> {
        let mut out=Self { inner: ('\0') };
        out.set_char(string);
        Gd::from_object(out)
    }
    /// Displays the `Char`
    /// 
    /// Does the same thing as `.get_char()`.
    /// Example:
    /// ```
    /// var char:=Char.from_string('S')
    /// assert(char.display()=='S' && char.display()==char.get_char())
    /// ```
    #[func]
    pub fn display(&self) -> GString {
        self.get_char()
    }
    /// Check if two `Char`s are identical
    /// 
    /// Examples:
    /// ```
    /// var char1:=Char.from_string('S')
    /// var char2:=Char.from_string('S')
    /// assert(char1.eq(char2))
    /// ```
    /// and
    /// ```
    /// var char1:=Char.from_string('S')
    /// var char2:=Char.from_string('k')
    /// assert(!char1.eq(char2))
    /// ```
    #[func]
    pub fn eq(&self, other: Gd<Self>) -> bool {
        self.inner==other.bind().inner
    }
    #[func]
    pub fn is_lower(&self) -> bool {
        self.inner.is_lowercase()
    }
    #[func]
    pub fn is_upper(&self) -> bool {
        self.inner.is_uppercase()
    }
    #[func]
    pub fn lower(&self) -> Gd<Self> {
        Gd::from_object(Char { inner: (self.inner.to_ascii_lowercase()) })
    }
    #[func]
    pub fn upper(&self) -> Gd<Self> {
        Gd::from_object(Char { inner: (self.inner.to_ascii_uppercase()) })
    }
}

/// A wrapper to help interact with the constants defined in the engine
#[derive(GodotClass)]
#[class(init)]
struct Pieces {}

#[godot_api]
impl Pieces {
    /// Returns value of character based on `ChessEngine` definitions
    /// 
    /// Returns an `int` in the range of `-128..127` (both ends included), returns a negative number if `chr` is uppercase (black) and a positive value if `chr` is lowercase.
    /// Currently it returns the following values for the following characters:
    /// `x`: `0` (empty)
    /// `p`: `10` (pawn)
    /// `n`: `30` (knight)
    /// `b`: `35` (bishop)
    /// `r`: `50` (rook)
    /// `q`: `90` (queen)
    /// `k`: `15` (king)
    /// `m`: `99` (monster)
    /// 
    /// (Returns their negative equivalent for uppercase characters, returns `0` for any other character)
    /// These exact values may change in future **major** versions of the engine, but the general logic of negative and positive values as well as the following equation will not change: `monster > queen > rook > bishop > knight > king > pawn > empty = 0`.
    #[func]
    pub fn get_val_of_char(chr: Gd<Char>) -> int {
        let checked = chr.bind().display().chars()[0];
        let r=parse_char(checked);
        r.map(|x| x as int).unwrap_or(0)
    }
    /// Returns the character associated with a certain value based on `ChessEngine` definitions
    /// 
    /// Returns a `GString` with length 1, returns an uppercase character if `val` is negative and a lowercase character if `val` is positive.
    /// Currently it returns the following characters for the following values:
    /// `0` : `x` (empty)
    /// `10`: `p` (pawn)        i8

    /// `30`: `n` (knight)
    /// `35`: `b` (bishop)
    /// `50`: `r` (rook)
    /// `90`: `q` (queen)
    /// `15`: `k` (king)
    /// `99`: `m` (monster)
    /// 
    /// (Returns their uppercase equivalent for negative values, returns `\0` for any other values)
    /// These exact values may change in future **major** versions of the engine, but the exact characters will remain the same. (Therefore it's a bad idea to use this function on its own.)
    #[func]
    pub fn get_char_from_val(val: int) -> Gd<Char> {
        let v=i8::try_from(val);
        Gd::from_object(v.map_or_else(|_| Char {inner: '\0'}, |val8| Char { inner: (val_to_char(val8).unwrap_or('\0')) }))
    }
    /// Returns the character associated with a piece name
    /// 
    /// Returns a `GString` with length 1; always returns a lowercase character (which is associated with white, to get its black counterpart simply make it an uppercase character).
    /// Currently it returns the following characters for the following names:
    /// `empty`  : `x`
    /// `pawn`   : `p`
    /// `knight` : `n`
    /// `bishop` : `b`
    /// `rook`   : `r`
    /// `queen`  : `q`
    /// `king`   : `k`
    /// `monster`: `m`
    /// 
    /// (Returns `\0` for any other value)
    #[func]
    pub fn get_char_from_name(name: GString) -> Gd<Char> {
        Gd::from_object(Char { inner: ({
            match name.to_string().to_lowercase().as_str() {
                "king" => KING.0 as char,
                "queen" => QUEEN.0 as char,
                "rook" => ROOK.0 as char,
                "bishop" => BISHOP.0 as char,
                "knight" => KNIGHT.0 as char,
                "pawn" => PAWN.0 as char,
                "monster" => MONSTER.0 as char,
                "empty" => EMPTY.0 as char,
                _ => '\0'
            }
        })})
    }
    /// Returns the value of a piece name based on `ChessEngine` definitions
    /// 
    /// Returns an `int` in the range of `0..127` (both ends included); it always returns a positive number (which is associated with white, to get its black counterpart simply negate the value).
    /// Currently it returns the following values for the following names:
    /// `empty`  : `0`
    /// `pawn`   : `10`
    /// `knight` : `30`
    /// `bishop` : `35`
    /// `rook`   : `50`
    /// `queen`  : `90`
    /// `king`   : `15`
    /// `monster`: `99`
    /// 
    /// (Returns `0` for any other name)
    /// These exact values may change in future **major** versions of the engine, but the general logic of negative and positive values as well as the following equation will not change: `monster > queen > rook > bishop > knight > king > pawn > empty = 0`.
    #[func]
    pub fn get_val_from_name(name: GString) -> int {
        match name.to_string().to_lowercase().as_str() {
            "king" => KING.1 as int,
            "queen" => QUEEN.1 as int,
            "rook" => ROOK.1 as int,
            "bishop" => BISHOP.1 as int,
            "knight" => KNIGHT.1 as int,
            "pawn" => PAWN.1 as int,
            "monster" => MONSTER.1 as int,
            "empty" => EMPTY.1 as int,
            _ => 0
        }
    }
}


#[derive(GodotClass)]
#[class(base=Node)]
struct ChessEngine {
    board: Board,
    is_game_over: bool,
    base: Base<Node>
}

#[godot_api]
impl INode for ChessEngine {
    fn init(base: Base<Node>) -> Self {     
        Self {
            board: (Board::default()),
            is_game_over: (false),
            base
        }
    }
}

fn array_contains_vec6(arr: Array<Gd<Vec6>>, element: Gd<Vec6>) -> bool {
    for i in 0..arr.len() {
        // element.inner literally implements Copy, it's fine
        if arr.get(i).unwrap().bind().eq(element.clone()) {
            return true;
        }
    }
    false
}

#[godot_api]
impl ChessEngine {
    #[func]
    pub fn parse(&mut self, mut n: int, str: GString) {
        if n<0 {
            godot_error!("Error [1]: Low value in function 'parse' of ChessEngine! 'n' must be greater or equal to zero! Got {n}, program will continue working with n = 1.");
            n = 1;
        }
        let s=str.to_string();
        self.board=Board::parse(n as usize,&s);
    }
    #[func]
    pub fn complete_parse(&mut self, mut n: int, str: GString, turns: Array<bool>) {
        if n<0 {
            godot_error!("Error [1]: Low value in function 'complete_parse' of ChessEngine! 'n' must be greater or equal to zero! Got {n}, program will continue working with n = 1.");
            n = 1;
        }
        self.parse(n, str);
        let mut v=vec![];
        for i in 0..turns.len() {
            v.push(turns.get(i).unwrap());
        }
        self.board.change_turns(v);
    }
    #[func]
    pub fn get_legal_moves(&self) -> Array<Gd<Vec6>> {
        let res=self.board.movegen();
        let mut out=Array::new();
        for m in res {
            out.push(Gd::from_object(Vec6 {inner: (m.inner)}));
        }
        out
    }
    #[func]
    pub fn is_move_promotion(&self, m: Gd<Vec6>) -> bool {
        let rf=m.bind();
        rf.inner[4].abs()!=PAWN.1 && self.board[(rf.inner[0] as usize,rf.inner[1] as usize as usize)]==rf.inner[4] ||
        rf.inner[4].abs()==PAWN.1 && !array_contains_vec6(self.get_legal_moves(), rf.clone())
    }
    #[func]
    pub fn get_best_move(&mut self, mut depth: int) -> Gd<Vec6> {
        if depth<0 {
            godot_error!("Error [0]: Low value in 'get_best_move' of ChessEngine! 'depth' should be greater or equal than zero! Got {depth}, program will continue working with depth = 0.");
            depth=0;
        }
        if depth>u8::MAX as int {
            godot_error!("Error [255]: High value in 'get_best_move' of ChessEngine! 'depth' should be smaller or equal than 255! Got {depth}, program will continue working with depth = 255.");
            depth=255;
        }
        let res=self.board.get_best_move(depth as u8);
        if let Some(val)=res {
            Gd::from_object(Vec6 { inner: (val.inner) })
        }
        else {
            godot_error!("Unrecoverable Error [65]: There are no possible moves to make in 'get_best_move' of ChessEngine! Terminating the program.");
            panic!("No moves can be made in this position!")
        }
    }
    /*#[func]
    pub fn get_probed_move(&mut self, depth: int, engine_depth: int) -> Gd<Vec6> {
        let res=self.board.engine_prober(depth as u8, engine_depth as u8);
        let mut p=PackedInt32Array::new();
        if let Some(val)=res {
            p.extend(val.inner.iter().map(|&x| x as int));
        }
        p
    }*/
    #[func]
    pub fn make_move(&mut self, m: Gd<Vec6>) -> bool {
        let current_move=Move { inner: (m.bind().inner) };
        let res=self.board.forward_move(&current_move);
        self.is_game_over=res.game_over();
        res.promoted()
    }
    #[func]
    pub fn revert_move(&mut self, m: Gd<Vec6>, prom: bool) {
        let current_move=Move { inner: (m.bind().inner) };
        self.board.backward_move(current_move,prom);
    }
    #[func]
    pub fn is_game_over(&self) -> bool {
        self.is_game_over
    }
    #[func]
    pub fn get_turn(&self) -> bool {
        self.board.player.1[self.board.player.0]
    }
    #[func]
    pub fn get_board(&self) -> GString {
        GString::from(self.board.return_chars())
    }
    #[func]
    pub fn f_move_instructions(&self, m: Gd<Vec6>) -> Array<Gd<Vec6>> {
        let mut out=Array::new();
        let mv=Move { inner: (m.bind().inner) };
        for m in self.board.forward_move_instructions(&mv) {
            out.push(Gd::from_object(Vec6 {inner: (m.inner)}));
        }
        out
    }
    #[func]
    pub fn b_move_instructions(&self, m: Gd<Vec6>, prom: bool) -> Array<Gd<Vec6>> {
        let mut out=Array::new();
        let mv=Move { inner: (m.bind().inner) };
        for m in self.board.backward_move_instructions(mv,prom) {
            out.push(Gd::from_object(Vec6 {inner: (m.inner)}));
        }
        out
    }
}
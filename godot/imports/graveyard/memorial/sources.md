Sources in 'memorial':
- [**Tree Interaction**'s and **great_tree**'s background](https://commons.wikimedia.org/wiki/File:Green_fields_of_grain.jpg) (in public domain); Tree Interaction is licensed under [CC-BY-SA-4.0](https://creativecommons.org/licenses/by-sa/4.0/)

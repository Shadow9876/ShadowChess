Sources in 'main_menu':
- [**Game Difficulty** and **Game Difficulty Deselect**](https://pixnio.com/nature-landscapes/waterfalls/waterfalls-background) (under CC0)
- [**Real Life Difficulty** and **Real Life Difficulty Deselect**](https://pixnio.com/nature-landscapes/canyon/bryce-canyon-at-sunrise) (under CC0)
- [Copyright.svg](https://en.wikipedia.org/wiki/File:Copyright.svg) (under CC0)
- **Information.svg** is a derivation of [Copyright.svg](https://en.wikipedia.org/wiki/File:Copyright.svg) (which is under CC0)

class_name Env
extends Node3D

## Environments with names[br]
## [br]
## [code]envs[/code] contains the name and constructor of all environments in a [code]Dictionary[/code]
## structure.[br]
## A valid example may be something like this:
## [codeblock]
## static var envs: Dictionary={"shrine":shrine}
## [/codeblock]
static var envs: Dictionary={"shrine":shrine}
## Parameters for the introduction of scenes[br]
## [br]
## Most parameters apply to the camera, [code]"dsmd"[/code] stands for "Directional Max Shadow Distance".[br]
## A valid format may look like this:
## [codeblock]
## static var intros: Dictionary={"shrine":{"fov":[50,30, 5],"position:y":[450,50, 5],"dsmd":[700,100]}}
## [/codeblock]
static var intros: Dictionary={"shrine":{"fov":[50,30, 5],"position:y":[450,50, 5],"dsmd":[700,100]}}

## Shrine environment
static func shrine() -> Env:
    var env:=Env.new()
    env.add_child(preload("res://imports/environment/shrine2.blend").instantiate())
    env.position=Vector3(7,-49,7)
    env.rotation=Vector3(0,0,0)
    env.scale=Vector3(25,25,25)
    return env
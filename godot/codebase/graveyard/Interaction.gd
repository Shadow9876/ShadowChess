class_name Interaction
extends Control

signal finished
signal _inner_timeout
signal skipped_current

var fonts: PackedStringArray=[]
@onready var bg: ColorRect=$BG
@onready var text: Label=$Text
@onready var image: TextureRect=$Image
@onready var video: VideoStreamPlayer=$Video
@onready var timer: Timer=$Timer
var skipped:=false

func set_fonts(font_names: PackedStringArray) -> void:
    fonts=font_names

func clear() -> void:
    skipped=false
    Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
    bg.color=Color.BLACK
    text.hide()
    image.hide()
    video.hide()

func sanitise() -> void:
    Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
    finished.emit()


func _on_timer_timeout() -> void:
    timer.stop()
    _inner_timeout.emit()

func _on_video_finished() -> void:
    _on_timer_timeout()

#region Text

func display_text(txt: String, text_col:=Color.WHITE, bg_col:=Color.BLACK, animate:=true, time:=7.0) -> void:
    # Setup
    clear()
    # Background
    bg.color=bg_col
    # Action
    text.text=txt
    text.label_settings.font_color=text_col
    text.show()

    if animate:
        text.label_settings.font=SystemFont.new()
        for i in int(time):
            if skipped: break
            (text.label_settings.font as SystemFont).set_font_names([SRand.rand_choice(fonts)])
            timer.start(1.0)
            await _inner_timeout
    else:
        text.label_settings.font=null
        timer.start(time)
        await _inner_timeout
    # Cleanup
    sanitise()

#endregion

#region Image

func display_image(path: String, time:=7.0) -> void:
    # Setup
    clear()
    bg.color=Color.BLACK
    # Action
    image.texture=Util.load_or_crash(path)
    image.show()

    timer.start(time)
    await _inner_timeout
    # Cleanup
    sanitise()

#endregion

#region Video

func display_video(path: String) -> void:
    # Setup
    clear()
    bg.color=Color.BLACK
    # Action
    var stream:=VideoStreamTheora.new()
    stream.file=path
    video.stream=stream
    video.show()
    video.play()

    await _inner_timeout
    # Cleanup
    sanitise()

#endregion

#region Colour

func display_colour(col: Color, time: float) -> void:
    # Setup
    clear()
    # Action
    bg.color=col

    timer.start(time)
    await _inner_timeout
    # Cleanup
    bg.color=Color.BLACK
    sanitise()

#endregion

#region ChessGame

func display_chessgame(layout: String, turns: Array[bool], move_time: float, moves: Array, duration: float, camera_rotation: Vector3) -> void:
    # Setup
    clear()
    bg.hide()

    var accumulated:=0.0
    var chessGame:=ChessGame.ChessGame(layout.split("\n").size(),0,0,layout,false,false,Color(1,1,1,1),Color(0,0,0,1),"3D",true)
    chessGame.transition_time=0
    chessGame.turns=turns
    chessGame.play_intro=false
    chessGame.show_turn_indicator=false
    add_child(chessGame)
    chessGame.environment.set_dsmd(100)
    chessGame.camera.rotation_degrees=camera_rotation
    # Action
    for move: Array in moves:
        if skipped: break
        if !move.is_empty():
            var mv0: String=move[0]
            var mv1: String=move[1]
            var arr4:=Vec4.from_packed_arr(
                PackedInt32Array([
                    int(Util.keep_nums(mv0))-1,
                    Util.get_num_from_letter(Util.keep_letters(mv0))-1,
                    int(Util.keep_nums(mv1))-1,
                    Util.get_num_from_letter(Util.keep_letters(mv1))-1
                ])
            )
            var mv:=chessGame.chessInput.parse_input(arr4)
            if !mv.is_empty():
                chessGame.execute_input(mv[0])
        else:
            chessGame.environment.inc_time()
        timer.start(move_time)
        await _inner_timeout
        accumulated+=move_time

    if !skipped && duration-accumulated>0.041: # If waiting time is greater or equal than a frame
        timer.start(duration-accumulated)
        await _inner_timeout
    # Cleanup
    remove_child(chessGame)
    if is_instance_valid(chessGame):
        chessGame.queue_free()

    bg.show()
    sanitise()

#endregion

func _input(event: InputEvent) -> void:
    if event.is_action_pressed("skip"):
        skipped=true
        skipped_current.emit()
        _on_timer_timeout()
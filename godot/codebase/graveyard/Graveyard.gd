class_name Graveyard
extends Node3D

## [codeblock]play_music: Signal[String][/codeblock]
signal play_music(playlist: String)
signal stop_music
signal back_to_main_menu

#region Components

var game: ChessGame=null
var casualties: Array[Glib.PieceWithName]=[]
var casualties_queens: Array[Glib.PieceWithName]=[]
var casualties_kings: Array[Glib.PieceWithName]=[]
var counter:=0
var interaction: Interaction=null

var defeated_countries: PackedStringArray=[]

#endregion

func _ready() -> void:
    play_music.emit("garden")
    interaction=preload("res://codebase/graveyard/interaction.tscn").instantiate()
    continue_further()
    interaction.set_fonts(Util.get_distinct_sys_fonts())
    add_child(interaction)
    interaction.hide()

func continue_further(_winner: bool=false) -> void:
    if game!=null:
        if game in get_children():
            remove_child(game)
            game.queue_free()

    game=ChessGame.new()

    Util.err_handle(game.chessInput.exec_input.connect(exec_input))
    game.chessInput.exec_input.disconnect(game.execute_input)
    Util.discard(game.end_of_game.connect(continue_further))

    counter+=1

    game.light=Color.from_string("93d48f",Color.WHITE)
    game.dark=Color.from_string("1e321a",Color.BLACK)
    game.play_intro=false; game.move_hints=false
    game.can_change_past=false; game.can_access_history=false; game.can_plan=false
    var layout:=Util.read_or_crash("res://imports/graveyard/layouts/%d%s.btl" % [counter, "b" if counter==2 && len(casualties_kings)==0 else ""])
    if counter!=1:
        var c:=0
        for i in layout.length():
            if layout[i]=="p":
                c+=1
                if counter==2 && casualties_queens.size()<=c || counter==3 && casualties_kings.size()<=c:
                    layout[i]="x"
    game.layout=layout
    game.turns=[false]
    game.pass_time=false
    game.show_turn_indicator=false
    game.starting_time=counter*6
    game.transition_time=1.0
    game.chessInput.block_history()
    
    add_child(game)
    apply_graveyard_theme()

func apply_graveyard_theme() -> void:
    var cas_counter:=0 if counter==1 else 1
    for i: Piece in game.chessPieces.get_children():
        match i.type.get_char():
            "p":
                var col:=counter==1 && (casualties.size()<=cas_counter || casualties[cas_counter].pieceType.is_lower())
                Util.discard(i.assign_mesh_to_imported("res://imports/graveyard/models/cross.blend",true,col,false))
                if counter==1 && casualties.size()>cas_counter:
                    i.nameStr=casualties[cas_counter].name
                    i.titleStr=casualties[cas_counter].title
                elif counter==2:
                    i.nameStr=casualties_queens[cas_counter].name
                    i.titleStr=casualties_queens[cas_counter].title
                elif counter==3:
                    i.nameStr=casualties_kings[cas_counter].name
                    i.titleStr=casualties_kings[cas_counter].title
                cas_counter+=1
            "k":
                Util.discard(i.assign_mesh_to_imported("res://imports/graveyard/models/cross.blend",true,true,false,false))
                i.mesh=null
            "r","b":
                Util.discard(i.assign_mesh_to_imported("res://imports/graveyard/images/wall_%d.png" % [counter*6],false,true,false,false))
                i.scale=Vector3(1.1,1.1,1.1)
                i.position.y=2
                if counter==3:
                    var shadow:=MeshInstance3D.new()
                    shadow.mesh=BoxMesh.new()
                    shadow.scale=Vector3(2,1,0.5)
                    shadow.cast_shadow=GeometryInstance3D.SHADOW_CASTING_SETTING_SHADOWS_ONLY
                    shadow.position.y=-2
                    i.add_child(shadow)
                if i.type.get_char()=='b': i.rotate_y(deg_to_rad(90))
            "m":
                Util.discard(i.assign_mesh_to_imported("res://imports/graveyard/images/connector_wall_%d.png" % [counter*6],false,true,false,false))
                i.scale=Vector3(1.1,1.1,1.1)
                i.position.y=2
                if counter==3:
                    var shadow:=MeshInstance3D.new()
                    shadow.mesh=BoxMesh.new()
                    shadow.scale=Vector3(0.5,1,2)
                    shadow.cast_shadow=GeometryInstance3D.SHADOW_CASTING_SETTING_SHADOWS_ONLY
                    shadow.position.y=-2
                    i.add_child(shadow)
                    var shadow2:=MeshInstance3D.new()
                    shadow2.mesh=BoxMesh.new()
                    shadow2.scale=Vector3(1,1,0.5)
                    shadow2.cast_shadow=GeometryInstance3D.SHADOW_CASTING_SETTING_SHADOWS_ONLY
                    shadow2.position.y=-2
                    shadow2.position.x=0.5
                    i.add_child(shadow2)
                #i.position.x+=0.5
            "n":
                Util.discard(i.assign_mesh_to_imported("res://imports/graveyard/images/curved_wall_%d.png" % [counter*6],false,true,false,false))
                i.scale=Vector3(1.1,1.1,1.1)
                i.position.y=2
                #i.position.x+=0.5
                #i.position.z+=0.5
            "q":
                var file:=""
                if counter==2:
                    if casualties_queens.is_empty(): file="Great Tree"; i.position.y=8
                    elif casualties_queens.size()<=3: file="Grand Memorial 1"
                    else: file="Grand Memorial 2"
                else:
                    file="Grand Memorial 3" if casualties_kings.size()<15 else "Grand Memorial 4"
                    # Shadow of Grand Memorial
                    var piece:=Piece.construct(i.initial_position,Char.from_string("k"))[0]
                    piece.cast_shadow=GeometryInstance3D.SHADOW_CASTING_SETTING_SHADOWS_ONLY
                    self.game.chessPieces.add_child(piece)
                    # Pedestal
                    var pedestal:=MeshInstance3D.new(); pedestal.mesh=BoxMesh.new()
                    pedestal.scale=Vector3(2,1,2)
                    piece.add_child(pedestal)
                    # Make children cast only shadow as well
                    for p: MeshInstance3D in piece.get_children():
                        p.cast_shadow=GeometryInstance3D.SHADOW_CASTING_SETTING_SHADOWS_ONLY
                Util.discard(i.assign_mesh_to_imported("res://imports/graveyard/memorial/%s.png" % file,false,true,true,false))

func exec_input(inp: Vec6, from_history: bool = false) -> void:
    if inp.get_at(5)==0 || inp.get_at(5)==Pieces.get_val_from_name("king"):
        game.execute_input(inp,from_history)
    else:
        interaction.show()
        game.chessInput.block_input()
        # Interactions
        match Pieces.get_char_from_val(inp.get_at(5)).display():
            "p":
                var piece:=get_piece_at(Vector2i(inp.get_at(3),inp.get_at(2)))
                interaction.display_text("Nameless" if piece.is_empty() || piece[0].nameStr.is_empty() else "%s\n%s" % [piece[0].nameStr,piece[0].titleStr])
                await interaction.finished
                interaction.hide()
                game.chessInput.unblock_input()
            "q":
                stop_music.emit()
                # Interactions and endings
                if counter==2:
                    if casualties_queens.is_empty():
                        if casualties_kings.is_empty(): ending_A_mi_amor()
                        else: 
                            interaction.display_image("res://imports/graveyard/memorial/Tree Interaction.jpg")
                            await interaction.finished
                            interaction.hide()
                            game.chessInput.unblock_input()
                            play_music.emit("garden")
                    elif casualties_queens.size()<=3:
                        interaction.display_image("res://imports/graveyard/memorial/Interaction1.jpg")
                        await interaction.finished
                        if casualties_kings.is_empty(): ending_B_black_tulip()
                        else:
                            interaction.hide()
                            game.chessInput.unblock_input()
                            play_music.emit("garden")
                    else:
                        interaction.display_image("res://imports/graveyard/memorial/Interaction2.jpg")
                        await interaction.finished
                        if casualties_kings.is_empty(): ending_C_felicidad_olvidada()
                        else:
                            interaction.hide()
                            game.chessInput.unblock_input()
                            play_music.emit("garden")
                else:
                    if casualties_kings.size()<15: ending_D_resignacion()
                    else: ending_S_un_dia_en_la_vida()
            _:
                interaction.hide()
                game.chessInput.unblock_input()

func get_piece_at(pos: Vector2i) -> Array[Piece]:
    for i: Piece in game.chessPieces.get_children():
        if i.initial_position==pos:
            return [i]
    return []

func get_piece(type: Char) -> Array[Piece]:
    for i: Piece in game.chessPieces.get_children():
        if i.type.eq(type):
            return [i]
    return []

func ending_A_mi_amor() -> void:
    # Interactions
    interaction.display_video("res://imports/graveyard/memorial/great_tree.ogv")
    await interaction.finished
    interaction.display_text("Cuando éramos jóvenes prometimos...")
    await interaction.finished
    interaction.display_text("...que seríamos enterrados juntos.",Color.BLACK,Color.WHITE)
    await interaction.finished
    interaction.hide()

    play_music.emit("mi_amor")

    # Piece Movements
    var k:=get_piece(Char.from_string("K"))[0]
    var move:=Vec6.constr()
    move=Vec6.from_packed_arr(PackedInt32Array([k.initial_position.y,k.initial_position.x,5,3,-Pieces.get_val_from_name("king"),0]))
    game.transition_time=0
    game.unblock_input.disconnect(game.game_unblock_input)
    game.execute_input(move)
    game.transition_time=1
    var q:=Piece.construct(Vector2i(2,5),Char.from_string("r"))[0]
    Util.discard(q.assign_mesh(Char.from_string("Q")))
    game.chessPieces.add_child(q)
    if !is_inside_tree(): return
    await get_tree().create_timer(2.0).timeout
    move=Vec6.from_packed_arr(PackedInt32Array([5,3,5,4,-Pieces.get_val_from_name("king"),0]))
    await game.execute_input(move)
    move=Vec6.from_packed_arr(PackedInt32Array([5,4,5,5,-Pieces.get_val_from_name("king"),0]))
    await game.execute_input(move)
    move=Vec6.from_packed_arr(PackedInt32Array([5,2,5,3,Pieces.get_val_from_name("rook"),0]))
    await game.execute_input(move)
    if !is_inside_tree(): return
    await get_tree().create_timer(2.0).timeout
    var illusion: Vec6=Vec6.from_packed_arr(PackedInt32Array([5,3,5,5,Pieces.get_val_from_name("rook"),0]))
    game.chessPieces.transition([illusion],0.75)
    if !is_inside_tree(): return
    await get_tree().create_timer(0.5).timeout
    game.transition_time=3
    move=Vec6.from_packed_arr(PackedInt32Array([5,5,1,7,-Pieces.get_val_from_name("king"),0]))
    await game.execute_input(move)
    illusion=Vec6.from_packed_arr(PackedInt32Array([5,5,5,7,Pieces.get_val_from_name("rook"),0]))
    await game.chessPieces.transition([illusion],0.5)
    game.transition_time=0.4
    move=Vec6.from_packed_arr(PackedInt32Array([1,7,0,6,-Pieces.get_val_from_name("king"),0]))
    await game.execute_input(move)
    illusion=Vec6.from_packed_arr(PackedInt32Array([5,7,0,7,Pieces.get_val_from_name("rook"),0]))
    await game.chessPieces.transition([illusion],0.5)
    game.transition_time=1.0

    #Final Choice
    game.chessInput.unblock_input()
    await game.chessInput.exec_input
    # Credits
    roll_credits("")

func ending_B_black_tulip() -> void:
    interaction.display_text("...que seríamos enterrados juntos.")
    await interaction.finished
    interaction.hide()
    roll_credits("black_tulip")

func ending_C_felicidad_olvidada() -> void:
    interaction.display_text("...que seríamos enterrados juntos.")
    await interaction.finished
    interaction.hide()
    roll_credits("forgotten")

func ending_D_resignacion() -> void:
    interaction.display_image("res://imports/graveyard/memorial/Interaction3.jpg")
    await interaction.finished
    interaction.hide()
    roll_credits("resignacion")

func ending_S_un_dia_en_la_vida() -> void:
    # Interactions
    interaction.display_image("res://imports/graveyard/memorial/Interaction4.jpg")
    await interaction.finished
    interaction.display_image("res://imports/graveyard/memorial/Interaction5.jpg")
    await interaction.finished
    interaction.hide()

    # Piece Movements
    var q:=get_piece(Char.from_string("q"))[0]
    Util.discard(q.assign_mesh_to_imported("res://imports/graveyard/memorial/Grand Memorial 5.png",false,true,true,false))
    var k:=get_piece(Char.from_string("K"))[0]
    var move:=Vec6.constr()
    move=Vec6.from_packed_arr(PackedInt32Array([k.initial_position.y,k.initial_position.x,5,4,-Pieces.get_val_from_name("king"),0]))
    game.transition_time=0
    game.unblock_input.disconnect(game.game_unblock_input)
    game.execute_input(move)
    game.transition_time=1
    if !is_inside_tree(): return
    await get_tree().create_timer(2.0).timeout
    move=Vec6.from_packed_arr(PackedInt32Array([5,4,4,5,-Pieces.get_val_from_name("king"),0]))
    await game.execute_input(move)
    move=Vec6.from_packed_arr(PackedInt32Array([4,5,3,5,-Pieces.get_val_from_name("king"),0]))
    await game.execute_input(move)
    move=Vec6.from_packed_arr(PackedInt32Array([3,5,2,6,-Pieces.get_val_from_name("king"),0]))
    await game.execute_input(move)
    move=Vec6.from_packed_arr(PackedInt32Array([2,6,1,7,-Pieces.get_val_from_name("king"),0]))
    await game.execute_input(move)
    move=Vec6.from_packed_arr(PackedInt32Array([1,7,0,7,-Pieces.get_val_from_name("king"),0]))
    await game.execute_input(move)
    if !is_inside_tree(): return
    await get_tree().create_timer(2.0).timeout
    var illusion: Vec6=Vec6.from_packed_arr(PackedInt32Array([0,7,-1,-1,-Pieces.get_val_from_name("king"),0]))
    await game.chessPieces.transition([illusion],1.0)

    # Credits
    roll_credits("life")

func roll_credits(music: String) -> void:
    play_music.emit(music)
    if !is_inside_tree(): return
    var time_of_credits:=(get_parent().get_parent() as Root).music.get_time_left_with_possible_restart()
    var individual_time:=(time_of_credits-1.5)/(defeated_countries.size()+1)

    interaction.show()
    await interaction.display_text("Defeated Countries",Color.WHITE,Color.BLACK,false,individual_time)
    for country in defeated_countries:
        await interaction.display_text(country,Color.WHITE,Color.BLACK,false,individual_time)
    
    stop_music.emit()
    await interaction.display_text("ShadowChess",Color.WHITE,Color.BLACK,false,4)
    await interaction.display_text("Created by\n...",Color.WHITE,Color.BLACK,false,3)
    await interaction.display_text("Shadow9876",Color.WHITE,Color.BLACK,false,3)

    back_to_main_menu.emit()
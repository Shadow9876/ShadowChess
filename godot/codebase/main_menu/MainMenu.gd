class_name MainMenu
extends Control

signal load_game_signal
## [codeblock]new_game_signal: Signal[String][/codeblock]
signal new_game_signal(story_name: String)
## [codeblock]set_music_volume: Signal[int][/codeblock]
signal set_music_volume(volume_percentage: int)

@onready var bg: TextureRect=$Background
@onready var main_menu: Control=$"Main Menu"
@onready var new_game: Control=$"New Game"
@onready var settings: Control=$Settings

@onready var continue_button: Button=$"Main Menu/Buttons/Continue"

@onready var music_label: Label=$"Settings/VBoxContainer/Music Label"
@onready var scrolling_label: Label=$"Settings/VBoxContainer/Scrolling Label"
@onready var music_slider: HSlider=$Settings/VBoxContainer/Music
@onready var scrolling_slider: HSlider=$Settings/VBoxContainer/Scrolling
@onready var animation_itemlist: ItemList=$Settings/VBoxContainer/Animation
@onready var turn_indicator_checkbutton: CheckButton=$"Settings/VBoxContainer/Turn Indicator"

var hover_message: Label=Label.new()

func _ready() -> void:
	add_child(hover_message)
	hover_message.label_settings=LabelSettings.new()
	hover_message.label_settings.outline_color=Color.BLACK
	hover_message.label_settings.outline_size=10
	hover_message.label_settings.font_size=20

func reset(apply: bool=true) -> void:
	hover_message.text=""
	main_menu.show()
	new_game.hide()
	settings.hide()
	if apply: apply_bg()

func apply_bg() -> void:
	var texture: Array[Resource]=Util.try_load("res://imports/ui/loading_screens/%s.jpg" % ["Dark Shrine" if SRand.rand(10)==0 else "Shrine"])
	if !texture.is_empty():
		bg.texture=texture[0]

#region Main Menu

func _on_new_game_pressed() -> void:
	main_menu.hide()
	new_game.show()

func _on_continue_pressed() -> void:
	main_menu.hide()
	load_game_signal.emit()

func _on_settings_pressed() -> void:
	main_menu.hide()
	settings.show()

func _on_quit_pressed() -> void:
	get_tree().quit()

func enable_continue() -> void:
	continue_button.disabled=false

func disable_continue() -> void:
	continue_button.disabled=true

#endregion

#region new_game

func _on_game_pressed() -> void:
	new_game_signal.emit("game")

func _on_real_life_pressed() -> void:
	new_game_signal.emit("real_life")

func _on_new_back_pressed() -> void:
	new_game.hide()
	main_menu.show()

#endregion

#region Credits

func _on_credits_pressed() -> void:
	var cred:=AcceptDialog.new()
	cred.title="Credits and Licenses"
	cred.dialog_text="""
	Credits:\n
		ShadowChess uses art from the following artists and/or authors:
		Duane Raver, USFWS,
		Vassil,
		Mathias Appel,
		USFWS,
		Harrison, Ause, & Co.,
		Thomsonmg2000,
		Mareve,
		and Jon Sullivan.
		More information can be found inside CREDITS.md in the repository
		(https://codeberg.org/Shadow9876/ShadowChess)\n
	Licenses:\n
		The code of ShadowChess is licensed under GPLv3.
		The art of ShadowChess (when not stated otherwise) is under CC BY-SA 4.0.
	"""
	cred.ok_button_text="Back to Main Menu"
	add_child(cred)
	cred.popup_centered()
	Util.err_handle(cred.confirmed.connect(func() -> void: remove_child(cred); cred.queue_free()))

func _on_credits_mouse_entered() -> void:
	hover_message.text="Credits"


func _on_credits_mouse_exited() -> void:
	hover_message.text=""


func _input(event: InputEvent) -> void:
	if event is InputEventMouse:
		var pos:=(event as InputEventMouse).position
		hover_message.position=Vector2(pos.x+40,pos.y)

#endregion

#region Information

func _on_information_pressed() -> void:
	var inf:=AcceptDialog.new()
	inf.title="???"
	inf.dialog_text="""
	Esc - .a.. t. m..n ..n.
	Lef. Mou.e B.tt.n - .s.d .or g..b.i.. .i.c.. .nd .ov.ng ...m
	Rig.t M..s. .ut.on - ..ed f.r p.a.n... ..i.h .et. ...r. ...ul.te .ov.s
	Wh.el up/d.wn - .... f.. ..w..ch... ...es ... .v.. .h...... ....
	"""
	inf.ok_button_text="Back"
	add_child(inf)
	inf.popup_centered()
	Util.err_handle(inf.confirmed.connect(func() -> void: remove_child(inf); inf.queue_free()))


func _on_information_mouse_entered() -> void:
	hover_message.text="???"


func _on_information_mouse_exited() -> void:
	hover_message.text=""

#endregion

#region Settings

func adjust_music_slider(value: float) -> void:
	music_slider.value=value

func adjust_scrolling_slider(value: float) -> void:
	scrolling_slider.value=value

func select_animation_item(ind: int) -> void:
	animation_itemlist.select(ind,false)
	_on_animation_item_selected(ind)

func set_turn_indicator_checkbutton(val: bool) -> void:
	turn_indicator_checkbutton.button_pressed=val

func _on_settings_button_pressed() -> void:
	(get_parent() as Root).save_user_prefs()
	settings.hide()
	main_menu.show()

func _on_music_value_changed(value: float) -> void:
	set_music_volume.emit(int(value))
	music_label.text="Music Volume - %d%%" % [int(value)]

func _on_animation_item_selected(index: int) -> void:
	Root.environmental_animations=index as Glib.EnvironmentalAnimation	

func _on_turn_indicator_toggled(toggled_on: bool) -> void:
	Root.turn_indicator_on_lefthand_side=toggled_on

func _on_scrolling_value_changed(value: float) -> void:
	Root.scroll_sensitivity=int(value)
	scrolling_label.text="Scrolling Sensitivity - %d" % [int(value)]

#endregion

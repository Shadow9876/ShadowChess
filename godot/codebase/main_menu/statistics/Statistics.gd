class_name Statistics
extends Control

#region Signals

signal back_to_main_menu
signal play_music

#endregion

#region Badges

@onready var loser: TextureRect=$"Image/VBoxContainer/Columns/Col3/HBoxContainer/Col1/Loser"
@onready var tactician: TextureRect=$"Image/VBoxContainer/Columns/Col3/HBoxContainer/Col2/Tactician"
@onready var badge_master: TextureRect=$"Image/VBoxContainer/Columns/Col3/HBoxContainer/Col3/Badge master"
@onready var cheater: TextureRect=$"Image/VBoxContainer/Columns/Col3/HBoxContainer/Col1/Cheater"
@onready var midnight_expert: TextureRect=$"Image/VBoxContainer/Columns/Col3/HBoxContainer/Col2/Midnight expert"
@onready var promoted: TextureRect=$"Image/VBoxContainer/Columns/Col3/HBoxContainer/Col3/Promoted"
@onready var fmous: TextureRect=$"Image/VBoxContainer/Columns/Col3/HBoxContainer/Col1/Fmous"

#endregion

#region Labels

@onready var enemy_pieces_taken: Label=$Image/VBoxContainer/Columns/Col2/Enemy
@onready var pieces_lost: Label=$Image/VBoxContainer/Columns/Col2/Pieces
@onready var fastest_victory: Label=$Image/VBoxContainer/Columns/Col2/Fastest
@onready var midnights_experienced: Label=$Image/VBoxContainer/Columns/Col2/Midnights
@onready var games_played: Label=$Image/VBoxContainer/Columns/Col2/Games
@onready var win_lose_ratio: Label=$Image/VBoxContainer/Columns/Col2/Ratio
@onready var moves_made: Label=$Image/VBoxContainer/Columns/Col2/Moves
@onready var promotions: Label=$Image/VBoxContainer/Columns/Col2/Promotions
@onready var used_planning: Label=$Image/VBoxContainer/Columns/Col2/Planning
@onready var changed_past: Label=$Image/VBoxContainer/Columns/Col2/Past

#endregion

#region Ready

var hover_message: Label=null
var settings:=LabelSettings.new()

func _ready() -> void:
	settings.outline_color=Color.BLACK
	settings.outline_size=6
	settings.font_size=25
	reset_hover_message()
	display_data()

func display_data() -> void:
	play_music.emit("credits")
	await interpolate(enemy_pieces_taken,get_enemy_pieces_taken())
	await interpolate(pieces_lost,get_pieces_lost())
	await interpolate(fastest_victory,get_fastest_victory())
	await interpolate(midnights_experienced,get_midnights_experienced())
	await interpolate(games_played,get_games_played())
	await interpolate(win_lose_ratio,get_win_lose_ratio(),true)
	await interpolate(moves_made,get_moves_made())
	await interpolate(promotions,get_promotions())
	await interpolate(used_planning,get_planning_used())
	await interpolate(changed_past,get_past_changed())
	if !is_inside_tree(): return
	await get_tree().create_timer(1.0).timeout
	display_badges()

func display_badges() -> void:
	await interpolate_badge(is_loser(),loser,"S")
	await interpolate_badge(is_tactician(),tactician,"A")
	await interpolate_badge(is_cheater(),cheater,"C")
	await interpolate_badge(is_midnight_expert(),midnight_expert,"D")
	await interpolate_badge(is_promoted(),promoted,"E")
	await interpolate_badge(is_fmous(),fmous,"F")
	await interpolate_badge(is_badge_master(),badge_master,"B")

#endregion

#region Data

var games: Array[GameData]=[]
var wins:=0
var losses:=0

func get_enemy_pieces_taken() -> int:
	var p:=0
	for game in games:
		for move in game.main_line:
			if move.get_at(5)>0: p+=1
	return p

func get_pieces_lost() -> int:
	var p:=0
	for game in games:
		for move in game.main_line:
			if move.get_at(5)<0: p+=1
	return p

func get_fastest_victory() -> int:
	var m:=-1
	for game in games:
		if !game.main_line.is_empty() && game.main_line[-1].get_at(4)<0 && (m==-1 || game.main_line.size()<m):
			m=game.main_line.size()
	return m

func get_midnights_experienced() -> int:
	var m:=0
	for i in games: m+=i.midnights
	return m

func get_games_played() -> int:
	return games.size()

func get_win_lose_ratio() -> int:
	return round((float(wins)/(wins+losses))*100)

func get_moves_made() -> int:
	var m:=0
	for game in games:
		for move in game.main_line:
			if move.get_at(4)<0: m+=1
	return m

func get_promotions() -> int:
	var p:=0
	for game in games:
		for move in game.main_line:
			if move.get_at(4)<0 && move.get_at(6)==1: p+=1
	return p

func get_planning_used() -> int:
	var p:=0
	for game in games: p+=game.used_planning
	return p

func get_past_changed() -> int:
	var p:=0
	for game in games: p+=game.changed_past
	return p

func is_loser() -> bool:
	for i in games.size():
		for j in range(i+1,games.size()):
			if !games[i].main_line.is_empty() && games[i].main_line[-1].get_at(4)>0 && GameData.eq(games[i],games[j]): return true
	return false

func is_tactician() -> bool:
	for game in games:
		if game.used_planning==0: return false
	return true

func is_badge_master() -> bool:
	return is_loser() && is_tactician() && is_cheater() && is_midnight_expert() && is_promoted() && is_fmous()

func is_cheater() -> bool:
	for game in games:
		if !game.main_line.is_empty() && game.main_line[-1].get_at(4)<0 && game.main_line.size()<=7: return true
	return false

func is_midnight_expert() -> bool:
	return get_midnights_experienced()>=6

func is_promoted() -> bool:
	var prom:=[-Pieces.get_val_from_name("knight"),-Pieces.get_val_from_name("bishop"),-Pieces.get_val_from_name("rook"),-Pieces.get_val_from_name("queen"),-Pieces.get_val_from_name("king")]
	for game in games:
		for move in game.main_line:
			if move.get_at(6)==1:
				var ind:=prom.find(move.get_at(4))
				if ind>=0: prom.remove_at(ind)
	return prom.is_empty()

func is_fmous() -> bool:
	for game in games:
		if game.main_line.is_empty(): continue
		var f_file:=false
		for move in game.main_line:
			if move.get_at(3)==5: f_file=true
			break
		if !f_file: return false
	return true

#endregion

#region Badges' Hover Message

func reset_hover_message() -> void:
	if hover_message!=null && is_instance_valid(hover_message) && hover_message in get_children():
		remove_child(hover_message)
		hover_message.queue_free()
	hover_message=Label.new()
	hover_message.label_settings=settings
	add_child(hover_message)

func _on_loser_mouse_entered() -> void:
	reset_hover_message()
	hover_message.text="Lose the same game twice with identical moves"

func _on_loser_mouse_exited() -> void:
	hover_message.text=""


func _on_tactician_mouse_entered() -> void:
	reset_hover_message()
	hover_message.text="Use planning in every game you play"

func _on_tactician_mouse_exited() -> void:
	hover_message.text=""


func _on_badge_master_mouse_entered() -> void:
	reset_hover_message()
	hover_message.text="Get every other badge"

func _on_badge_master_mouse_exited() -> void:
	hover_message.text=""


func _on_cheater_mouse_entered() -> void:
	reset_hover_message()
	hover_message.text="Win a game in 7 moves or less"

func _on_cheater_mouse_exited() -> void:
	hover_message.text=""


func _on_midnight_expert_mouse_entered() -> void:
	reset_hover_message()
	hover_message.text="Experience 6 or more midnights"

func _on_midnight_expert_mouse_exited() -> void:
	hover_message.text=""


func _on_promoted_mouse_entered() -> void:
	reset_hover_message()
	hover_message.text="Promote to every available piece"

func _on_promoted_mouse_exited() -> void:
	hover_message.text=""


func _on_fmous_mouse_entered() -> void:
	reset_hover_message()
	hover_message.text="Make a piece land on the F file in every game"

func _on_fmous_mouse_exited() -> void:
	hover_message.text=""

#endregion

#region Input

func _on_button_pressed() -> void:
	back_to_main_menu.emit()


func _input(event: InputEvent) -> void:
	if event is InputEventMouse:
		var pos:=(event as InputEventMouse).position
		if hover_message!=null && is_instance_valid(hover_message) && hover_message in get_children():
			hover_message.position=Vector2(pos.x-hover_message.size.x/2,pos.y+20)
#endregion

#region Interpolate

func interpolate(label: Label, val: int, percent: bool=false) -> void:
	if !is_inside_tree(): return
	if val==0:
		label.text="0"
		if !is_inside_tree(): return
		await get_tree().create_timer(1.0).timeout
	else:
		for i in val+1:
			label.text="%d%s" % [i,"%" if percent else ""]
			if !is_inside_tree(): return
			await get_tree().create_timer(2.0/val).timeout

func interpolate_badge(condition: bool, rect: TextureRect, badge: String) -> void:
	if condition:
		for __ in 2:
			rect.texture=Util.load_or_crash("res://imports/ui/statistics/badge%s.png" % badge)
			if !is_inside_tree(): return
			await get_tree().create_timer(0.5).timeout
			rect.texture=Util.load_or_crash("res://imports/ui/statistics/unbadge%s.png" % badge)
			if !is_inside_tree(): return
			await get_tree().create_timer(0.5).timeout
		rect.texture=Util.load_or_crash("res://imports/ui/statistics/badge%s.png" % badge)
		if !is_inside_tree(): return
		await get_tree().create_timer(0.5).timeout

#endregion

class_name GameSupervisor
extends Node

#region Signals

signal save_game
signal delete_save
signal CampaignLost
## [codeblock]play_music: Signal[String,String][/codeblock]
signal play_music(playlist: String, start_with: String)
signal stop_music

#endregion

#region Game Components

var map:=Map.new()
var path:="res://imports/maps/"

var chessGame: ChessGame
var tmp_colour:=Color.BLACK

## Interaction for Events
@onready var interaction: Interaction=preload("res://codebase/graveyard/interaction.tscn").instantiate()

#endregion

#region Flags

@export var story_name: String="" : 
    set(val):
        if story_name!=val:
            var tmp:=story_name
            story_name=val
            if !set_flags():
                printerr("'%s' was not a valid story name!" % val)
                story_name=tmp
            else:
                load_events()
                EndingConnector.connect_ending(self)

var white_depth: int=4
var black_depth: int=4
var is_white_ai: bool=true
var is_black_ai: bool=false
var death_on_loss:=false
var can_change_past:=true
var move_hints:=false
var base_saturation: float=1
var map_name: String = "" :
    set(val):
        if val!=map_name:
            map_name=val
            assign_map()
            load_special_names()

var events: Glib.EventSettings=Glib.EventSettings.ALL
var delete_save_on_death:=false

var on_state_end: Glib.StateEnd=Glib.StateEnd.GRAVEYARD

#endregion

#region Names and Casualties

var casualties: Array[Glib.PieceWithName]=[]
var survivors: Array[Glib.PieceWithName]=[]
var current_survivors:Array[Glib.PieceWithName]=[]
var king_count:=1
var queen_count:=1

var defeated_countries: PackedStringArray=[]

## [codeblock]events_dict: Dictionary[int,PackedStringArray][/codeblock]
## where each [code]String[/code] in [code]PackedStringArray[/code] is like
## [code]str1 => str2[/code]
var events_dict: Dictionary={}

##[codeblock]special_names: Dictionary[String,String2][/codeblock]
## where [code]String2: "Name\nTitle"[/code]
var special_names: Dictionary={}

var games: Array[GameData]=[]
var wins:=0
var losses:=0

var death_index:=0
var death_messages: PackedStringArray=[]

#endregion

#region Ready

var skipped_interaction:=false

func _skip_interaction() -> void:
    skipped_interaction=true

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
    assign_map()
    if map!=null: map.map3d.environment.set_saturation(1.0)
    add_child(interaction)
    interaction.hide()
    Util.err_handle(interaction.skipped_current.connect(_skip_interaction))

func assign_map() -> void:
    map.base=weakref(self)
    if map in get_children():
        remove_child(map)
    if map.start_game.is_connected(start_game):
        map.start_game.disconnect(start_game)
    map.map_name=map_name
    add_child(map)
    var __:=map.start_game.connect(start_game)

func set_flags() -> bool:
    var file:=path.replace("maps","stories")+story_name+".json"
    var data:=Util.try_read(file)
    if data.is_empty():
        printerr("File %s does not exist!" % file)
        return false
    var dict: Dictionary=JSON.parse_string(data[0])
    for key: String in dict:
        match key.to_lower():
            "name": pass
            "white_depth":
                var f: float=dict[key]
                self.white_depth=int(f)
            "black_depth":
                var f: float=dict[key]
                self.black_depth=int(f)
            "is_white_ai":
                self.is_white_ai=dict[key]
            "is_black_ai":
                self.is_black_ai=dict[key]
            "move_hints":
                self.move_hints=dict[key]
            "death_on_loss":
                self.death_on_loss=dict[key]
            "can_change_past":
                self.can_change_past=dict[key]
            "map":
                self.map_name=dict[key]
            "base_saturation":
                self.base_saturation=dict[key]
            "death_messages":
                var arr: Array=dict[key]
                death_messages.clear()
                for i: String in arr:
                    Util.discard(death_messages.push_back(i))
            "events":
                match dict[key]:
                    "all":
                        self.events=Glib.EventSettings.ALL
                    "none":
                        self.events=Glib.EventSettings.NONE
                    _:
                        self.events=Glib.EventSettings.TEXT_ONLY
            "delete_save_on_death":
                self.delete_save_on_death=dict[key]
            "state_end":
                match dict[key]:
                    "graveyard":
                        self.on_state_end=Glib.StateEnd.GRAVEYARD
                    "statistics":
                        self.on_state_end=Glib.StateEnd.STATISTICS
                    _:
                        self.on_state_end=Glib.StateEnd.CUSTOM
            _:
                printerr("Unknown flag specified in '%s'! Namely attribute: '%s' with value: '%s'!" % [file, key, dict[key]])

    return true

func load_events() -> void:
    events_dict.clear()
    var data:=Util.read_or_crash(path+map_name+"/data/events.json")
    var dict: Dictionary=JSON.parse_string(data)
    for key: String in dict:
        for ev: String in dict[key]:
            if "DEFEATED_COUNTRIES" in dict[key][ev]:
                var tag: String=dict[key][ev]["DEFEATED_COUNTRIES"]
                dict[key][ev]["DEFEATED_COUNTRIES"]=map.countries.get_country(tag)[0].name
        events_dict[int(key)]=dict[key]

func load_special_names() -> void:
    # macros: Dictionary[String,String]
    var names_and_chars:={}
    var file:=path+map_name+"/data/special_names.json"
    var data:=Util.try_read(file)
    if data.is_empty(): return
    var dict: Dictionary=JSON.parse_string(data[0])
    for key: String in dict:
        var piece_name:=key.split(" ")[0]
        var col:=piece_name.split("_")[0]
        var type:=piece_name.split("_")[1]
        var ch:=Pieces.get_char_from_name(type)
        ch=ch.upper() if col.to_lower()=="black" else ch.lower()
        names_and_chars[piece_name]=ch.display()
    for key: String in dict:
        var val: Array=dict[key]
        var arr: PackedStringArray=[]
        for s: String in val:
            var out_s:=s
            for n_key: String in names_and_chars:
                var n_val: String=names_and_chars[n_key]
                out_s=out_s.replace(n_key,n_val)
            Util.discard(arr.push_back(out_s))
        var new_key:=key
        for n_key: String in names_and_chars:
            var n_val: String=names_and_chars[n_key]
            new_key=new_key.replace(n_key,n_val)
        special_names[new_key]=arr

#endregion

#region Game Launcher

func start_game(tag: Tag, ind: int, col: Color) -> void:
    # Delete save
    if delete_save_on_death:
        delete_save.emit()
    # Cache opponent
    tmp_colour=col
    # Adjust children
    remove_child(map)
    # Launch game with parametersget_country
    # Get battle
    var with_tag:=Util.try_read(path+map_name+"/data/battles/%d_%s.btl" % [ind, tag.inner])
    var level: String
    if !with_tag.is_empty():
        level=with_tag[0]
    else:
        level=Util.read_or_crash(path+map_name+"/data/battles/%d.btl" % ind)
    # Construct game
    chessGame=ChessGame.ChessGame(level.split("\n")[0].split(" ").size(),self.white_depth,self.black_depth,level,is_white_ai,is_black_ai)
    # Get and apply theme
    with_tag=Util.try_read(path+map_name+"/data/themes/%d_%s.json" % [ind, tag.inner])
    var theme: String
    if !with_tag.is_empty():
        theme=with_tag[0]
    else:
        theme=Util.read_or_crash(path+map_name+"/data/themes/%d.json" % ind)
    chessGame.apply_theme(theme)
    # Get names
    var names_path:=path+map_name+"/data/names/%d_%s.json" % [ind, tag.inner] if FileAccess.file_exists(path+map_name+"/data/names/%d_%s.json" % [ind, tag.inner]) else path+map_name+"/data/names/%d.json" % ind
    # Adjust intro (animation)
    if Root.environmental_animations!=Glib.EnvironmentalAnimation.NONE && chessGame.environment.outside not in Root.played_animations:
        Util.discard(Root.played_animations.push_back(chessGame.environment.outside))
    else:
        chessGame.play_intro=false
    # Adjust a few parameters
    chessGame.move_hints=self.move_hints
    chessGame.base_saturation=self.base_saturation
    chessGame.can_change_past=self.can_change_past
    # Add child
    add_child(chessGame)
    # Set Scroll Sensitivity
    chessGame.chessInput.scroll_accumulation_scale=Root.scroll_sensitivity
    # Grant names
    chessGame.chessPieces.grant_names(get_names(names_path))
    chessGame.chessPieces.grant_custom_names(get_special_names())
    # Play music
    if !chessGame.music_playlist.is_empty():
        play_music.emit(chessGame.music_playlist,chessGame.music_start_with)
    # Connect end of game to end_of_game function
    Util.err_handle(chessGame.end_of_game.connect(end_of_game))

func end_of_game(winner: bool) -> void:
    # Stop music
    if !chessGame.music_playlist.is_empty():
        stop_music.emit()
    # Get game data
    games.push_back(chessGame.get_game_data())
    # Get country that the player attacked
    var country: Country=map.countries.countries[tmp_colour]
    # Death on loss
    if death_on_loss && winner && country.defeat_message.is_empty():
        # Normal death
        if death_index>=death_messages.size() || chessGame.instant_death:
            CampaignLost.emit()
            return
        # Interaction
        interaction.show()
        interaction.display_text(death_messages[death_index],Color.WHITE,Color.BLACK,false,5)
        await interaction.finished
        interaction.hide()
        # Lives
        if death_index<death_messages.size()-1:
            await chessGame.reset()
            death_index+=1
            if !chessGame.music_playlist.is_empty():
                play_music.emit(chessGame.music_playlist,chessGame.music_start_with)
        # Death (no lives remaining)
        else:
            CampaignLost.emit()
    # No death on loss
    else:
        # Black won (successful game)
        if !winner:
            self.wins+=1
            for casualty in chessGame.get_casualties():
                if casualty.pieceType.get_char()=="K" && !casualty.title.is_empty():
                    king_count+=1
                elif casualty.pieceType.get_char()=="Q" && !casualty.title.is_empty():
                    queen_count+=1
            casualties.append_array(chessGame.get_casualties())
            survivors.append_array(chessGame.get_survivors())
            current_survivors=chessGame.get_survivors()
        # White won (unsuccessful game)
        else:
            self.losses+=1
        # Adjust children
        remove_child(chessGame)
        chessGame.queue_free()
        chessGame=null
        add_child(map)
        map.map3d.environment.set_saturation(1.0)
        # Adjust game result on map
        await map.rectify_choice(tmp_colour,!winner)
        # Save
        save_game.emit()

#endregion

#region Events

func execute_events(num: int) -> void:
    if num not in events_dict: return
    var evs: Dictionary=events_dict[num]
    for ev: String in evs:
        var event: Dictionary=evs[ev]
        if is_event_condition_true(event):
            await exec_which_event(event)

func is_event_condition_true(event: Dictionary) -> bool:
    if "SURVIVORS" in event || "CASUALTIES" in event:
        var is_surv:="SURVIVORS" in event
        var nameStr:=""
        if is_surv: nameStr=event["SURVIVORS"]
        else: nameStr=event["CASUALTIES"]
        if nameStr.to_upper()==nameStr && ("white" in nameStr.to_lower() || "black" in nameStr.to_lower()):
            var curr_arr:=current_survivors if is_surv else casualties
            var col:=nameStr.split("_")[0]
            var type:=nameStr.split("_")[1]
            var ch:=Pieces.get_char_from_name(type)
            ch=ch.upper() if col.to_lower()=="black" else ch.lower()
            for piece in curr_arr:
                if piece.pieceType.eq(ch):
                    return true
        else:
            var curr_arr:=survivors if is_surv else casualties
            for piece in curr_arr:
                if piece.name==nameStr.replace("_"," "):
                    return true
    elif "DEFEATED_COUNTRIES" in event:
        var country: String=event["DEFEATED_COUNTRIES"]
        if country in defeated_countries:
            return true
    return false

func exec_which_event(d: Dictionary) -> void:
    if "annex" in d:
        var arr: Array=d["annex"]
        var arr2: PackedStringArray=[]
        for j: String in arr:
            Util.discard(arr2.push_back(j))
        await execute_event("annex",arr2)
    else:
        for i in PackedStringArray(["json","video","image","text","animated_text"]):
            if i in d:
                var arr: Array=[d[i]]
                var arr2: PackedStringArray=[]
                for j: String in arr:
                    Util.discard(arr2.push_back(j))
                await execute_event(i,arr2)

func execute_event(event: String, args: PackedStringArray) -> void:
    skipped_interaction=false
    match event:
        "annex":
            var c1:=map.countries.get_country(args[0])
            var c2:=map.countries.get_country(args[1])
            if !c1.is_empty() && c1[0].colour in map.countries.countries && !c2.is_empty() && c2[0].colour in map.countries.countries:
                map.countries.annex_country(c1[0].colour,c2[0].colour,true)
        "video":
            if events!=Glib.EventSettings.ALL: return
            interaction.show()
            interaction.display_video("res://imports/events/%s.ogv" % args[0])
            play_music.emit(args[0])
            await interaction.finished
            interaction.hide()
            stop_music.emit()
        "image":
            if events!=Glib.EventSettings.ALL: return
            interaction.show()
            var pathStr:=""
            for ext: String in ["png","jpg","jpeg","svg"]:
                var pth:="res://imports/events/%s.%s" % [args[0],ext]
                if ResourceLoader.exists(pth):
                    pathStr=pth
                    break
            if pathStr.is_empty():
                printerr("There was no such image as 'res://imports/events/%s.*' during event!" % args[0])
            else:
                interaction.display_image(pathStr)
                await interaction.finished
            interaction.hide()
        "animated_text":
            if events==Glib.EventSettings.NONE: return
            interaction.show()
            interaction.display_text(args[0])
            await interaction.finished
            interaction.hide()
        "text":
            if events==Glib.EventSettings.NONE: return
            interaction.show()
            interaction.display_text(args[0],Color.WHITE,Color.BLACK,false)
            await interaction.finished
            interaction.hide()
        "json":
            if events!=Glib.EventSettings.ALL: return
            interaction.show()
            var d: Dictionary=JSON.parse_string(Util.read_or_crash("res://imports/events/%s.json" % args[0]))
            for key: String in d:
                if skipped_interaction: break
                if key=="music":
                    play_music.emit(d[key])
                    continue
                var dict: Dictionary=d[key]
                if "img" in dict:
                    var img: String=dict["img"]
                    var dur: float=dict["duration"]
                    interaction.display_image(img,dur)
                    await interaction.finished
                elif "video" in dict:
                    var v: String=dict["video"]
                    interaction.display_video(v)
                    await interaction.finished
                elif "chess_game" in dict:
                    var d2: Dictionary=dict["chess_game"]
                    var turns: Array=d2["turns"]
                    var turns2: Array[bool]=[]
                    for i: bool in turns:
                        turns2.push_back(i)
                    var rotation: Vector3=Vector3(-90,0,0)
                    if "camera_rotation_x" in d2:
                        rotation.x=d2["camera_rotation_x"]
                    if "camera_rotation_y" in d2:
                        rotation.y=d2["camera_rotation_y"]
                    if "camera_rotation_z" in d2:
                        rotation=d2["camera_rotation_z"]
                    var layout: String=d2["layout"]
                    var move_time: float=d2["move_time"]
                    var moves: Array=d2["moves"]
                    var duration: float=dict["duration"]
                    interaction.display_chessgame(layout,turns2,move_time,moves,duration,rotation)
                    await interaction.finished
                elif "col" in dict:
                    var c: String=dict["col"]
                    var dur: float=dict["duration"]
                    interaction.display_colour(Color.from_string(c,Color.BLACK),dur)
                    await interaction.finished
            stop_music.emit()
            interaction.hide()
        

#endregion

#region Util

func get_names(file: String) -> Dictionary:
    var out:={}
    var res:=Util.read_or_crash(file)
    var dict: Dictionary=JSON.parse_string(res)
    for key: String in dict:
        var nums_y:=Util.keep_nums(key).to_int()
        var letters_x:=Util.get_num_from_letter(Util.keep_letters(key))
        var v:=Vector2i(letters_x-1,nums_y-1)
        out[v]=dict[key]
    return out

func get_special_names() -> Dictionary:
    var country: Country=self.map.countries.countries[self.tmp_colour]
    var tag:=country.tag.inner
    var king:=Pieces.get_char_from_name("king").upper()
    var queen:=Pieces.get_char_from_name("queen").upper()
    var out:={}
    for key: String in special_names:
        var val: PackedStringArray=special_names[key]
        if " in " in val[0]:
            var sp:=val[0].split(" in ")
            if sp[1]=="SURVIVORS":
                var prev_died:=true
                for i: Glib.PieceWithName in current_survivors:
                    if i.pieceType.get_char()==sp[0] && !i.title.is_empty():
                        val[0]=i.name
                        prev_died=false
                        break
                if prev_died:
                    for i: Glib.PieceWithName in current_survivors:
                        if i.pieceType.get_char()==sp[0]:
                            val[0]=i.name
                            break
        if key[0]==king.display() && int(key.split(" ")[1])==king_count:
            out[key]=val
        elif key[0]==queen.display() && int(key.split(" ")[1])==queen_count:
            out[key]=val
        elif key[0]!=king.display() && key[0]!=queen.display() && tag in key:
            out[key]=val
    return out

func is_condition_true(opp: Campaign.Opponents) -> bool:
    if "!" in opp.condition:
        return !Color.from_string(opp.condition.replace("!","").replace(" ",""),Color.BLACK) in self.map.countries.countries
    if "in" in opp.condition:
        if "king_count" in opp.condition.to_lower():
            for i in opp.condition.replace("[",",").replace("]",",").split(","):
                if i.trim_prefix(" ").trim_suffix(" ")=="%d" % king_count: return true
            return false
        elif "queen_count" in opp.condition.to_lower():
            for i in opp.condition.replace("[",",").replace("]",",").split(","):
                if i.trim_prefix(" ").trim_suffix(" ")=="%d" % queen_count: return true
            return false
    return true

#endregion

#region Save

func save() -> String:
    var dict:={
        "story_name": story_name,
        "casualties": Util.save_piece_array(casualties),
        "survivors": Util.save_piece_array(survivors),
        "current_survivors": Util.save_piece_array(current_survivors),
        "king_count": king_count,
        "queen_count": queen_count,
        "map": map.save(),
        "defeated_countries": defeated_countries,
        "random": SRand.save(),
        "wins": wins,
        "losses": losses,
        "games": games.map(func(g: GameData) -> Dictionary: return g.save()),
        "death_index": death_index
    }
    return JSON.stringify(dict)

func load_data(data: String) -> bool:
    var dict: Dictionary=JSON.parse_string(data)

    # Extra checks
    var key:=Util.missing_key(dict,["story_name","casualties","survivors","current_survivors","king_count","queen_count","map","defeated_countries","random","wins","losses","death_index","games"])
    if !key.is_empty():
        printerr("Loading Game failed! Loading Game needs '%s', but it's missing!" % key[0])
        return false
    
    # Remove previous
    if chessGame in get_children():
        remove_child(chessGame)
        add_child(map)
    
    story_name=dict["story_name"]
    var c: Array=dict["casualties"]; casualties=Util.load_piece_array(c)
    var s: Array=dict["survivors"]; survivors=Util.load_piece_array(s)
    var c2: Array=dict["current_survivors"]; current_survivors=Util.load_piece_array(c2)
    var k: float=dict["king_count"]; king_count=int(k)
    var q: float=dict["queen_count"]; queen_count=int(q)
    var m: Dictionary=dict["map"]; if !map.load_data(m): return false
    defeated_countries=dict["defeated_countries"]
    var s2: float=dict["random"]; SRand.load_data(s2)
    var w: float=dict["wins"]; wins=int(w)
    var l: float=dict["losses"]; losses=int(l)
    var d: float=dict["death_index"]; death_index=int(d)
    var g2: Array=dict["games"];
    games.clear()
    for st: Dictionary in g2:
        var opt_dat:=GameData.load_data(st)
        if opt_dat.is_empty(): return false
        games.push_back(opt_dat[0])
    return true

#endregion

#region Cheats

func _input(event: InputEvent) -> void:
    if OS.is_debug_build() && event.is_action_pressed("ui_home"):
        start_game(Tag.Tag("WAT"),16,Color.from_string("#A3F8F7",Color.BLACK))

#endregion
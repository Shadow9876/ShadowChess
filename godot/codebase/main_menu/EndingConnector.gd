class_name EndingConnector
extends RefCounted

static func connect_ending(game: GameSupervisor) -> void:
    match game.on_state_end:
        Glib.StateEnd.GRAVEYARD:
            Util.err_handle(game.map.state_end.connect(
                func() -> void:
                    game.remove_child(game.map)
                    game.map.queue_free()
                    game.map=null
                    var dead_kings: Array[Glib.PieceWithName]=[]
                    var dead_queens: Array[Glib.PieceWithName]=[]
                    var other_casualties: Array[Glib.PieceWithName]=[]
                    for casualty: Glib.PieceWithName in game.casualties:
                        if !casualty.title.is_empty():
                            match casualty.pieceType.get_char():
                                "K":
                                    dead_kings.push_back(casualty)
                                "Q":
                                    dead_queens.push_back(casualty)
                                _:
                                    other_casualties.push_back(casualty)
                    var graveyard:=Graveyard.new()
                    graveyard.casualties_kings=dead_kings
                    graveyard.casualties_queens=dead_queens
                    graveyard.casualties=other_casualties
                    graveyard.defeated_countries=game.defeated_countries
                    Util.err_handle(graveyard.back_to_main_menu.connect(func () -> void: game.delete_save.emit(); game.CampaignLost.emit()))
                    Util.err_handle(graveyard.play_music.connect(func (music: String) -> void: game.play_music.emit(music)))
                    Util.err_handle(graveyard.stop_music.connect(func () -> void: game.stop_music.emit()))
                    game.add_child(graveyard)
            ))
        Glib.StateEnd.STATISTICS:
            Util.err_handle(game.map.state_end.connect(
                func() -> void:
                    game.remove_child(game.map)
                    game.map.queue_free()
                    game.map=null
                    var stat: Statistics=preload("res://codebase/main_menu/statistics/statistics.tscn").instantiate()
                    stat.games=game.games
                    stat.wins=game.wins
                    stat.losses=game.losses
                    Util.err_handle(stat.back_to_main_menu.connect(func () -> void: game.delete_save.emit(); game.CampaignLost.emit()))
                    Util.err_handle(stat.play_music.connect(func (music: String) -> void: game.play_music.emit(music)))
                    game.add_child(stat)
            ))
        Glib.StateEnd.CUSTOM:
            pass

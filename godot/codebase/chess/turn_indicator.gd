class_name TurnIndicator
extends Node3D

#region Export Variables

@export var turns: Array[bool]=[false,true,true,false] :
    set(val):
        if val!=turns:
            turns=val
            regenerate()

var turn:=0

@export var transition_time:=0.8
@export var theme:="3D"

#endregion

#region Flags

var destiny_helper:=false
var black_avatar:="pawn"
var white_avatar:="pawn"

#endregion

#region Components

var chessPieces:=ChessPieces.new()

#endregion

#region Ready

func find_avatars() -> void:
    for col: bool in [false,true]:
        for n: String in ["pawn","knight","bishop","rook","queen","king","monster"]:
            var chr:=Pieces.get_char_from_name(n)
            if !col: chr=chr.upper()
            var piece:=Piece.construct(Vector2i(0,0),chr,theme,false)
            if !piece.is_empty():
                if col: white_avatar=n
                else: black_avatar=n
                piece[0].queue_free()
                break

func _ready() -> void:
    add_child(chessPieces)
    find_avatars()
    regenerate()

func clear() -> void:
    for child in get_children():
        remove_child(child)
        if is_instance_valid(child):
            child.queue_free()

func regenerate() -> void:
    var layout: String=""
    for t in turns:
        layout+=(Pieces.get_char_from_name(white_avatar) if t else Pieces.get_char_from_name(black_avatar).upper()).display()
        for i in turns.size()-1:
            layout+=Pieces.get_char_from_name("empty").display()
    chessPieces.assign(turns.size(),layout,theme)

#endregion

#region Change

func get_current_turns() -> Array[bool]:
    var curr:=turns.duplicate(true)
    for i in turn:
        curr.push_back(curr.pop_front())
    return curr

func get_val_from_avatar(col: bool) -> int:
    var avatar:=white_avatar if col else black_avatar
    var val:=Pieces.get_val_from_name(avatar)
    if !col: val*=-1
    return val

func change(forward: bool) -> void:
    var instructions: Array[Vec6]=[]
    var curr:=get_current_turns()
    for i: int in (range(1,turns.size()) if forward else range(turns.size()-1)):
        var instr:=PackedInt32Array([i,0,i-(1 if forward else -1),0,get_val_from_avatar(curr[i]),0])
        instructions.push_back(Vec6.from_packed_arr(instr))
    var last_instrs: Array[Vec6]=[]
    if forward:
        last_instrs.push_back(Vec6.from_packed_arr(PackedInt32Array([0,0,-1,-1,get_val_from_avatar(curr[-1]),0])))
        last_instrs.push_back(Vec6.from_packed_arr(PackedInt32Array([-1,-1,turns.size()-1,0,get_val_from_avatar(curr[0]),0])))
    else:
        last_instrs.push_back(Vec6.from_packed_arr(PackedInt32Array([turns.size()-1,0,-1,-1,get_val_from_avatar(curr[0]),0])))
        last_instrs.push_back(Vec6.from_packed_arr(PackedInt32Array([-1,-1,0,0,get_val_from_avatar(curr[-1]),0])))
    instructions.append_array(last_instrs)
    # Increment or decrement turn
    turn+=1 if forward else turns.size()-1
    turn%=turns.size()
    await chessPieces.transition(instructions,transition_time)

#endregion

#region Transparency

func set_transparency(val: float, time: float) -> void:
    if time==0:
        for piece: Piece in chessPieces.get_children():
            piece.transparency=val
    else:
        var tw:=create_tween()
        for piece: Piece in chessPieces.get_children(): Util.discard(tw.parallel().tween_property(piece,'transparency',val,time))
        await tw.finished

#endregion
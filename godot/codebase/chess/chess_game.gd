class_name ChessGame
extends Node3D

#region Signals

signal block_input
signal unblock_input
signal block_history
signal unblock_history
signal end_of_game(winner: bool)

#endregion

#region Export Variables

## Size[br]
## [br]
## Determines the size of the board.
## Changing [code]size[/code] automatically calls [code]regenerate()[/code].
@export var size:=8 : 
	set(val):
		if size!=val:
			size=val
			regenerate()

## Turns[br]
## [br]
## In which order does black and white go?
@export var turns: Array[bool]=[false,true,true,false] : 
	set(val):
		if turns!=val:
			turns=val
			regenerate()

## If true white will be controlled by the engine
@export var isWhiteAi:=true

## If true black will be controlled by the engine
@export var isBlackAi:=false

## Depth[br]
## [br]
## Determines the depth of the engine's search on white pieces (if white is ai)
@export var white_depth:=4

## Depth[br]
## [br]
## Determines the depth of the engine's search on black pieces (if black is ai)
@export var black_depth:=4

## Light colour[br]
## [br]
## Determines the colour of the light tiles.
## Changing [code]light[/code] automatically calls [code]regenerate()[/code].
@export var light:=Color(1,1,1,1) : 
	set(val):
		if light!=val:
			light=val
			regenerate()

## Dark colour[br]
## [br]
## Determines the colour of the dark tiles.
## Changing [code]dark[/code] automatically calls [code]regenerate()[/code].
@export var dark:=Color(0,0,0,1) : 
	set(val):
		if dark!=val:
			dark=val
			regenerate()

## Layout of the pieces on the chessboard[br]
## [br]
## [code]layout[/code] should implement the following guidelines:[br]
## - [code]layout[/code] should have at least [code]size * size[/code] valid characters[br]
## - Valid characters include: [code]'p', 'n', 'b', 'r', 'q', 'k', 'm'[/code] and their uppercase
## variants (lower case is white, upper case is black)[br]
## - 'x' is also a valid character which is parsed to an empty square[br]
## - Any other characters won't get parsed, simply side stepped[br]
## - If the [code]layout[/code] has more than [code]size * size[/code] characters, the parser will check for 
## leaders[br]
## - Leaders can be given to the parser after the [code]size * size[/code] valid characters following
## format: [code]"kqMR"[/code] (this will make the king and queen the leaders of white and the
## monster and rook the leaders of black)[br]
## [br]
## Changing [code]layout[/code] automatically calls [code]regenerate()[/code].
@export var layout:="" : 
	set(val):
		if layout!=val:
			layout=val
			regenerate()

## Theme of the pieces[br]
## [br]
## [code]theme[/code] sets the path for individual pieces and thus is responsible for the appearance of pieces.
## Valid themes include [code]"2D"[/code], [code]"3D"[/code] and [code]"fish"[/code]. Themes containing the number
## [code]'3'[/code] will be considered a 3D theme, therefore they'll require 3D models to appear properly. Otherwise
## it will be considered a 2D theme and will need images instead. If [code]fish[/code] is present in the name of a 2D
## theme, each piece will occupy a [code]3x3[/code] square instead of a [code]1x1[/code].[br]
## [br]
## Changing [code]theme[/code] automatically calls [code]regenerate()[/code].
@export var theme:="3D" : 
	set(val):
		if theme!=val:
			theme=val
			regenerate()

## Starting time of the game[br]
## [br]
## It's [code]6[/code] by default, meaning 6 AM ([code]0<=time<24[/code] and [code]time[/code] moves in [code]0.25[/code]
## increments).
@export var starting_time:=6 : 
	set(val):
		if val>=0 && val<24:
			starting_time=val

var music_playlist:=""
var music_start_with:=""
var instant_death:=false

## Whether the game is scripted and was subsequently started from an [code]Interaction[/code]
var scripted:=false

#endregion

#region Theme

func apply_theme(thm: String) -> void:
	var dict: Dictionary=JSON.parse_string(thm)
	for key: String in dict:
		var attr: Variant=self.get(key)
		match typeof(attr):
			TYPE_BOOL, TYPE_FLOAT, TYPE_STRING:
				self.set(key,dict[key])
			TYPE_INT:
				var val: float=dict[key]
				self.set(key,int(val))
			TYPE_COLOR:
				var val: String=dict[key]
				self.set(key,Color.from_string(val,Color.BLACK))
			_:
				assert(false,"The type '%s' is not currently handled by the function 'apply_theme'." % typeof(attr))


#endregion

#region Components

var chessBoard:=ChessBoard.new()
var chessPieces:=ChessPieces.new()
var camera:=Camera3D.new()
var chessEngine:=ChessEngine.new()
var moveHints:=MoveHints.new()
var environment: ChessEnvironment=preload("res://codebase/chess/environment.tscn").instantiate()
## History[br]
## [br]
## History contains all moves and subsequently all lines.
## [codeblock]History: Array[Array[Move]][/codeblock]
## [codeblock]Move: Vec8 = Vec6 + promotion + turn[/codeblock]
## (where [codeblock]promotion in [0,1] and turn: int[/codeblock])
var history : Array[Array] = [[]]
## A variable to keep track of turn regardless of linearity
var turn := 0
## Line[br]
## [br]
## Line keeps track of the current variation we are exploring, the main line
## is always [0]
var line : Array[int] = [0]
## Input for moving pieces
var chessInput:=ChessInput.new()
## Pieces which indicate whose turn it is
var turnIndicator:=TurnIndicator.new()

#endregion

#region Flags

## Whether or not to play the introduction to a scene
var play_intro:=true
## Whether or not player is in planning phase
var planning: bool=false
## Whether player can change past moves and transition into a new line (outside planning phase),
## means nothing if [code]can_access_history[/code] is [code]false[/code].
var can_change_past: bool=true
## Whether player can view past moves
var can_access_history: bool=true
## Whether player can enter planning phase
var can_plan: bool=true
## Time of piece movements in seconds (0 means no animation will be played at all)
var transition_time: float=0.8
## Whether or not to display move hints (all possible moves and captures when selecting a piece)
var move_hints: bool=true
## Whether time should pass
var pass_time: bool=true
## Base saturation value
var base_saturation: float=1
## Whether or not to show turn indicator
var show_turn_indicator: bool=true
## Whether to show threats via turn indicator
var show_destiny_helper: bool=false

#endregion

#region Init

func _init() -> void:
	var f:=func(pos: Vector2) -> void:
		camera.position.x=pos.x
		camera.position.z=pos.y
		turnIndicator.position.x=-4 if Root.turn_indicator_on_lefthand_side else (chessBoard.size-1)*2+4
		turnIndicator.position.z=chessBoard.size-turns.size()
	camera.current=true
	camera.fov=30
	camera.position.y=50
	camera.rotate_x(deg_to_rad(-90))

	environment.outside="shrine"
	
	Util.err_handle(chessBoard.camera_pos.connect(f))
	# Initialise input
	chessInput.assign(self)
	Util.err_handle(chessInput.exec_input.connect(execute_input))
	Util.err_handle(chessInput.revert_input.connect(revert_input))
	Util.err_handle(chessInput.hide_promotion.connect(hide_prom))
	Util.err_handle(chessInput.show_promotion.connect(show_prom))
	Util.err_handle(chessInput.toggle_planning.connect(toggle_planning))
	Util.err_handle(chessInput.remove_phantom.connect(chessPieces.remove_phantom_pieces))
	Util.err_handle(chessInput.spawn_phantom.connect(chessPieces.spawn_phantom_piece_at))
	Util.err_handle(chessInput.spawn_phantom.connect(show_hints))
	Util.err_handle(chessInput.remove_phantom.connect(hide_hints))
	Util.err_handle(environment.notable_time.connect(notable_time_handler))
	Util.err_handle(block_input.connect(game_block_input))
	Util.err_handle(unblock_input.connect(game_unblock_input))
	Util.err_handle(block_history.connect(game_block_history))
	Util.err_handle(unblock_history.connect(game_unblock_history))

func game_block_input() -> void:
	chessInput.block_input()

func game_block_history() -> void:
	chessInput.block_history()

func game_unblock_input() -> void:
	if !scripted:
		chessInput.unblock_input()

func game_unblock_history() -> void:
	if !scripted:
		chessInput.unblock_history()

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	regenerate()
	# Add components to scene
	add_child(chessBoard)
	add_child(chessPieces)
	add_child(camera)
	add_child(chessInput)
	add_child(chessEngine)
	add_child(moveHints)
	add_child(environment)
	add_child(turnIndicator)

	if !show_turn_indicator || "turn_animation" not in Root.played_animations:
		turnIndicator.hide()
	
	turnIndicator.transition_time=transition_time
	environment.tr_speed=0
	environment.time=starting_time
	environment.tr_speed=transition_time
	environment.set_saturation(base_saturation)
	for i: String in Env.intros[environment.outside]:
		if i=="dsmd":
			var val: float=Env.intros[environment.outside][i][0]
			environment.set_dsmd(val)
		else:
			camera.set_indexed(i,Env.intros[environment.outside][i][0])
	if play_intro:
		var tw:=create_tween()
		var dsmd_val:=-1
		for i: String in Env.intros[environment.outside]:
			if i=="dsmd":
				dsmd_val=Env.intros[environment.outside][i][1]
				continue
			var arr: Array=Env.intros[environment.outside][i]
			if arr.size()<3: continue
			var dur: float=arr[2]
			Util.discard(tw.parallel().tween_property(camera,i,arr[1],dur))
		await tw.finished
		if dsmd_val>=0: environment.set_dsmd(dsmd_val)
	else:
		for i: String in Env.intros[environment.outside]:
			if i=="dsmd":
				var val: float=Env.intros[environment.outside][i][1]
				environment.set_dsmd(val)
			else:
				camera.set_indexed(i,Env.intros[environment.outside][i][1])
	try_engine_move()

## Redraw the board and pieces based on [code]light[/code], [code]dark[/code],
## [code]size[/code], [code]layout[/code] and [code]theme[/code].
func regenerate() -> void:
	turnIndicator.turns=self.turns
	turnIndicator.theme=theme
	
	chessBoard.assign(size,light,dark)
	chessPieces.assign(size,layout,theme)
	if !layout.is_empty():
		chessEngine.complete_parse(size,layout,turns)

#endregion

#region Move Hints

## Generate new move hints inside [code]moveHints[/code]
func show_hints(piece: Vector2i, _chr: Char) -> void:
	if !move_hints: return
	var mvs: Array[Vec6]=chessEngine.get_legal_moves().filter(func(m: Vec6) -> bool: return m.get_at(0)==piece.y && m.get_at(1)==piece.x)
	moveHints.generate(mvs)

## Call [code]moveHints.clear()[/code]
func hide_hints() -> void:
	moveHints.clear()

#endregion

#region Constructor

## Construct a new [code]ChessGame[/code]
static func ChessGame(SIZE:=8, WHITE_DEPTH:=4, BLACK_DEPTH:=4, LAYOUT:="", ISWHITEAI:=true, ISBLACKAI:=false, LIGHT:=Color(1,1,1,1), DARK:=Color(0,0,0,1), THEME:="3D", SCRIPTED:=false) -> ChessGame:
	var ret:=ChessGame.new()
	ret.size=SIZE
	ret.isWhiteAi=ISWHITEAI
	ret.isBlackAi=ISBLACKAI
	ret.white_depth=WHITE_DEPTH
	ret.black_depth=BLACK_DEPTH
	ret.layout=LAYOUT
	ret.light=LIGHT
	ret.dark=DARK
	ret.theme=THEME
	ret.scripted=SCRIPTED
	if ret.scripted:
		ret.block_history.emit()
		ret.block_input.emit()
	return ret

#endregion

#region Input

var promotion: Array[Promotion]=[]

func get_appropriate_forward_move(t: int = 0) -> Array[Vec8]:
	if !can_access_history: return []
	var max_turn:=2_147_483_647
	for l in range(line.size()-1,-1,-1):
		for i in range(self.history[line[l]].size()-1,-1,-1):
			var m: Vec8=history[line[l]][i]
			if m.get_at(7)<max_turn && m.get_at(7)==turn + t:
				if line.size()!=1 && i==0:
					line.pop_back()
				return [m.clone()]
		if l<line.size() && line[l]<history.size() && !history[line[l]].is_empty():
			var first: Vec8=history[line[l]][0]
			max_turn=first.get_at(7)
	return []

func get_engine_move() -> Vec6:
	return chessEngine.get_best_move(white_depth if chessEngine.get_turn() else black_depth)

func revert_input(inp: Vec6, prom: bool) -> void:
	if planning && turn==cache_turn || !planning && !can_access_history: return
	chessInput.reset_input()
	if !promotion.is_empty():
		for i in promotion.size():
			hide_prom()
	if turn==0: return

	environment.tr_speed=transition_time
	if pass_time: environment.dec_time()
	var instr:=chessEngine.b_move_instructions(inp, prom)
	chessEngine.revert_move(inp,prom)
	turn-=1

	if should_ai_move(): block_input.emit()
	else: unblock_input.emit()
	
	if show_turn_indicator: turnIndicator.change(false)
	await chessPieces.transition(instr,transition_time)

## Returns with Option[line_num]
func any_line_contains_input(inp: Vec8) -> Array[int]:
	for i in range(history.size()-1,-1,-1):
		var ret:=line_contains_input(i,inp)
		if !ret.is_empty():
			return [i]
	return []

## Returns with Option[inp_index_in_line]
func line_contains_input(i: int, inp: Vec8) -> Array[int]:
	var last: Vec8=history[i][-1]
	var first: Vec8=history[i][0]
	if not (last.get_at(7)>=turn and first.get_at(7)<=turn):
		return []
	for j in history[i].size():
		var curr: Vec8=history[i][j]
		if curr.eq(inp):
			return [j]
	return []

func execute_input(inp: Vec6, from_history: bool = false) -> void:
	if "turn_animation" not in Root.played_animations && turn==turns.size()-1:
		Util.discard(Root.played_animations.push_back("turn_animation"))
		turnIndicator.show()
		turnIndicator.set_transparency(1.0,0.0)
		turnIndicator.set_transparency(0.0,transition_time*2)
	if !from_history && !can_change_past && !planning && !history[line[0]].is_empty():
		var last: Vec8=history[line[0]][-1]
		if last.get_at(7)!=turn: return
	environment.tr_speed=transition_time
	if pass_time: environment.inc_time()
	# Only necessary for end of game
	var winner:=chessEngine.get_turn()
	var instr: Array[Vec6]=chessEngine.f_move_instructions(inp)
	var p: bool=chessEngine.make_move(inp)
	# History
	turn+=1
	var mov:=Vec8.from6(inp)
	Util.discard(mov.set_at(6,1 if p else 0))
	Util.discard(mov.set_at(7,turn))
	var last2: Vec8=Vec8.constr() if history[line[-1]].is_empty() else history[line[-1]][-1]
	if not history[line[-1]].is_empty() and last2.get_at(7)>=turn:
		var l:=any_line_contains_input(mov)
		if not l.is_empty():
			if not l[0] in line:
				line.push_back(l[0])
		else:
			history.push_back([mov])
			line.push_back(history.size()-1)
	else:
		history[line[-1]].push_back(mov)

	if should_ai_move(): block_input.emit()
	elif !scripted: unblock_input.emit()
	
	# Transition
	if show_turn_indicator: turnIndicator.change(true)
	await chessPieces.transition(instr,transition_time)
	
	var has_game_ended:=chessEngine.is_game_over()
	if !from_history && !planning:
		if !has_game_ended:
			try_engine_move()
		else:
			block_input.emit()
			block_history.emit()
			end_of_game.emit(winner)

func try_engine_move() -> void:
	# Engine move
	if should_ai_move():
		block_input.emit()
		block_history.emit()
		var cached:=get_appropriate_forward_move(1)
		if !cached.is_empty():
			execute_input(Vec6.from8(cached[0]))
		else:
			execute_input(get_engine_move())
	elif !scripted:
		unblock_input.emit()
		unblock_history.emit()

func show_prom(colour: bool, pos: Vector2i) -> void:
	var prom:=Promotion.new()
	prom.colour=colour
	prom.dark=dark.lightened(0.1)
	prom.light=light.darkened(0.1)
	prom.theme=theme
	prom.time=transition_time
	prom.set_pos(pos)
	promotion.push_back(prom)
	add_child(prom)
	prom.appear()

func hide_prom() -> void:
	if !promotion.is_empty():
		var prom: Promotion=promotion.pop_front()
		prom.disappear()
		remove_child(prom)
		prom.free()

var cache_history : Array[Array] = [[]]
var cache_turn := -1

func toggle_planning() -> void:
	if !can_plan: return
	planning=!planning
	if planning:
		self.pass_time=false
		self.environment.set_saturation(0.01)
		cache_history=history.duplicate(true)
		cache_turn=turn
	else:
		while turn!=cache_turn:
			var inp:=get_appropriate_forward_move()[0]
			revert_input(Vec6.from8(inp),inp.get_at(6)==1)
		history=cache_history.duplicate(true)
		self.pass_time=true
		self.environment.set_saturation(base_saturation)
	# For Statistics
	used_planning+=1

func should_ai_move() -> bool:
	var ch_turn:=chessEngine.get_turn()
	return !planning && (ch_turn && isWhiteAi || !ch_turn && isBlackAi)

# Cheats
func _input(event: InputEvent) -> void:
	if OS.is_debug_build() && event.is_action_pressed("ui_end"):
		end_of_game.emit(false)

#endregion

#region Reset

func reset() -> void:
	pass_time=false
	environment.time=starting_time
	self.environment.set_saturation(0.01)
	await get_tree().create_timer(0.8).timeout
	while turn!=0:
		var inp:=get_appropriate_forward_move()[0]
		revert_input(Vec6.from8(inp),inp.get_at(6)==1)
		await get_tree().create_timer(0.1).timeout
	history=[[]]
	line=[0]
	self.pass_time=true
	self.environment.set_saturation(base_saturation)

#endregion

#region Casualties

func get_casualties() -> Array[Glib.PieceWithName]:
	var out: Array[Glib.PieceWithName]=[]
	for v: Vector2i in chessPieces.removed_piece_stack:
		var pieces: Array=chessPieces.removed_piece_stack[v]
		for piece: Glib.PieceWithName in pieces:
			if !piece.name.is_empty():
				out.push_back(piece)
	return out

func get_survivors() -> Array[Glib.PieceWithName]:
	var out: Array[Glib.PieceWithName]=[]
	for piece: Piece in chessPieces.get_children():
		if !piece.nameStr.is_empty():
			out.push_back(Glib.PieceWithName.PieceWithName(piece.type,piece.nameStr,piece.titleStr))
	return out

#endregion

#region Statistics

var used_planning:=0

var midnights:=0

func notable_time_handler(timeInt: int) -> void:
	if timeInt==0: midnights+=1

func get_game_data() -> GameData:
	return GameData.GameData(history,line,used_planning,midnights)

#endregion

## The environment of the chess game
## 
## This script is connected to a scene, as such it should be used in the following manner:
## [codeblock]
## var environment: ChessEnvironment=preload("res://codebase/chess/environment.tscn").instantiate()
## environment.inc_time() # Move forward in time (with transition)
## environment.dec_time() # Move backwards in time (with transition)
## environment.seek_time(12) # Instantly set time to 12 o'clock
## [/codeblock]
class_name ChessEnvironment
extends Node3D

#region Signals

## [codeblock]notable_time: Signal[int][/codeblock][br]
## [br]
## This signal is emitted once a whole hour which is divisible by 6 occurs
## (for example 0, 6, 12 and 18)
signal notable_time(time: int)

#endregion

#region Variables

## Time in the chess environment[br]
## [br]
## 0 <= time <= 23.75, it moves in 0.25 intervals. Setting this value will trigger
## the [code]set_time[/code] function resulting in a smooth transition between
## two timeframes (in 1/[code]speed[/code] seconds). If time is already equal to the
## value the user wants to set it to, nothing will happen.
@export var time: float=6 : 
	set(val):
		if time!=val && time>=0 && time<24:
			time=val
			if int(time)==time && int(time)%6==0:
				notable_time.emit(int(time))
			set_time()

## Speed of the time's progression[br]
## [br]
## It will take tr_speed seconds to transition from one timestamp
## to another.
@export var tr_speed: float=1

## The outside environment of the chess game[br]
## [br]
## All valid outsides are defined in [code]Env.envs[/code].
@export var outside: String="" :
	set(val):
		if get_child_count()>4:
			var ch:=self.get_child(-1)
			self.remove_child(ch)
			ch.free()
		if outside!=val and val in Env.envs:
			outside=val
			var f: Callable=Env.envs[val]
			var res: Env=f.call()
			self.add_child(res)


## Whether time is moving forward
var forward := true

#endregion

#region Ready

## Main animated chess environment[br]
## [br]
## This is a script specifically attached to environment.tscn,
## as such $AnimationPlayer comes from that scene.
@onready var animationPlayer: AnimationPlayer=$AnimationPlayer

func _ready() -> void:
	if outside in Env.envs:
		var f: Callable=Env.envs[outside]
		var res: Env=f.call()
		self.add_child(res)
	animationPlayer.play("day night cycle",-1,0.25/tr_speed)
	seek_time(time)

#endregion

#region Functions

## Instantly transition to specified timeframe[br]
## [br]
## Example:
## [codeblock]
## var environment: ChessEnvironment=preload("res://codebase/chess/environment.tscn").instantiate()
## environment.seek_time(18) # Jump to 18 o'clock in the environment
## [/codeblock]
func seek_time(timeframe: float) -> void:
	animationPlayer.pause()
	animationPlayer.seek(timeframe,true)

## Transition to [code]time[/code] in 1/[code]speed[/code] seconds[br]
## [br]
## Example:
## [codeblock]
## var environment: ChessEnvironment=preload("res://codebase/chess/environment.tscn").instantiate()
## environment.time=12 # The setter of time automatically calls this method
## environment.set_time() # So calling it here will achieve nothing
## [/codeblock]
func set_time() -> void:
	if tr_speed==0: return seek_time(time)
	animationPlayer.play("day night cycle",-1,0.25/(tr_speed if forward else -tr_speed))
	if !is_inside_tree(): return
	await get_tree().create_timer(tr_speed).timeout
	animationPlayer.pause()
	if time!=animationPlayer.current_animation_position:
		seek_time(time)

## Go forward in time[br]
## [br]
## Increase time by 0.25 (if time is 23.75 set it to 0.00), function [code]set_time[/code]
## will automatically be called and trigger a smooth transition between the previous and
## current time (in 1/[code]speed[/code] seconds).
## Example:
## [codeblock]
## var environment: ChessEnvironment=preload("res://codebase/chess/environment.tscn").instantiate()
## environment.inc_time() # Time will move forward from the initial 6.00 to 6.25
## [/codeblock]
func inc_time() -> void:
	forward=true
	if time+0.25>=24:
		time=0
	else:
		time+=0.25

## Go backwards in time[br]
## [br]
## Decrease time by 0.25 (if time is 0.00 set it to 23.75), function [code]set_time[/code]
## will automatically be called and trigger a smooth transition between the current and
## previous time (in 1/[code]speed[/code] seconds). In this case time will move backwards.
## Example:
## [codeblock]
## var environment: ChessEnvironment=preload("res://codebase/chess/environment.tscn").instantiate()
## environment.dec_time() # Time will move backwards from the initial 6.00 to 5.75
## [/codeblock]
func dec_time() -> void:
	forward=false
	if time-0.25<0:
		time=23.75
	else:
		time-=0.25

## Set Directional Shadow Max Distance for Sun[br]
## [br]
## Close scenes are set to approximately [code]60[/code] to [code]100[/code].
func set_dsmd(val: float) -> void:
	($Sun as DirectionalLight3D).directional_shadow_max_distance=val

## Set saturation for Environment[br]
## [br]
## [code]1[/code] is the default value, [code]0.01[/code] means grayscale.
func set_saturation(val: float) -> void:
	($WorldEnvironment as WorldEnvironment).environment.adjustment_saturation=val

#endregion

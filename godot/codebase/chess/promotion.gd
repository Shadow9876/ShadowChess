## Class implementing the appearance of promotion in the chess game
class_name Promotion
extends Node3D

#region Export Variables

## Light colour[br]
## [br]
## Determines the colour of the light tiles. It is white by default.
@export var light:=Color(1,1,1,1) : 
	set(val):
		if light!=val:
			light=val
			regenerate()

## Dark colour[br]
## [br]
## Determines the colour of the dark tiles. It is black by default.
@export var dark:=Color(0,0,0,1) : 
	set(val):
		if dark!=val:
			dark=val
			regenerate()

## Theme of the pieces in Promotion[br]
## [br]
## Currently valid themes are [code]"2D"[/code], [code]"3D"[/code]
## and [code]"fish"[/code].
@export var theme:="3D" : 
	set(val):
		if theme!=val:
			theme=val
			regenerate()

## Which side should promote?[br]
## [br]
## [code]true[/code] is for white, [code]false[/code] is for
## black. Updating this will redraw the promotion.
@export var colour:=true :
	set(val):
		if colour!=val:
			colour=val
			regenerate()

## The time it takes for the promotion to appear (in seconds)
@export var time: float=1

#endregion

#region Ready

func _ready() -> void:
	regenerate()

## Regenerate [br]
## [br]
## Delete old and create a new promotion
func regenerate() -> void:
	for child in get_children():
		remove_child(child)
		child.free()
	var pieces: Array[Char]=[
		Pieces.get_char_from_name("knight"),
		Pieces.get_char_from_name("bishop"),
		Pieces.get_char_from_name("rook"),
		Pieces.get_char_from_name("queen"),
		Pieces.get_char_from_name("king")
	]
	for i in pieces.size():
		var base:=MeshInstance3D.new()
		base.mesh=PlaneMesh.new()
		base.position=Vector3(i*2,0,0)
		base.material_override=StandardMaterial3D.new()
		(base.material_override as StandardMaterial3D).albedo_color=dark if i%2==0 else light
		add_child(base)
		base.transparency=1
		var piece:=Piece.construct(Vector2i(i,0),pieces[i] if colour else pieces[i].upper(),theme)
		add_child(piece[0])
		piece[0].transparency=1

#endregion

#region Functions

## Set Position [br]
## [br]
## Set position of a promotion
func set_pos(pos: Vector2i) -> void:
	var offset:=1 if colour else -1
	(self as Node3D).position=Vector3(pos.y*2-4,1,(pos.x+offset)*2)

## Get Character At [br]
## [br]
## Get the character (type) of the piece at pos (only x axis)
func get_ch_at(pos: int) -> Char:
	for child in get_children():
		if child is Piece:
			var ch:=child as Piece
			if ch.global_position.x==pos*2:
				return ch.type
	return Char.new()

## Set transparency of all pieces (and squares) with transition
func set_transparency(tp: float) -> void:
	if time==0:
		for child: GeometryInstance3D in get_children(): child.transparency=tp
	else:
		var tw:=create_tween()
		for child in get_children(): Util.discard(tw.parallel().tween_property(child,'transparency',tp,time))
		await tw.finished

## Make Promotion appear (tween from 1 transparency to 0)
func appear() -> void:
	set_transparency(0)

## Make Promotion disappear (tween from 0 transparency to 1 and remove children)
func disappear() -> void:
	set_transparency(1)
	for child in get_children():
		remove_child(child)
		child.free()

#endregion

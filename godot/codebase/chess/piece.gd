## Class implementing the construction of an individual chess piece
class_name Piece
extends MeshInstance3D

#region Export Variables

## Type of the piece[br]
## [br]
## [codeblock]type: Char[/codeblock]
## Type contains the form of the piece (king/queen/...) and its colour as well.
## It automatically assigns a mesh to the piece based on this, its [code]path[/code]
## and its [code]theme[/code].
var type: Char=Char.new() :
	set(val):
		if !type.eq(val) && assign_mesh(val):
			type=val

## Path of the chess piece[br]
## [br]
## This path should not be changed as setting the [code]theme[/code]
## also changes it.
var path:="res://imports/chess/3D"

## Theme of the chess pieces[br]
## [br]
## This modifies [code]path[/code] as well. The default [code]theme[/code]
## is [code]"3D"[/code] but [code]"2D"[/code] and [code]"fish"[/code] are
## also valid values (although not for every piece). If theme has a [code]'3'[/code]
## in it, it'll be considered as a 3D theme, otherwise it'll be a 2D theme.
## 2D themes require .png or .svg files as pieces and if [code]"fish"[/code] is present in their
## names, they'll take up 3x3 squares instead of 1x1. 3D themes require .blend files as pieces.
@export var theme:="3D" : 
	set(val):
		if theme!=val:
			theme=val
			path="res://imports/chess/%s" % theme
			var __:=assign_mesh(type)

## Logical position of the piece[br]
## [br]
## This is the position the piece SHOULD BE logically. The piece might not be exactly here
## (it might be moving to this square).
var initial_position:=Vector2i(0,0)

## Whether or not this piece is captured[br]
## [br]
## This flag is the result of the nonlinearity of the "history" system. If a piece is captured
## it will be deleted, though not always immediately. Sometimes a death animation (fading away)
## is played, thus this flag is needed in a lot of logic concerning moving back and forth in time.
var captured:=false

## All accepted extensions for 2D themes[br]
## [br]
## Only [code]"png"[/code] and [code]"svg"[/code] are accepted by default.
var ext2D: PackedStringArray = ["png","svg"]

## All accepted extensions for 3D themes[br]
## [br]
## Only [code]"blend"[/code] and [code]"obj"[/code] are accepted as default.
var ext3D: PackedStringArray = ["blend","obj"]

## Whether or not to show a warning upon trying and failing to assign a mesh to the piece
var show_warning:=true

#endregion

#region Name

## Name of the piece
var nameStr: String = ""

## Title of the piece
var titleStr: String = ""

## Grant a name (and title) to a [code]Piece[/code]
func grant_name(granted_name: String, granted_title: String="") -> void:
	self.nameStr=granted_name
	self.titleStr=granted_title

#endregion

#region Functions

## Constructs and returns a new piece at the specified position[br]
## [br]
## A specified theme can be selected via [code]piece_theme[/code]. If the construction
## is not possible it returns with an empty [code]Array[/code].
static func construct(pos: Vector2i, chr: Char, piece_theme:="3D", warn:=true) -> Array[Piece]:
	var out:=Piece.new()
	# Set Warning
	out.show_warning=warn
	# Set Theme
	out.theme=piece_theme
	# Try to set type to chr
	out.type=chr
	# If setting is successful do this (type has a custom setter; yes this needs a rewrite)
	if out.type.eq(chr):
		out.initial_position=pos
		out.snap_to_initial_position()
		return [out]
	out.free()
	return []

## Move piece to specified square
func move_to(pos: Vector2i) -> void:
	(self as Node3D).position=Vector3(pos.x*2,0.8,pos.y*2)

## Snap piece to its logical position
func snap_to_initial_position() -> void:
	(self as Node3D).position=Vector3(initial_position.x*2,0.8,initial_position.y*2)

#endregion

#region Assign Mesh

## Assign mesh to piece[br]
## [br]
## It assigns meshes based on its [code]theme[/code] and [code]type[/code].
## If the path of the assigned mesh doesn't exist, or it's present with an unrecognised format,
## it returns with a [code]false[/code], otherwise it assigns the present mesh and returns with
## [code]true[/code].
func assign_mesh(ident: Char) -> bool:
	# Set extension
	var extensions:=ext3D if theme.contains('3') else ext2D
	# Set colour for 2d loading
	var colour:="" if extensions==ext3D else ("white_" if ident.is_lower() else "black_")
	# Set type
	# piece_dict: Dictionary[String, String]
	var piece_dict := {
		Pieces.get_char_from_name("pawn").display():"pawn",
		Pieces.get_char_from_name("knight").display():"knight",
		Pieces.get_char_from_name("bishop").display():"bishop",
		Pieces.get_char_from_name("rook").display():"rook",
		Pieces.get_char_from_name("queen").display():"queen",
		Pieces.get_char_from_name("king").display():"king",
		Pieces.get_char_from_name("monster").display():"monster",
	}
	var piece: String = '' if ident.lower().get_char() not in piece_dict else piece_dict[ident.lower().get_char()]
	if piece.is_empty(): return false
	var pth: String
	for extension: String in extensions:
		pth="%s/%s%s.%s" % [path, colour, piece, extension]
		if assign_mesh_to_imported(pth, extensions==ext3D, ident.is_lower(),"fish" in theme): return true
	if show_warning: push_warning("File %s/%s%s.* was not found with the following extensions: %s!" % [path,colour,piece,", ".join(extensions)])
	return false
	
## Assign mesh based on its file name ([code]imported[/code]), and other parameters
## ([code]is_3d[/code] and [code]is_white[/code])
func assign_mesh_to_imported(imported: String, is_3d: bool, is_white: bool, is_3x3: bool, override_material: bool=true) -> bool:
	var loaded:=Util.try_load(imported)
	# We couldn't load anything
	if loaded.is_empty(): return false
	var loaded_res:=loaded[0]
	var meshes: Array[MeshInstance3D]=[]
	if loaded_res is PackedScene:
		var ob:=(loaded_res as PackedScene).instantiate()
		meshes=Util.get_mesh(ob)
	elif loaded_res is Mesh:
		var m_inst:=MeshInstance3D.new()
		m_inst.mesh=loaded_res
		meshes=[m_inst]
	elif is_3d:
		printerr("Could not load mesh for 3D piece with path '%s'. Resource on path is neither Mesh nor PackedScene!" % imported)
		return false
	if is_3d:
		for i: MeshInstance3D in get_children():
			remove_child(i)
			i.free()
		# Creating material
		var material:=StandardMaterial3D.new()
		if override_material:
			# material.cull_mode=BaseMaterial3D.CULL_DISABLED
			material.vertex_color_use_as_albedo=true
			material.roughness=0.5
			material.albedo_color=Color(1,1,1,1) if is_white else Color(0,0,0,1)
		# 3D theme
		var m: MeshInstance3D=meshes.pop_front()
		self.mesh=m.mesh
		# Colour
		if override_material: self.material_override=material
		else: self.material_override=m.material_override
		# Other
		self.scale=m.scale
		self.rotation=m.rotation
		# Same for children (if 3d object contained more than 1 mesh)
		for ch: MeshInstance3D in meshes:
			var msh:=MeshInstance3D.new()
			msh.mesh=ch.mesh
			msh.scale=ch.scale
			msh.rotation=ch.rotation
			if override_material: msh.material_override=material
			else: msh.material_override=ch.material_override
			msh.position=ch.position
			self.add_child(msh)
		for i: MeshInstance3D in meshes: i.free()
		if is_instance_valid(m): m.free()
	else:
		# 2D theme
		self.mesh=PlaneMesh.new()
		if is_3x3:
			self.scale=Vector3(3,0.1,3)
		(self as GeometryInstance3D).material_override=StandardMaterial3D.new()
		((self as GeometryInstance3D).material_override as StandardMaterial3D).transparency=BaseMaterial3D.TRANSPARENCY_ALPHA
		((self as GeometryInstance3D).material_override as StandardMaterial3D).albedo_texture=loaded_res
	return true

#endregion

## Class implementing the construction of chess pieces on a board
class_name ChessPieces
extends Node3D

#region Export Variables

## Size of the chessboard[br]
## [br]
## A chessboard is always a square (8x8 by default).
@export var size:=8 : 
	set(val):
		if size!=val:
			size=val
			regenerate()

## Layout of the pieces on the chessboard[br]
## [br]
## [code]layout[/code] should implement the following guidelines:[br]
## - [code]layout[/code] should have at least [code]size * size[/code] valid characters[br]
## - Valid characters include: [code]'p', 'n', 'b', 'r', 'q', 'k', 'm'[/code] and their uppercase
## variants (lower case is white, upper case is black)[br]
## - 'x' is also a valid character which is parsed to an empty square[br]
## - Any other characters won't get parsed, simply side stepped[br]
## - If the [code]layout[/code] has more than [code]size * size[/code] characters, the parser will check for 
## leaders[br]
## - Leaders can be given to the parser after the [code]size * size[/code] valid characters following
## format: [code]"kqMR"[/code] (this will make the king and queen the leaders of white and the
## monster and rook the leaders of black)[br]
## [br]
## Changing [code]layout[/code] automatically calls [code]regenerate()[/code].
@export var layout:="" : 
	set(val):
		if layout!=val:
			layout=val
			regenerate()

## Theme of the pieces[br]
## [br]
## [code]theme[/code] sets the path for individual pieces and thus is responsible for the appearance of pieces.
## Valid themes include [code]"2D"[/code], [code]"3D"[/code] and [code]"fish"[/code]. Themes containing the number
## [code]'3'[/code] will be considered a 3D theme, therefore they'll require 3D models to appear properly. Otherwise
## it will be considered a 2D theme and will need images instead. If [code]fish[/code] is present in the name of a 2D
## theme, each piece will occupy a [code]3x3[/code] square instead of a [code]1x1[/code].[br]
## [br]
## Changing [code]theme[/code] automatically calls [code]regenerate()[/code].
@export var theme:="3D" : 
	set(val):
		if theme!=val:
			theme=val
			regenerate()

#endregion

#region Ready

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	regenerate()

## Semi-constructor for ChessPieces[br]
## [br]
## Example:
## [codeblock]
## var chessPieces: ChessPieces = ChessPieces.new()
## chessPieces.assign(3, "nkn | xxx | NKN", "3D") # Make a new set of pieces on a 3x3 chessboard
## [/codeblock]
func assign(n: int, str_layout: String, str_theme: String) -> void:
	self.size=n
	self.layout=str_layout
	self.theme=str_theme

#endregion

#region Transition

## Removed Pieces associated with their death location in a stack[br]
## [br]
## [codeblock]removed_piece_stack: Dictionary[Vector2i,Array[PieceWithName]][/codeblock]
var removed_piece_stack:={}

## Transition from one position to the next with animations based on [code]instructions[/code][br]
## [br]
## Animation is implemented with the use of [code]Tween[/code]. Both forward and backward moves are
## implemented.
func transition(instructions: Array[Vec6], time: float) -> void:
	var mov_pieces: Array[Piece]=[]
	var rem_pieces: Array[Piece]=[]
	var creat_pieces: Array[Piece]=[]
	var mov_pos: Array[Vec6]=[]
	var creat_pos: Array[Vector2i]=[]
	var tw:=create_tween()
	# Sorting pieces
	for move: Vec6 in instructions:
		var p_moved:=get_piece_at(Vector2i(move.get_at(1),move.get_at(0)))
		if move.get_at(4)==0: continue
		if p_moved.is_empty():
			var p:=Piece.construct(Vector2i(-2,-2),Pieces.get_char_from_val(move.get_at(4)),theme)
			var pos:=Vector2i(move.get_at(3),move.get_at(2))
			var arr: Array=[] if pos not in removed_piece_stack else removed_piece_stack[pos]
			var piece_with_name: Glib.PieceWithName=arr.pop_back()
			if !p.is_empty():
				if piece_with_name!=null:
					p[0].grant_name(piece_with_name.name,piece_with_name.title)
				p[0].transparency=1
				add_child(p[0])
				creat_pieces.push_back(p[0])
				creat_pos.push_back(pos)
			continue
		var moved:=p_moved[0]
		if move.get_at(3)>=0 && move.get_at(2)>=0:
			mov_pieces.append(moved)
			mov_pos.append(move)
		else:
			moved.captured=true
			rem_pieces.append(moved)
	# Animations
	for i in mov_pieces.size():
		var piece: Piece=mov_pieces[i]
		var move: Vec6=mov_pos[i]
		piece.initial_position=Vector2i(move.get_at(3),move.get_at(2))
		Util.discard(tw.parallel().tween_property(piece,'position',Vector3(move.get_at(3)*2,0.8,move.get_at(2)*2),time))
	for i in creat_pieces.size():
		var piece:=creat_pieces[i]
		var pos:=creat_pos[i]
		piece.initial_position=pos
		piece.snap_to_initial_position()
		Util.discard(tw.parallel().tween_property(piece,'transparency',0,time))
	for piece in rem_pieces:
		var st: Array=removed_piece_stack[piece.initial_position]
		st.push_back(Glib.PieceWithName.PieceWithName(piece.type,piece.nameStr,piece.titleStr))
		Util.discard(tw.parallel().tween_property(piece,'transparency',1,time))
	# Wait for tween to finish
	await tw.finished
	# Board corrections
	for i in mov_pieces.size():
		var piece: Piece=mov_pieces[i]
		var move: Vec6=mov_pos[i]
		var res:=Pieces.get_char_from_val(move.get_at(4))
		if !piece.type.eq(res): piece.type=res
	for piece in rem_pieces:
		if is_instance_valid(piece):
			remove_child(piece)
			piece.free()

#endregion

#region Generate

## Remove all pieces and generate new ones based on [code]layout[/code]
func regenerate() -> void:
	removed_piece_stack.clear()
	for child in get_children():
		remove_child(child)
		child.free()
	generate()

## Generate a new set of chess pieces based on [code]layout[/code]
## [br]
## [b]If a piece doesn't have a mesh or image attached to it (loader should warn about that) it won't appear
## on board even if the engine is perfectly able to handle it.[/b]
func generate() -> void:
	var i:=0; var j:=0
	for chr in layout:
		if chr!=Pieces.get_char_from_name("empty").get_char():
		# Create new Optional Piece
			var opt_child:=Piece.construct(Vector2i(j,i),Char.from_string(chr),theme)
			# If piece is not constructed
			if opt_child.is_empty(): continue
			# Add piece to children
			var child:=opt_child[0]
			add_child(child)
		removed_piece_stack[Vector2i(j,i)]=[]
		# Increment indices
		j+=1
		if j==size:
			i+=1
			if i==size: break
			j=0

## Grant names for pieces[br]
## [br]
## [codeblock]names: Dictionary[Vector2i,PackedStringArray][/codeblock] 
func grant_names(names: Dictionary) -> void:
	for child: Piece in get_children():
		if child.initial_position in names:
			var n: PackedStringArray=names[child.initial_position]
			child.grant_name(n[0],"" if n.size()<2 else n[1])

## Grant custom names for pieces[br]
## [br]
## [codeblock]names: Dictionary[String,PackedStringArray][/codeblock]
func grant_custom_names(names: Dictionary) -> void:
	for type_and_num: String in names:
		var n: PackedStringArray=names[type_and_num]
		var type:=Char.from_string(type_and_num[0])
		for child: Piece in get_children():
			if child.type.eq(type):
				child.nameStr=n[0]
				if n.size()>=2: child.titleStr=n[1]
				break

#endregion

#region Functions

## Spawn phantom piece based on [code]chr[/code] at the specified square.
func spawn_phantom_piece_at(pos: Vector2i, chr: Char) -> void:
	var piece:=Piece.construct(pos,chr,theme)[0]
	piece.transparency=0.5
	add_child(piece)

## Remove all phantom pieces which this instance owns
func remove_phantom_pieces() -> void:
	for child: Piece in get_children():
		if child.transparency>0.1:
			remove_child(child)
			child.free()

## Get piece which is standing on square[br]
## [br]
## Returns the piece which has the same logical position ([code]initial_position[/code])
## as [code]pos[/code].
func get_piece_at(pos: Vector2i) -> Array[Piece]:
	for child: Piece in get_children():
		if child.initial_position==pos && !child.captured:
			return [child]
	return []

#endregion

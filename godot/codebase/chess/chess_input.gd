## This class implements the user input of the game.
##
## This script mainly communicates via signals such as [code]spawn_phantom[/code],
## [code]remove_phantom[/code], [code]exec_input[/code], [code]revert_input[/code],
## [code]show_promotion[/code] and [code]hide_promotion[/code], but it also modifies
## the position of the piece the player grabs.[br]
## It requires a ChessGame as its base. But creating a ChessGame automatically solves
## this issue.
class_name ChessInput
extends Node3D

#region Signals

## Signal for removing phantom pieces (semi-transparent pieces
## which highlight the starting position of the input)
signal remove_phantom
## Signal for spawning phantom pieces (semi-transparent pieces
## which highlight the starting position of the input)[br]
## [br]
## [codeblock] spawn_phantom: Signal[Vector2i, Char] [/codeblock]
signal spawn_phantom(position: Vector2i, type: Char)
## Signal for executing input[br]
## [br]
## [codeblock] exec_input: Signal[Vec6, bool=false] [/codeblock]
## [code]bool[/code] means whether input should be executed from history
## or not.
signal exec_input(input: Vec6, from_history: bool)
## Signal for reverting an input[br]
## [br]
## [codeblock] revert_input: Signal[Vec6, bool] [/codeblock]
## [code]bool[/code] stands for whether promotion happened or not.
signal revert_input(input: Vec6, is_promotion: bool)
## Signal for showing promotion
signal show_promotion
## Signal for hiding promotion
signal hide_promotion
## Signal for switching between planning and playing
signal toggle_planning

#endregion

#region Components

## Reference for a ChessGame
var base: WeakRef
## Check if input is blocked
var inputBlocked: bool=false
## Check if history (as in moving back and forth in the timeline) is blocked
var historyBlocked: bool=false
## Whether or not player can rewatch history
var can_undo: bool=true
## Whether or not player can plan
var can_plan: bool=true

#endregion

#region Init

## Gradually building input[br]
## [br]
## Input may be incomplete (if player is in the process of executing a move).
## Once an input is completed an [code]exec_input[/code] signal is sent to
## its base [code]ChessGame[/code].
var input:=Vec6.constr()
## Scroll Accumulation[br]
## [br]
## Makes scrolling input accumulate for more precise input
var scroll_accumulation:=0
## Scale of Scroll Accumulation
var scroll_accumulation_scale:=5
## Which piece is in the palms of the player[br]
## [br]
## This is an optional mutable reference to the piece.
## [codeblock]
## grabbed_piece: Option[&mut Piece]
## [/codeblock]
var grabbed_piece: Array[Piece]=[]

## Assign ChessGame to ChessInput[br]
## [br]
## Used mainly for signals
func assign(game: ChessGame) -> void:
	self.base=weakref(game)

## Block input
func block_input() -> void:
	self.inputBlocked=true

## Unblock input
func unblock_input() -> void:
	self.inputBlocked=false

## Block history
func block_history() -> void:
	self.historyBlocked=true

## Unblock history
func unblock_history() -> void:
	self.historyBlocked=false

#endregion

#region Input

## Get height of square[br]
## [br]
## The height of a square solely depends on which piece is standing on it.
func get_square_height(sq: Vector2i) -> float:
	var b: ChessGame=base.get_ref()
	for piece: Piece in b.chessPieces.get_children():
		if piece.initial_position==sq && piece.captured==false && piece.transparency<=0.1 && piece not in grabbed_piece:
			var aabb:=piece.get_aabb()
			return abs(aabb.size.y*piece.scale.y)
	return 0

## Transform raw [code]Vector3[/code] position into [code]Vector2i[/code] position[br]
## [br]
## This [code]Vector2i[/code] position can be used to determine which square the player
## selected.
func square_from_raw(raw: Vector3) -> Vector2i:
	# Base transformation: raw => 2*k => k
	# Reason: raw is in the range of 2k-1, 2k+1
	# Divide x and z positions by 2
	raw.x/=2; raw.z/=2
	#raw.x+=0.25; raw.z+=0.25
	# Round the results
	var rounded: Vector3i = raw.round()
	# Isolate x and z coordinates => (in)valid input
	return Vector2i(rounded.x,rounded.z)

func is_piece_colour_correct(piece: Piece) -> bool:
	var b: ChessGame=base.get_ref()
	return piece.type.is_lower() && b.chessEngine.get_turn() || !piece.type.is_lower() && !b.chessEngine.get_turn()

func move_back_in_history() -> void:
	var b: ChessGame=base.get_ref()
	var arr:=b.get_appropriate_forward_move()
	if not arr.is_empty(): revert_input.emit(Vec6.from8(arr[0]),arr[0].get_at(6)==1)

func move_forward_in_history() -> void:
	var b: ChessGame=base.get_ref()
	var arr:=b.get_appropriate_forward_move(1)
	if not arr.is_empty(): exec_input.emit(Vec6.from8(arr[0]),true)

func _input(event: InputEvent) -> void:
	if !historyBlocked:
		if event is InputEventMouseButton:
			var ev: InputEventMouseButton=event as InputEventMouseButton
			if can_undo && ev.button_index==MOUSE_BUTTON_WHEEL_DOWN:
				if scroll_accumulation>0: scroll_accumulation=0
				scroll_accumulation-=1
				if scroll_accumulation<=-scroll_accumulation_scale:
					scroll_accumulation=0
					move_back_in_history()
			elif can_undo && ev.button_index==MOUSE_BUTTON_WHEEL_UP:
				if scroll_accumulation<0: scroll_accumulation=0
				scroll_accumulation+=1
				if scroll_accumulation>=scroll_accumulation_scale:
					scroll_accumulation=0
					move_forward_in_history()
			elif can_plan && ev.button_index==MOUSE_BUTTON_RIGHT && ev.is_pressed():
				toggle_planning.emit()
		elif event.is_action_pressed("undo") && can_undo:
			move_back_in_history()
		elif event.is_action_released("redo") && can_undo:
			move_forward_in_history()
	if inputBlocked: return
	if event is InputEventMouseButton && (event as InputEventMouseButton).button_index==MOUSE_BUTTON_LEFT:
		# Get raw position with raycast
		var b: ChessGame=base.get_ref()
		var raw_pos := b.camera.project_position((event as InputEventMouseButton).position,b.camera.position.y)
		var pos := square_from_raw(raw_pos)
		# Change input based on event
		# Input is entirely uninitialised: add positions of event
		if input.is_uninit() && event.is_pressed():
			remove_phantom.emit()
			grabbed_piece=b.chessPieces.get_piece_at(pos)
			if !grabbed_piece.is_empty():
				if is_piece_colour_correct(grabbed_piece[0]):
					Util.discard(input.set_at(0,pos.y)); Util.discard(input.set_at(1, pos.x))
					spawn_phantom.emit(pos,grabbed_piece[0].type)
				else: grabbed_piece.clear()
		# Input is has 4 uninitialised parts: try to execute input (except promotion)
		elif input.count_uninit()==4 && event.is_released() && (input.get_at(0)!=pos.y || input.get_at(1)!=pos.x):
			Util.discard(input.set_at(2,pos.y)); Util.discard(input.set_at(3,pos.x))
			# Parse input
			var potential:=parse_input(Vec4.from6(input))
			if potential.is_empty():
				input.uninit()
			else:
				input=potential[0]
			grabbed_piece[0].snap_to_initial_position()
			# Execute input if input is not uninitialised (potential wasn't empty)
			if !input.is_uninit():
				# Normal moves
				if abs(input.get_at(4))!=1:
					exec_input.emit(input)
					# Clear input for future input
					input.uninit()
					remove_phantom.emit()
				# Promotion
				else: show_promotion.emit(input.get_at(4)==1, Vector2i(input.get_at(2),input.get_at(3)))
			else: input.uninit()
			remove_phantom.emit()
			grabbed_piece.clear()
		# Input is entirely initialised: promotion
		elif input.count_uninit()==0:
			# Get promoted value
			var prom:=b.promotion[0].get_ch_at(pos.x)
			# If value is valid execute move
			if !prom.eq(Char.new()):
				Util.discard(input.set_at(4,Pieces.get_val_of_char(prom)))
				exec_input.emit(input)
				hide_promotion.emit()
				# Clear input for future input
				input.uninit()
				remove_phantom.emit()
	elif event is InputEventMouseMotion:
		var b: ChessGame=base.get_ref()
		var pos := b.camera.project_position((event as InputEventMouseMotion).position,b.camera.position.y-5)
		if !grabbed_piece.is_empty(): grabbed_piece[0].position=Vector3(pos.x,0.81+get_square_height(square_from_raw(pos)),pos.z)

## Make an input of size 6 from an input of size 4[br]
## [br]
## Examples:[br]
## [codeblock]
## var x:=[6,3,4,3]
## var y:=ChessInput.parse_input(x)
## assert([6,3,4,3,-10,0]==y)
## [/codeblock]
func parse_input(inp: Vec4) -> Array[Vec6]:
	var b: ChessGame=base.get_ref()
	var inputs:=b.chessEngine.get_legal_moves()
	var potential: Array[Vec6]=inputs.filter(func(item: Vec6) -> bool: return Vec4.from6(item).eq(inp))
	if potential.is_empty(): return []
	elif potential.size()==1: return [potential[0]]
	else:
		var out:=potential[0]
		var sig: int=sign(out.get_at(4))
		Util.discard(out.set_at(4,sig))
		return [out]

## Clear input
func reset_input() -> void:
	self.input.uninit()

#endregion

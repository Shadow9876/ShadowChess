## Class implementing the construction and destruction of move hints
class_name MoveHints
extends Node3D

#region Materials

## Material for the standard move highlight (white by default)
var standard_material:=StandardMaterial3D.new()
## Material for the standard move contour (black by default)
var standard_contour:=StandardMaterial3D.new()
## Material for the capture move highlight (red by default)
var capture_material:=StandardMaterial3D.new()
## Material for the capture move contour (black by default)
var capture_contour:=StandardMaterial3D.new()

## Standard colour[br]
## [br]
## Colour of [code]standard_material[/code]. Modifying it changes
## [code]standard_material[/code] as well. White by default.
@export var standard_colour:=Color.WHITE :
    set(val):
        standard_material.albedo_color=val

## Standard contour colour[br]
## [br]
## Colour of [code]standard_contour[/code]. Modifying it changes
## [code]standard_contour[/code] as well. Black by default.
@export var standard_contour_colour:=Color.BLACK : 
    set(val):
        standard_contour.albedo_color=val

## Capture colour[br]
## [br]
## Colour of [code]capture_material[/code]. Modifying it changes
## [code]capture_material[/code] as well. Red by default.
@export var capture_colour:=Color.RED :
    set(val):
        capture_material.albedo_color=val

## Capture contour colour[br]
## [br]
## Colour of [code]capture_contour[/code]. Modifying it changes
## [code]capture_contour[/code] as well. Black by default.
@export var capture_contour_colour:=Color.BLACK : 
    set(val):
        capture_contour.albedo_color=val

#endregion

func _init() -> void:
    standard_material.albedo_color=standard_colour
    standard_contour.albedo_color=standard_contour_colour
    capture_material.albedo_color=capture_colour
    capture_contour.albedo_color=capture_contour_colour

## Generate move highlights based on moves
func generate(moves: Array[Vec6]) -> void:
    for move in moves:
        var pos:=Vector2i(move.get_at(2),move.get_at(3))
        if move.get_at(5)==0: new_standard(pos)
        else: new_capture(pos)

## Create a new standard move highlight on specified square
func new_standard(at: Vector2i) -> void:
    var res:=MeshInstance3D.new()
    res.mesh=PlaneMesh.new()
    res.material_override=standard_material
    res.rotate_y(deg_to_rad(45))
    res.scale=Vector3(0.5,0.5,0.5)
    res.position=Vector3(at.y*2,0.1,at.x*2)
    self.add_child(res)
    var res2:=MeshInstance3D.new()
    res2.mesh=PlaneMesh.new()
    res2.material_override=standard_contour
    res2.rotate_y(deg_to_rad(45))
    res2.scale=Vector3(0.6,0.6,0.6)
    res2.position=Vector3(at.y*2,0.09,at.x*2)
    self.add_child(res2)

## Create a new capture move highlight on specified square
func new_capture(at: Vector2i) -> void:
    var res:=MeshInstance3D.new()
    res.mesh=PlaneMesh.new()
    res.material_override=capture_material
    res.scale=Vector3(0.9,0.9,0.9)
    res.position=Vector3(at.y*2,0.1,at.x*2)
    self.add_child(res)
    var res2:=MeshInstance3D.new()
    res2.mesh=PlaneMesh.new()
    res2.material_override=capture_contour
    res2.position=Vector3(at.y*2,0.09,at.x*2)
    self.add_child(res2)

## Remove all highlights from board
func clear() -> void:
    for ch in get_children():
        self.remove_child(ch)
        ch.free()

class_name GameData
extends RefCounted

var main_line: Array[Vec8]=[]
var changed_past:=0
var used_planning:=0
var midnights:=0

func _to_string() -> String:
    return "{ main_line: %s, changed_past: %d, used_planning: %d, midnights: %d }" % [main_line,changed_past,used_planning,midnights]

static func eq(lhs: GameData, rhs: GameData) -> bool:
    if lhs.main_line.size()!=rhs.main_line.size(): return false
    for i in lhs.main_line.size():
        if !lhs.main_line[i].eq(rhs.main_line[i]): return false
    return true

static func GameData(history: Array[Array], lines: Array[int], planning: int, mid: int) -> GameData:
    var out:=GameData.new()
    var max_turn:=-1
    lines.reverse()
    for i in lines:
        history[i].reverse()
        for move: Vec8 in history[i]:
            if max_turn==-1 || move.get_at(7)<max_turn:
                out.main_line.push_front(move.clone())
        if !history[i].is_empty():
            var zero: Vec8=history[i][-1]
            max_turn=zero.get_at(7)
    out.changed_past=history.size()-1
    out.used_planning=planning
    out.midnights=mid
    return out

func save() -> Dictionary:
    var dict:={
        "main_line": main_line.map(func(i: Vec8) -> PackedInt32Array: return i.to_packed()),
        "changed_past": changed_past,
        "used_planning": used_planning,
        "midnights": midnights
    }
    return dict

static func load_data(dict: Dictionary) -> Array[GameData]:
    var key:=Util.missing_key(dict,["main_line","changed_past","used_planning","midnights"])
    if !key.is_empty():
        printerr("Loading Game Data failed! It's missing '%s'!" % key[0])
        return []
    
    var out:=GameData.new()
    var arr: Array=dict["main_line"];
    for i: Array in arr:
        var arr2: PackedInt32Array=[]
        for j: float in i:
            Util.discard(arr2.push_back(int(j)))
        var move: Vec8=Vec8.constr()
        move=Vec8.from_packed_arr(arr2)
        out.main_line.push_back(move)
    for i: String in ["changed_past", "used_planning", "midnights"]:
        var val: float=dict[i]
        out.set(i,int(val))
    return [out]
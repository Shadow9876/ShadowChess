## This is the class implements the board part of the game.
## 
## This script can generate a new [code] n * n [/code] board. With user defined
## [code] light [/code] and [code] dark [/code] colours.
class_name ChessBoard
extends Node3D

#region Signal(s)

## Camera pos[br]
## [br]
## A signal which updates the position of the camera
## giving it the middle of the board as parameters.[br]
## [br]
## Examples:[br]
## [codeblock]
## # Emitting signal
## camera_pos.emit(Vector2(size-1, size-1))
## [/codeblock]
signal camera_pos

#endregion

#region Export Variables

## Size[br]
## [br]
## Determines the size of the board. The board is 8x8 by default.
## Modifying this will redraw the board.
@export var size:=8 : 
	set(val):
		if size!=val:
			size=val
			regenerate()

## Light colour[br]
## [br]
## Determines the colour of the light tiles. It's white by default.
## Modifying this will redraw the board.
@export var light:=Color(1,1,1,1) : 
	set(val):
		if light!=val:
			light=val
			regenerate()

## Dark colour[br]
## [br]
## Determines the colour of the dark tiles. It's black by default.
## Modifying this will redraw the board.
@export var dark:=Color(0,0,0,1) : 
	set(val):
		if dark!=val:
			dark=val
			regenerate()

#endregion

#region Ready

## Semi-constructor for ChessBoard[br]
## [br]
## Example:
## [codeblock]
## var chessBoard: ChessBoard = ChessBoard.new()
## chessBoard.assign(10,Color(0,0,0,1),Color(1,1,1,1)) # Make a 10x10 chessboard with inverted colours
## [/codeblock]
func assign(n: int, colour_light: Color, colour_dark: Color) -> void:
	self.size=n
	self.dark=colour_dark
	self.light=colour_light

func _ready() -> void:
	regenerate()
	
#endregion

#region Functions

## Regenerate[br]
## [br]
## Clears and regenerates the board, emitting a signal to change
## camera position.[br]
## Example:
## [codeblock]
## var chessBoard:=ChessBoard.new()
## chessBoard.regenerate() # Redraw the chessboard (redundant)
## [/codeblock]
## But you don't really want to use it as simply setting [code]dark[/code],
## [code]light[/code] or [code]size[/code] automatically calls this function.
func regenerate() -> void:
	for child in get_children():
		remove_child(child)
		child.free()
	generate(size)
	camera_pos.emit(Vector2(size-1,size-1))

## Generate[br]
## [br]
## Generates the board based on [code]size[/code].
## Example:
## [codeblock]
## var chessBoard:=ChessBoard.new()
## for child in chessBoard.get_children():
## 	chessBoard.remove_child(child) # Remove everything from old chessboard
## chessBoard.generate(8) # Draw a new chessboard
## [/codeblock]
## Though if you just set [code]dark[/code], [code]light[/code] or [code]size[/code]
## this will all happen through [code]regenerate[/code] (as these setters will call it).
func generate(n: int) -> void:
	for i in n:
		for j in n:
			var this:=MeshInstance3D.new()
			this.mesh=PlaneMesh.new()
			(this.mesh as PlaneMesh).material=StandardMaterial3D.new()
			((this.mesh as PlaneMesh).material as StandardMaterial3D).albedo_color=light if (i+j)%2==0 else dark
			(this as Node3D).position=Vector3(i*2,0,j*2)
			add_child(this)

#endregion
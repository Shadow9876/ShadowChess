class_name Tag extends RefCounted

@export var inner: String = "" : 
    set(val):
        assert(val.length()==3,"Value must be a Tag and not a String! It has a length of {len} while it should have 3!".format({"len":val.length()}))
        assert(val.to_upper()==val,"Value must be upper case!")
        inner=val

static func Tag(tag: String) -> Tag:
    var out:=Tag.new()
    out.inner=tag
    return out

func _to_string() -> String:
    return self.inner
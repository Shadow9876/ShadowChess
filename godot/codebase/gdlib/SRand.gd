class_name SRand extends RefCounted

static var state: int

static func _static_init() -> void:
	var dict:=Time.get_time_dict_from_system()
	var hour: int = dict["hour"]
	var minute: int = dict["minute"]
	var second: int = dict["second"]
	var s: int=hour*3600+minute*60+second
	@warning_ignore("integer_division")
	state=rand_from_seed(s/10)[0]

## Generate a random number [code]n[/code], where [code]start <= n < end[/code]
static func rand2(start: int, end: int) -> int:
	if start>end: var tmp:=end; end=start; start=tmp
	assert(start!=end,"Invalid parameters given to 'rand'! This won't generate a random number!")
	var res: int
	res=state%(end-start)+start
	state=rand_from_seed(state)[0]
	return res

## Generate a random number [code]n[/code], where [code]0 <= n < end[/code]
static func rand(end: int) -> int:
	return rand2(0,end)

## Generate a random boolean
static func rand_b() -> bool:
	return rand(2)==0

## Generate a random ascii string with [code]length[/code]
static func rand_s(length: int) -> String:
	var out:=""
	for i in length:
		out+=char(rand2(33,127))
	return out

## Choose a random [code]String[/code] from a [code]PackedStringArray[/code]
static func rand_choice(arr: PackedStringArray) -> String:
	var i:=rand(arr.size())
	return arr[i]


static func save() -> int:
	return state

static func load_data(data: float) -> void:
	state=int(data)
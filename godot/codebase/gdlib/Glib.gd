class_name Glib
extends RefCounted

enum EventSettings {ALL, TEXT_ONLY, NONE}
enum StateEnd {GRAVEYARD, STATISTICS, CUSTOM}

enum EnvironmentalAnimation {NONE, ONLYONCE, ONCESTART, ONCEGAME}

class PieceWithName:
	var pieceType: Char
	var name: String
	var title: String
	static func PieceWithName(type: Char, NAME: String, TITLE: String) -> PieceWithName:
		var out:=PieceWithName.new()
		out.pieceType=type
		out.name=NAME
		out.title=TITLE
		return out
	func _to_string() -> String:
		return "( type: %s, name: %s )" % [pieceType.to_string(),name] if title.is_empty() else "( type: %s, name: %s, title: %s )" % [pieceType.to_string(),name,title]
	
	func save() -> Dictionary:
		var dict:={
			"pieceType": pieceType.get_char(),
			"name": name,
			"title": title
		}
		return dict

	static func load_data(dict: Dictionary) -> Array[PieceWithName]:
		var key:=Util.missing_key(dict,["pieceType","name","title"])
		if !key.is_empty():
			printerr("Loading PieceWithName failed! It's missing '%s'!" % key[0])
			return []

		var out:=PieceWithName.new()
		var s: String=dict["pieceType"]
		out.pieceType=Char.from_string(s)
		out.name=dict["name"]
		out.title=dict["title"]
		return [out]
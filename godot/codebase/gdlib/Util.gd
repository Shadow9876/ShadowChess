class_name Util
extends RefCounted

#region Load

static func try_load(file: String) -> Array[Resource]:
	if ResourceLoader.exists(file):
		return [load(file)]
	return []

static func load_or_crash(file: String) -> Resource:
	assert(ResourceLoader.exists(file),"Loading resource %s failed! Terminating program! If you wish this behaviour was different try 'try_load'!" % file)
	return load(file)

static func load_with_fallback(filename: String, path: String, fallback_path: String) -> Resource:
	var res:=try_load(path+filename)
	if !res.is_empty(): return res[0]
	var res2:=try_load(fallback_path+filename)
	assert(!res2.is_empty(),"Could not load file %s even with fallback path %s" % [filename,fallback_path])
	return res2[0]

#endregion

#region Image

## Load image from a [code]res://[/code] subfolder
static func load_image(file: String) -> Image:
	return (load_or_crash(file) as CompressedTexture2D).get_image()

## Save image as a png[br]
## [br]
## It should NOT be used to save maps
static func save_img_png(img: Image, file: String) -> void:
	err_handle(img.save_png(file))

#endregion

#region Read

static func try_read(file: String) -> Array[String]:
	if FileAccess.file_exists(file):
		return [FileAccess.open(file,FileAccess.READ).get_as_text()]
	return []

static func read_or_crash(file: String) -> String:
	assert(FileAccess.file_exists(file),"Loading file %s failed! Terminating program! If you wish this behaviour was different try 'try_read'!" % file)
	return FileAccess.open(file,FileAccess.READ).get_as_text()

static func read_with_fallback(filename: String, path: String, fallback_path: String) -> String:
	var res:=try_read(path+filename)
	if !res.is_empty(): return res[0]
	var res2:=try_read(fallback_path+filename)
	assert(!res2.is_empty(),"Could not read file %s even with fallback path %s" % [filename,fallback_path])
	return res2[0]

#endregion

#region Write

static func write_to_file(data: String, file: String) -> void:
	FileAccess.open(file,FileAccess.WRITE).store_string(data)

#endregion

#region Util

static func err_handle(error: Error) -> void:
	if error != OK:
		printerr("Program panicked: ", error)
		printerr(get_stack())

static func discard(_v: Variant) -> void:
	pass

#endregion

#region Strings

static func keep_letters(s: String) -> String:
	var out:=""
	var alpha:="abcdefghijklmnopqrstuvwxyz"
	alpha+=alpha.to_upper()
	for chr in s:
		out+=chr if chr in alpha else ""
	return out

static func get_num_from_letter(letter: String) -> int:
	var res:=0
	var alpha:="ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	for chr in letter:
		res*=alpha.length()
		res+=alpha.find(chr.to_upper())+1
	return res

static func keep_nums(s: String) -> String:
	var out:=""
	var nums:="0123456789"
	for chr in s:
		out+=chr if chr in nums else ""
	return out

static func is_lower(s: String) -> bool:
	return s.to_lower()==s

static func is_upper(s: String) -> bool:
	return s.to_upper()==s

#endregion

#region Arrays

static func union(u1: Array[Color], u2: Array[Color]) -> Array[Color]:
	var out: Array[Color]=[]
	for i: Variant in u1:
		if i in u2:
			out.push_back(i)
	return out

static func to_string_array(array: PackedColorArray) -> PackedStringArray:
	var out: PackedStringArray=[]
	for i in array:
		discard(out.push_back(i.to_html(false)))
	return out

static func to_color_array(array: PackedStringArray) -> PackedColorArray:
	var out: PackedColorArray=[]
	for i in array:
		discard(out.push_back(Color.from_string(i,Color.BLACK)))
	return out

static func save_piece_array(array: Array[Glib.PieceWithName]) -> Array:
	return array.map(func(item: Glib.PieceWithName) -> Dictionary: return item.save())

static func load_piece_array(array: Array) -> Array[Glib.PieceWithName]:
	var out: Array[Glib.PieceWithName]=[]
	for item: Dictionary in array:
		var opt_piece:=Glib.PieceWithName.load_data(item)
		if !opt_piece.is_empty():
			out.push_back(opt_piece[0])
	return out

#endregion

#region Dictionaries

static func to_string_dict(dict: Dictionary) -> Dictionary:
	var out:={}
	for key: Color in dict:
		var val: Color=dict[key]
		out[key.to_html(false)]=val.to_html(false)
	return out

static func to_color_dict(dict: Dictionary) -> Dictionary:
	var out:={}
	for key: String in dict:
		var val: String=dict[key]
		out[Color.from_string(key,Color.BLACK)]=Color.from_string(val,Color.BLACK)
	return out

static func missing_key(dict: Dictionary, keys: Array[String]) -> Array[String]:
	for key: String in keys:
		if !key in dict: return [key]
	return []

#endregion

#region Mesh

static func get_mesh(node: Node) -> Array[MeshInstance3D]:
	var out: Array[MeshInstance3D]=[]
	for child: MeshInstance3D in node.get_children():
		out.push_back(child)
		for grandchild: MeshInstance3D in child.get_children():
			out.push_back(grandchild)
			child.remove_child(grandchild)
		node.remove_child(child)
	node.free()
	return out

#endregion

#region System Fonts

static func get_distinct_sys_fonts() -> PackedStringArray:
	var out: PackedStringArray=[]
	var all:=OS.get_system_fonts()
	for font in all:
		var sub_font:=false
		for font2 in all:
			if font2 in font && font2!=font:
				sub_font=true
				break
		if !sub_font:
			discard(out.push_back(font))
	return out

#endregion

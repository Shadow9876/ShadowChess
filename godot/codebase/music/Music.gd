class_name Music
extends AudioStreamPlayer

## [codeblock]playlists: Dictionary[String,AudioStreamRandomizer][/codeblock]
var playlists: Dictionary={}
var current:=""
var base_volume:=0
var volume_percentage:=100

func set_volume_percentage(val: int) -> void:
    var tmp:=volume_percentage
    volume_percentage=val
    base_volume=-40 if volume_percentage==0 else int(linear_to_db(val/100.0))
    volume_db=base_volume
    if volume_percentage==0: self.stop()
    if tmp==0 && val!=0: self.play_random()

func _init() -> void:
    # Load playlists
    var dict: Dictionary=JSON.parse_string(Util.read_or_crash("res://imports/music/playlists.json"))
    for key: String in dict:
        var val: Array=dict[key]
        var list:=AudioStreamRandomizer.new()
        for music: String in val:
            list.add_stream(-1,Util.load_or_crash("res://imports/music/%s.ogg" % music) as AudioStream)
        playlists[key]=list
    Util.err_handle(self.finished.connect(play_random))

func stream_playlist(playlist: String, start_with: String="") -> void:
    if playlist not in playlists || volume_percentage==0: return
    restore_volume()
    if !start_with.is_empty():
        current=start_with
        self.stream=Util.load_or_crash("res://imports/music/%s.ogg" % start_with)
        self.play()
        await self.finished
        if current!=start_with: return
    self.stream=playlists[playlist]
    current=playlist
    play_random()

func fadeout(time: float=2.0) -> void:
    self.volume_db=base_volume-1
    for i in range(2,21):
        if self.volume_db!=base_volume-(i-1) || volume_percentage==0: return
        if self.volume_db==base_volume-(i-1) && volume_percentage!=0: self.volume_db=base_volume-i
        if !is_inside_tree(): return
        await get_tree().create_timer(time/20.0).timeout
    if self.volume_db!=base_volume: stop_playlist()

func fadein(playlist: String, time: float=1.0) -> void:
    if playlist not in playlists || volume_percentage==0: return
    self.volume_db=base_volume-20
    self.stream=playlists[playlist]
    current=playlist
    play_random()
    for i in range(19,-1,-1):
        if self.volume_db!=base_volume-(i+1) || volume_percentage==0: return
        if self.volume_db==base_volume-(i+1) && volume_percentage!=0: self.volume_db=base_volume-i
        if !is_inside_tree(): return
        await get_tree().create_timer(time/20.0).timeout

func stop_playlist() -> void:
    self.stream=null
    current=""
    self.stop()

func restore_volume() -> void:
    self.volume_db=base_volume

func play_random() -> void:
    if self.current.is_empty() || volume_percentage==0: return
    self.play()

func get_time_left_with_possible_restart() -> float:
    var fst:=((self.stream as AudioStreamRandomizer).get_stream(0).get_length()) - self.get_playback_position()
    var snd:=fst + ((self.stream as AudioStreamRandomizer).get_stream(0).get_length())
    return fst if fst>=40 else snd
class_name Map3D
extends Node3D

var water: Water=null

var environment: ChessEnvironment=null

var darkness: Darkness=null

var bg_col:=Color.BLACK

@export var path : String = "" :
	set(val):
		if path!=val:
			path=val
			if water==null || !is_instance_valid(water):
				water=Water.new()
			water.initialise(path)

func _ready() -> void:
	reset()

func reset() -> void:
	if get_child_count()>0:
		for i in get_children():
			remove_child(i)
			i.free()
	environment=preload("res://codebase/chess/environment.tscn").instantiate()
	if water==null || !is_instance_valid(water):
		water=Water.new()
	darkness=Darkness.new()
	water.initialise(path)
	var bg:=MeshInstance3D.new()
	bg.mesh=PlaneMesh.new()
	bg.material_override=StandardMaterial3D.new()
	(bg.material_override as StandardMaterial3D).albedo_color=bg_col
	bg.scale=Vector3(10,10,10)
	bg.position.y=-1
	add_child(bg)
	add_child(water)
	add_child(environment)
	add_child(darkness)
	darkness.hide()
	environment.set_dsmd(0)
	environment.tr_speed=0

func darken_water(col_decr : float = 0.12, norm_decr : float = 0.03, rough_incr : float = 0.03) -> void:
	return water.darken_water(col_decr,norm_decr,rough_incr)

func lighten_water(col_incr : float = 0.12, norm_incr : float = 0.03, rough_decr : float = 0.03) -> void:
	return water.lighten_water(col_incr,norm_incr,rough_decr)

func set_water_colour(col: Color) -> void:
	return water.set_water_colour(col)

func set_normal_multiplier(norm: float) -> void:
	return water.set_normal_multiplier(norm)

func set_roughness(rough: float) -> void:
	return water.set_roughness(rough)
class_name MapData
extends Node

#region Signals

signal refresh

#endregion

#region Export Variables

@export var path: String : 
	set(val):
		if val!=path:
			path=val
			prov=Util.load_image(path+"img/prov.png")
			map=Util.load_image(path+"img/0.png")

@export var water_colour: Color=Color.from_string("a3f8f7",Color.BLUE)

#endregion

#region Components

var prov: Image

var map: Image

## Province Rectangles[br]
## [br]
## [codeblock]rects: Dictionary[Color,Rect2i][/codeblock]
## It's a dictionary of province colours associated with their
## bounding box.
var rects: Dictionary
## Province Connections[br]
## [br]
## [codeblock]rects: Dictionary[Color,Array[Color]][/codeblock]
## where colors represent that these two provinces are connected.
var connections: Dictionary
## Owners of Provinces [br]
## [br]
## [codeblock]owners: Dictionary[Color1,Color2][/codeblock][br]
## where [code]Color1[/code] is the colour of the province and
## [code]Color2[/code] is the colour of the owner.
var owners: Dictionary

## [codeblock]regions: Array[Dictionary[Color1,Color2]][/codeblock][br]
## where [code]Color1[/code] is the colour of the province and
## [code]Color2[/code] is the colour of the attribute.
var attr: Array[Dictionary]

#endregion

#region Ready

func _ready() -> void:
	var data:=ImageUtil.get_map_data(path,prov,water_colour)
	rects=data[0]
	connections=data[1]
	owners=data[2]
	attr=data.slice(3)

#endregion

#region Fill

func fill_province(prov_colour: Color, fill_colour: Color) -> void:
	map=ImageUtil.fill_province(map,prov,prov_colour,fill_colour,rects)
	owners[prov_colour]=fill_colour

func get_prov_owner_colour(prov_col: Color) -> Color:
	var r: Rect2i=rects[prov_col]
	for y in range(r.position.y,r.end.y):
		for x in range(r.position.x,r.end.x):
			if prov.get_pixel(x,y)==prov_col:
				return map.get_pixel(x,y)
	assert(false,"Unreachable")
	return Color.BLACK

func annex_country(annexed_colour: Color, new_owner: Color) -> void:
	for col: Color in owners:
		var col2: Color=owners[col]
		if col2==annexed_colour:
			fill_province(col,new_owner)
			owners[col]=new_owner
	if annexed_colour in highlighted_owners:
		Util.discard(highlighted_provinces.erase(annexed_colour))
		highlighted_owners.remove_at(highlighted_owners.find(annexed_colour))
	refresh.emit()

#endregion

#region Highlight

var counter:=0.0
var decrease:=false
var highlighted_provinces: Dictionary={}
var highlighted_owners: PackedColorArray=[]
var util:=ImageUtil.new()

func highlight_country(own: Color) -> void:
	var arr: PackedColorArray=[]
	for col: Color in owners:
		var col2: Color=owners[col]
		if col2!=water_colour && col2==own:
			var __:=arr.push_back(col)
	highlighted_provinces[own]=arr
	var _2:=highlighted_owners.push_back(own)

func end_highlight() -> void:
	for col: Color in highlighted_provinces:
		for col2: Color in highlighted_provinces[col]:
			map=util.fill_cached_provinces(map,prov,col2,col,rects)
	refresh.emit()
	highlighted_provinces.clear()
	highlighted_owners.clear()
	util.cache.clear()

func _process(delta: float) -> void:
	if highlighted_provinces.size()>0:
		for highlight_colour: Color in highlighted_provinces:
			var new_col:=highlight_colour.darkened(counter) if counter>0 else highlight_colour.lightened(-counter)
			for col2: Color in highlighted_provinces[highlight_colour]:
				map=util.fill_cached_provinces(map,prov,col2,new_col,rects)
		refresh.emit()
		if counter>=0.5: decrease=true
		elif counter<=-0.5: decrease=false
		if !decrease: counter+=delta
		else: counter-=delta

#endregion

#region Save

func save() -> Dictionary:
	var dict:={
		"owners": Util.to_string_dict(owners)
	}
	return dict

func load_data(dict: Dictionary) -> bool:
	if "owners" not in dict:
		printerr("Loading MapData failed! It's missing 'owners'!")
		return false
	
	var d: Dictionary=dict["owners"]
	owners=Util.to_color_dict(d)
	for key: Color in owners:
		var val: Color=owners[key]
		if get_prov_owner_colour(key)!=val:
			fill_province(key,val)
	return true

#endregion

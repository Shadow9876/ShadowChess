class_name ImageUtil
extends RefCounted

static func get_map_data(path: String, prov: Image, water_colour: Color) -> Array[Dictionary]:
	if FileAccess.file_exists(path+"data/map.dat"): return load_map_data(path)
	var data:=calc_map_data(path,prov,water_colour)
	write_map_data(path,data)
	return data

static func calc_map_data(path: String, prov: Image, water_colour: Color) -> Array[Dictionary]:
	var rects:=calc_rect(prov)
	var connections:=calc_connections(prov,water_colour,rects)
	var arr: Array[Dictionary]=[rects,connections]
	var attr:=0
	while FileAccess.file_exists("%simg/%d.png" % [path, attr]):
		arr.push_back(calc_attribute(Util.load_image("%simg/%d.png" % [path, attr]), prov, rects))
		attr+=1
	return arr

static func write_map_data(path: String, data: Array[Dictionary]) -> void:
	var rects:=data[0]
	var connections:=data[1]
	var attr:=data.slice(2)
	var string:="colour rect"
	for i in attr.size():
		string+=" %s" % i
	string+=" connections\n"
	for col: Color in rects:
		var r: Rect2i=rects[col]
		string+="%s %s %s %s %s" % [col.to_html(false),r.position.x,r.position.y,r.size.x,r.size.y]
		for d: Dictionary in attr:
			var col2: Color=d[col]
			string+=" %s" % col2.to_html(false)
		var con: PackedColorArray=connections[col]
		for c: Color in con:
			string+=" %s" % c.to_html(false)
		string+="\n"
	Util.write_to_file(string, path+"data/map.dat")

static func load_map_data(path: String) -> Array[Dictionary]:
	var out: Array[Dictionary]=[{},{}]
	# Attribute indexes
	var col_idx:=-1
	var rect_idx:=-1
	var attr_idx:=-1
	var attr_len:=0
	var con_idx:=-1 # Connections will always be the last
	var s:=Util.read_or_crash(path+"data/map.dat")
	# Parse headers
	var header:=s.split("\n")[0]
	var i:=0
	for head: String in header.split(" "):
		if head=="colour": col_idx=i
		elif head=="rect": rect_idx=i; i+=3
		elif head=="connections": con_idx=i
		elif attr_idx==-1 && head.is_valid_int(): attr_idx=i
		if attr_idx!=-1 && head.is_valid_int(): attr_len+=1; out.push_back({})
		i+=1
	# Parse lines
	assert(col_idx>=0,"There is no 'colour' attribute in '%sdata/map.dat'!" % path)
	assert(rect_idx>=0,"There is no 'rect' attribute in '%sdata/map.dat'!" % path)
	assert(attr_idx>=0,"There is no 'attr' attribute in '%sdata/map.dat'!" % path)
	assert(con_idx>=0,"There is no 'connections' attribute in '%sdata/map.dat'!" % path)
	var lines:=s.split("\n").slice(1)
	for line: String in lines:
		var packed:=line.split(" ")
		if packed.size()<2: continue
		var col: Color=Color.from_string(packed[col_idx],Color.BLACK)
		# Rects
		var r:=Rect2i(int(packed[rect_idx]),int(packed[rect_idx+1]),int(packed[rect_idx+2]),int(packed[rect_idx+3]))
		out[0][col]=r
		# Attributes
		for j in range(0,attr_len):
			var col2: Color=Color.from_string(packed[attr_idx+j],Color.BLACK)
			out[2+j][col]=col2
		# Connections
		var arr:=[]
		for con in packed.slice(con_idx):
			arr.push_back(Color.from_string(con,Color.BLACK))
		out[1][col]=arr
	return out

static func calc_rect(prov: Image) -> Dictionary:
	var d:={}
	for y in prov.get_height():
		for x in prov.get_width():
			var col:=prov.get_pixel(x, y);
			if col not in d: d[col]=Rect2i(x, y, 1, 1)
			else: 
				var r: Rect2i=d[col]
				d[col]=r.expand(Vector2i(x,y))
	return d

static func calc_connections(prov: Image, water_colour: Color, prov_rect: Dictionary) -> Dictionary:
	var connections: Dictionary={}
	var colours:=prov_rect.keys()
	var ind:=colours.find(water_colour)
	if ind>=0:
		colours.remove_at(ind)
		connections[water_colour]=[]
	var graph:=Graph.construct(colours.size())
	for i: int in colours.size():
		var col1: Color=colours[i]
		for j: int in colours.size():
			var col2: Color=colours[j]
			if col1==col2: graph.set2(i,j,false); continue
			var r1: Rect2i=prov_rect[col1]; var r2: Rect2i=prov_rect[col2]
			if graph.is_undefined(i,j): graph.set2(i,j,are_connected(prov,col1,r1,col2,r2))
	for i: int in colours.size():
		connections[colours[i]]=[]
		for j: int in colours.size():
			if graph.get2(i,j):
				var arr: Array=connections[colours[i]]
				arr.push_back(colours[j])
	return connections

static func are_connected(prov: Image, p1: Color, r1: Rect2i, p2: Color, r2: Rect2i) -> bool:
	if !r1.intersects(r2): return false
	var inter_area: Rect2i=r1.intersection(r2)
	for y in range(inter_area.position.y+1,inter_area.end.y-1):
		for x in range(inter_area.position.x+1,inter_area.end.x-1):
			if prov.get_pixel(x,y)==p1 && (prov.get_pixel(x+1,y)==p2 || prov.get_pixel(x-1,y)==p2 || prov.get_pixel(x,y+1)==p2 || prov.get_pixel(x,y-1)==p2):
				return true
	return false

static func calc_attribute(attr: Image, prov: Image, prov_rect: Dictionary) -> Dictionary:
	var d:={}
	for col: Color in prov_rect:
		var r: Rect2i=prov_rect[col]
		var found: bool=false
		for y in range(r.position.y,r.end.y):
			for x in range(r.position.x,r.end.x):
				if prov.get_pixel(x,y)==col:
					d[col]=attr.get_pixel(x,y)
					found=true
				if found: break
			if found: break
	return d

static func fill_province(img: Image, prov: Image, prov_colour: Color, fill_colour: Color, prov_rect: Dictionary) -> Image:
	var r: Rect2i=prov_rect[prov_colour]
	for y in range(r.position.y,r.end.y+1):
		for x in range(r.position.x,r.end.x+1):
			if prov.get_pixel(x,y)==prov_colour: img.set_pixel(x,y,fill_colour)
	return img

var cache: Dictionary={}

func fill_cached_provinces(img: Image, prov: Image, prov_colour: Color, fill_colour: Color, prov_rect: Dictionary) -> Image:
	if cache.size()>40: cache.clear()
	if prov_colour not in cache:
		var r: Rect2i=prov_rect[prov_colour]
		var arr: Array[Vector2i]=[]
		for y in range(r.position.y,r.end.y):
			for x in range(r.position.x,r.end.x):
				if prov.get_pixel(x,y)==prov_colour:
					img.set_pixel(x,y,fill_colour)
					arr.push_back(Vector2i(x,y))
		cache[prov_colour]=arr
	else:
		for v: Vector2i in cache[prov_colour]:
			img.set_pixelv(v,fill_colour)
	return img

class_name Darkness
extends Node3D

var darkness: MeshInstance3D=MeshInstance3D.new()

func _ready() -> void:
    initialise()
    add_child(darkness)

func initialise() -> void:
    darkness.mesh=PlaneMesh.new()

    var shader:=ShaderMaterial.new()
    shader.shader=Util.load_or_crash("res://imports/maps/darkness.gdshader")

    shader.set_shader_parameter("density",0.2)
    var noise := NoiseTexture2D.new()
    noise.seamless=true
    noise.as_normal_map=true
    noise.noise=FastNoiseLite.new()
    (noise.noise as FastNoiseLite).noise_type=FastNoiseLite.TYPE_PERLIN
    shader.set_shader_parameter("noise",noise)

    darkness.material_override=shader

    darkness.scale=Vector3(5,5,2.5)
    darkness.position.y=5

func increase_density(inc: float=0.1) -> void:
    var prev: float=(darkness.material_override as ShaderMaterial).get_shader_parameter("density")
    (darkness.material_override as ShaderMaterial).set_shader_parameter("density",prev+inc)

func decrease_density(dec: float=0.1) -> void:
    var prev: float=(darkness.material_override as ShaderMaterial).get_shader_parameter("density")
    (darkness.material_override as ShaderMaterial).set_shader_parameter("density",prev-dec)
class_name Country
extends RefCounted

#region Properties

var colour: Color=Color.BLACK

var tag: Tag=Tag.new()

var name: String=""

var provinces: PackedColorArray=[]

var attr: PackedColorArray=[]

var strength: float=0

var overlord: Array[Tag]=[]

var attack_message: String=""

var victory_message: String=""

var defeat_message: String=""

#endregion

#region Constructor

static func construct(COLOUR: Color, TAG: Tag, NAME: String, owners: Dictionary, messages: PackedStringArray) -> Country:
    var country:=Country.new()
    country.colour=COLOUR
    country.tag=TAG
    var sp:=NAME.replace("`","").split(".")
    if sp.size()>1:
        country.name=sp[1]
        country.overlord=[Tag.Tag(sp[0])]
    else:
        country.name=sp[0]
        country.overlord=[]
    for col: Color in owners:
        if owners[col]==COLOUR:
            var __:=country.provinces.push_back(col)
    country.strength=country.provinces.size()*float(SRand.rand2(2,9))/5
    for msg in messages:
        if country.attack_message.is_empty(): country.attack_message=msg.replace("`","")
        elif country.victory_message.is_empty(): country.victory_message=msg.replace("`","")
        elif country.defeat_message.is_empty(): country.defeat_message=msg.replace("`","")
    return country

#endregion

#region Annex

func annex_country(country: Country) -> void:
    strength*=float(SRand.rand(10))/10
    strength+=country.strength*float(SRand.rand2(1,4))/4
    provinces+=country.provinces.duplicate()

#endregion

#region Save

func save() -> Dictionary:
    var dict:={
        "colour": colour.to_html(false),
        "tag": tag.inner,
        "overlord": "empty" if self.overlord.is_empty() else self.overlord[0].inner,
    }
    for st: String in ["name","attack_message","victory_message","defeat_message","strength"]:
        dict[st]=self.get(st)
    for st: String in ["provinces","attr"]:
        var a: PackedColorArray=self.get(st)
        dict[st]=Util.to_string_array(a)
    return dict

static func load_data(dict: Dictionary) -> Array[Country]:
    var key:=Util.missing_key(dict,["colour","tag","overlord","name","attack_message","victory_message","defeat_message","strength","provinces","attr"])
    if !key.is_empty():
        printerr("Loading Country failed! It's missing '%s'!" % key[0])
        return []

    var out:=Country.new()
    var c: String=dict["colour"]; out.colour=Color.from_string(c,Color.BLACK)
    var t: String=dict["tag"]; out.tag=Tag.Tag(t)
    var o: String=dict["overlord"]
    if o=="empty":
        out.overlord=[]
    else:
        out.overlord=[Tag.Tag(o)]
    for st: String in ["name","attack_message","victory_message","defeat_message","strength"]:
        out.set(st,dict[st])
    for st: String in ["provinces","attr"]:
        var a: PackedStringArray=dict[st]
        out.set(st,Util.to_color_array(a))
    return [out]

#endregion
class_name Campaign
extends RefCounted

#region Attributes

var player: Color=Color.BLACK
var inner: Array[Choice]=[]
var counter:=0
var ind:=0
var ind_ext:=0

#endregion

#region Functions

static func Campaign(data: String) -> Campaign:
    var out:=Campaign.new()
    var macros:={}
    for line: String in data.split("\n"):
        var packed:=line.split(": ")
        if packed.size()>=2:
            packed[0]=packed[0].replace("extern ","")
            macros[packed[0]]=": ".join(packed.slice(1))
    if "$player" in macros:
        var string: String=macros["$player"]
        out.player=Color.from_string(string,Color.BLACK)
    for line: String in data.split("\n"):
        if line=="..." && out.inner.size()!=0:
            for i in out.inner[-1].choices.size()-1:
                out.inner.push_back(out.inner[-1].dupicate())
            continue
        var valid:=line.split(": ")
        if valid.size()!=1: continue
        for m: String in macros:
            var r: String=macros[m]
            if m in line: line=line.replace(m,r)
        if line.length()<3: continue
        var ch:=Choice.new()
        ch.choices=[]
        var sp:=line.split("|")
        for line2: String in sp:
            var ops:=Opponents.new()
            ops.opponents=[]
            var sp2:=line2.split("&")
            var cond:=""
            for s in sp2:
                var op:=Opponent.new()
                if "!" in s or "[" in s or "]" in s or "=" in s:
                    cond=s
                    continue
                elif "COUNT" in s || "STATE" in s:
                    op=Opponent.State(s)
                else:
                    var sp3:=s.replace(" ","").replace("\t","").split("+")
                    if sp3.size()==1:
                        op=Opponent.Opponent(Color.from_string(sp3[0],Color.BLACK),0)
                    else:
                        op=Opponent.Opponent(Color.from_string(sp3[0],Color.BLACK),int(sp3[1]))
                ops.opponents.push_back(op)
            ops.condition=cond
            ch.choices.push_back(ops)
        out.inner.push_back(ch)
    return out

func get_next_encounter() -> Array[Choice]:
    if self.inner.size()<=counter: return []
    var out:=self.inner[counter]
    if !Choice.eq(self.inner[counter],self.inner[counter-1]):
        self.ind+=1
        self.ind+=ind_ext
        ind_ext=0
    else:
        ind_ext+=1
    self.counter+=1
    return [out]

func get_current_encounter() -> Array[Choice]:
    if self.inner.size()<=counter || counter==0: return []
    return [self.inner[counter-1]]

#endregion

#region AnnexRegions

class AnnexRegion:
    var col: Color
    var level: int
    var time_left: int
    static func AnnexRegion(COL: Color, LEVEL: int, TIME_LEFT: int) -> AnnexRegion:
        var out:=AnnexRegion.new()
        out.col=COL
        out.level=LEVEL
        out.time_left=TIME_LEFT
        return out
    func _to_string() -> String:
        return "AnnexRegion( col: %s, level: %d, time_left: %d )" % [col.to_html(),level,time_left]

func get_annex_goals() -> Array[AnnexRegion]:
    var out: Array[AnnexRegion]=[]
    for i in range(max(counter-1,0),inner.size()):
        var ch:=inner[i]
        for opponent in ch.get_opponents():
            if opponent.level>0:
                var annexregion:=AnnexRegion.AnnexRegion(opponent.col,opponent.level,i-counter+1)
                var found:=false
                for reg in out:
                    if reg.col==annexregion.col && reg.level==annexregion.level:
                        found=true
                        break
                if !found: out.push_back(annexregion)
    return out

#endregion

#region Opponents

class Opponent extends RefCounted:
    var col: Color
    var level: int
    var state_name: String
    func is_state() -> bool:
        return self.level<0
    static func Opponent(colour: Color, LEVEL: int) -> Opponent:
        var out:=Opponent.new()
        out.col=colour
        out.level=LEVEL
        return out
    static func State(name: String) -> Opponent:
        var out:=Opponent.new()
        out.level=-1
        out.state_name=name
        return out
    static func eq(l: Opponent, r: Opponent) -> bool:
        return l.col==r.col && l.level==r.level && l.state_name==r.state_name
    func is_end() -> bool:
        return "state_end" in self.state_name.to_lower()
    func _to_string() -> String:
        return "( %s + %s )" % [col.to_html(),level] if !self.is_state() else "( state_name: %s )" % state_name

class Opponents extends RefCounted:
    var opponents: Array[Opponent]
    var condition: String
    static func eq(l: Opponents, r: Opponents) -> bool:
        if l.condition!=r.condition || l.opponents.size()!=r.opponents.size(): return false
        for i in l.opponents.size():
            if !Opponent.eq(l.opponents[i],r.opponents[i]): return false
        return true
    func has_end() -> bool:
        for opponent in opponents:
            if opponent.is_end(): return true
        return false
    func _to_string() -> String:
        return "[ %s , condition: %s ]" % [", ".join(opponents.map(func(opponent: Opponent) -> String: return opponent.to_string())),condition] if !condition.is_empty() else "[ %s ]" % ", ".join(opponents.map(func(opponent: Opponent) -> String: return opponent.to_string()))

class Choice extends RefCounted:
    var choices: Array[Opponents]
    func dupicate() -> Choice:
        var out:=Choice.new()
        out.choices=choices.duplicate(true)
        return out
    func get_opponents() -> Array[Opponent]:
        var out: Array[Opponent]=[]
        for opponents in choices:
            for opponent in opponents.opponents:
                out.push_back(opponent)
        return out
    static func eq(l: Choice, r: Choice) -> bool:
        if l.choices.size()!=r.choices.size(): return false
        for i in l.choices.size():
            if !Opponents.eq(l.choices[i],r.choices[i]): return false
        return true
    func has_end() -> bool:
        for opponents in choices:
            if opponents.has_end(): return true
        return false
    func _to_string() -> String:
        return "{ %s }" % ", ".join(choices.map(func(opponents: Opponents) -> String: return opponents.to_string()))

#endregion

#region Save

func save() -> Dictionary:
    var dict:={
        "counter": counter,
        "ind": ind,
        "ind_ext": ind_ext
    }
    return dict

func load_data(dict: Dictionary) -> bool:
    var key:=Util.missing_key(dict,["counter","ind","ind_ext"])
    if !key.is_empty():
        printerr("Loading Campaign failed! It's missing '%s'!" % key[0])
        return false

    for k: String in dict:
        var f: float=dict[k]
        self.set(k,int(f))
    return true

#endregion

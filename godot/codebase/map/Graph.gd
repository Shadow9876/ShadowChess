class_name Graph
extends RefCounted

var n: int=0
var graph: Array[PackedByteArray]=[]

static func construct(N: int) -> Graph:
    var g:=Graph.new()
    g.n=N; g.graph=[]
    for i in N:
        var arr: PackedByteArray=[]
        for j in N:
            var __:=arr.push_back(127)
        g.graph.push_back(arr)
    return g

func is_undefined(i: int, j: int) -> bool:
    var r:=graph[i][j]
    return r!=0 && r!=1

func get2(i: int, j: int) -> bool:
    return graph[i][j]!=0

func set2(i: int, j: int, val: bool) -> void:
    graph[i][j]=1 if val else 0
    graph[j][i]=1 if val else 0

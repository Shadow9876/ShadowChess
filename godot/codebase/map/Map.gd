class_name Map
extends Node3D

#region Signals

signal start_game
signal state_end

#endregion

#region Components

var path : String = "res://imports/maps/"

@export var map_name: String:
    set(val):
        if val!=map_name:
            var tmp:=map_name
            map_name=val
            if load_mesh() && FileAccess.file_exists(path+map_name+"/data/campaign.dat"):
                map3d.path=path+map_name+"/"
                countries.path=path+map_name+"/"
                countries.generate_countries()
                campaign=Campaign.Campaign(Util.read_or_crash(path+map_name+"/data/campaign.dat"))
                refresh()
            else: map_name=tmp

var mesh: MeshInstance3D=MeshInstance3D.new()

var map3d:= Map3D.new()

var camera: Camera3D=Camera3D.new()

var countries: Countries=Countries.new()

var message: Label=Label.new()

var campaign: Campaign=Campaign.new()

var current_choice: Campaign.Choice=Campaign.Choice.new()

var base: WeakRef

#endregion

#region Ready

func load_mesh() -> bool:
    mesh.mesh=PlaneMesh.new()
    mesh.ignore_occlusion_culling=false
    (mesh.mesh as PlaneMesh).subdivide_depth=230
    (mesh.mesh as PlaneMesh).subdivide_width=230

    var shader:=ShaderMaterial.new()
    var res:=Util.try_load("res://imports/maps/height.gdshader")
    if res.is_empty(): return false
    var height:=Util.try_load(path+map_name+"/mesh/height.jpg")
    if height.is_empty(): return false
    var normal:=Util.try_load(path+map_name+"/mesh/normal.jpg")
    if normal.is_empty(): return false
    shader.shader=res[0]
    shader.set_shader_parameter("height_scale",0.15)
    shader.set_shader_parameter("height",height[0])
    #shader.set_shader_parameter("img",null)
    shader.set_shader_parameter("normal",normal[0])
    mesh.material_override=shader
    return true

func _init() -> void:
    Util.discard(countries.data.refresh.connect(refresh))

    camera.position.y=20
    camera.rotate_x(deg_to_rad(-90))

    message.add_theme_color_override("font_outline_color",Color.BLACK)
    message.add_theme_constant_override("outline_size",5)

func _ready() -> void:
    add_child(camera)
    add_child(map3d)
    add_child(countries)
    add_child(mesh)
    add_child(message)

    mesh.scale.z=float(countries.data.map.get_size().y)/countries.data.map.get_size().x
    camera.fov=mesh.scale.z*91.31/(camera.position.y/1.24)

    if get_next_encounter(): highlight_next()

#endregion

#region Highlight

func _input(event: InputEvent) -> void:
    if event is InputEventMouse:
        message.text=""
        var img_size:=countries.data.map.get_size()
        var window_size:=get_window().size
        var upscaled_size:=img_size*window_size.y/img_size.y
        var margin:=window_size.x-upscaled_size.x
        var event_pos:=(event as InputEventMouse).position
        event_pos.x-=margin/2.0
        event_pos*=img_size.y/float(window_size.y)
        var event_pos2:=event_pos.round()
        var data_size:=countries.data.prov.get_size()
        var pix:=Color.WHITE if OS.get_name()=="Web" else DisplayServer.screen_get_pixel(Vector2i(int((event as InputEventMouse).position.x),int((event as InputEventMouse).position.y)))
        if current_choice.has_end() && (!(event_pos2.x>=20 && event_pos2.y>=20 && event_pos2.x<data_size.x-20 && event_pos2.y<data_size.y-20) || pix.r+pix.g+pix.b<0.2):
            message.text="It is the end"
            var pos:=(event as InputEventMouse).position
            pos.x+=40
            message.position=pos
            if event.is_pressed():
                state_end.emit()
        elif event_pos2.x>=0 && event_pos2.y>=0 && event_pos2.x<data_size.x && event_pos2.y<data_size.y:
            var pixel: Color=countries.data.prov.get_pixelv(event_pos2)
            if pixel in countries.data.owners:
                var col: Color=countries.data.owners[pixel]
                if col in countries.data.highlighted_owners:
                    var c: Country=countries.countries[col]
                    message.text=c.attack_message
                    var pos:=(event as InputEventMouse).position
                    pos.x+=40
                    message.position=pos

                    if event.is_pressed() && c.attack_message!=c.defeat_message:
                        message.text=""
                        var country: Country=countries.countries[col]
                        start_game.emit(country.tag,campaign.ind+current_choice.choices.find(get_defeated(col)),col)

func rectify_choice(col: Color, player_won: bool) -> void:
    if player_won:
        annex_in_regions()
        annex_defeated(get_defeated(col))
        countries.data.end_highlight()
        var b: GameSupervisor=base.get_ref()
        map3d.hide()
        mesh.hide()
        message.hide()
        camera.hide()
        await b.execute_events(campaign.counter)
        map3d.show()
        mesh.show()
        message.show()
        camera.show()
        if get_next_encounter(): highlight_next()
    else:
        rectify_deathless_loss(get_defeated(col))

func get_defeated(col: Color) -> Campaign.Opponents:
    if current_choice.choices.size()==1:
        return current_choice.choices[0]
    for opponents in current_choice.choices:
        for opponent in opponents.opponents:
            if !opponent.is_state():
                var in_countries:=countries.get_from_region(opponent.col,opponent.level)
                if col in in_countries:
                    return opponents
    assert(false,"This should not have happened.")
    return null

func get_next_encounter() -> bool:
    var ch:=campaign.get_next_encounter()
    if ch.is_empty(): return false
    current_choice=ch[0]
    return true

func highlight_next() -> void:
    for opponents: Campaign.Opponents in current_choice.choices:
        var b: GameSupervisor=base.get_ref()
        if b.is_condition_true(opponents):
            for opponent: Campaign.Opponent in opponents.opponents:
                if !opponent.is_state():
                    var count:=countries.get_from_region(opponent.col,opponent.level)
                    for c in count:
                        countries.data.highlight_country(c)
    map3d.environment.time+=0.8
    map3d.darken_water()
    if current_choice.has_end():
        map3d.darkness.show()
        map3d.darkness.increase_density()
    else:
        map3d.darkness.hide()

#endregion

#region Annex

func annex_in_regions() -> void:
    for reg: Campaign.AnnexRegion in campaign.get_annex_goals():
        annex_in_region(reg)

func annex_in_region(reg: Campaign.AnnexRegion) -> void:
    var in_region:=countries.get_from_region(reg.col,reg.level).size()
    var subregs:=countries.get_subregions(reg.col,reg.level)
    if reg.time_left>0 && (in_region>reg.time_left || SRand.rand(10)>8):
        if reg.level!=1 && subregs.size()>1 && in_region>subregs.size():
            for c in subregs:
                annex_in_region(Campaign.AnnexRegion.AnnexRegion(c,reg.level-1,reg.time_left-subregs.size()+1))
        else:
            countries.annex_from_region(reg.col,reg.level,campaign.player)

func annex_defeated(defeated: Campaign.Opponents) -> void:
    for opponent in defeated.opponents:
        if !opponent.is_state():
            var cols:=countries.get_from_region(opponent.col,opponent.level)
            for colour in cols:
                var country: Country=countries.countries[colour]
                var b: GameSupervisor=base.get_ref()
                Util.discard(b.defeated_countries.push_back(country.name))
                countries.annex_country(campaign.player,colour,true)

func rectify_deathless_loss(lost_to: Campaign.Opponents) -> void:
    for opponent in lost_to.opponents:
        if !opponent.is_state():
            var cols:=countries.get_from_region(opponent.col,opponent.level)
            for colour in cols:
                var country: Country=countries.countries[colour]
                if !country.defeat_message.is_empty():
                    country.attack_message=country.defeat_message

func refresh() -> void:
    (mesh.material_override as ShaderMaterial).set_shader_parameter("img",ImageTexture.create_from_image(countries.data.map))

#endregion

#region Save

func save() -> Dictionary:
    var dict:={
        "countries": countries.save(),
        "campaign": campaign.save()
    }
    return dict

func load_data(dict: Dictionary) -> bool:
    var key:=Util.missing_key(dict,["countries","campaign"])
    if !key.is_empty():
        printerr("Loading Map data failed! It's missing '%s'!" % key[0])
        return false

    countries.data.end_highlight()
    var c: Dictionary=dict["countries"]; if !self.countries.load_data(c): return false
    var c2: Dictionary=dict["campaign"]; if !self.campaign.load_data(c2): return false
    var e:=campaign.get_current_encounter()
    if !e.is_empty():
        current_choice=e[0]
        map3d.reset()
        map3d.environment.time+=0.8*(campaign.counter-1)
        for __ in campaign.counter-1:
            map3d.darken_water()
        for i in campaign.counter-1:
            if campaign.inner[i].has_end():
                map3d.darkness.increase_density()
        if current_choice.has_end():
            map3d.darkness.show()
        refresh()
    highlight_next()
    return true

#endregion

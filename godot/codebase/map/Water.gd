class_name Water
extends Node3D

#region Init

var water: MeshInstance3D=MeshInstance3D.new()

func _ready() -> void:
    add_child(water)

func initialise(path: String) -> void:
    water.mesh=PlaneMesh.new()

    var shader:=ShaderMaterial.new()
    shader.shader=Util.load_with_fallback("water/water.gdshader", path, "res://imports/maps/iberia/")
    water.material_override=shader

    var script := GDScript.new()
    script.set_source_code(Util.read_with_fallback("water/defaults.dat",path,"res://imports/maps/iberia/"))
    Util.discard(script.reload())
    Util.discard(script.call("init",water))

#endregion

#region Darken

func darken_water(col_decr : float = 0.12, norm_decr : float = 0.03, rough_incr : float = 0.03) -> void:
    var col: Color=water.material_override.get("shader_parameter/water_colour")
    water.material_override.set("shader_parameter/water_colour",col.darkened(col_decr))
    var norm: float=water.material_override.get("shader_parameter/normal_multiplier")
    water.material_override.set("shader_parameter/normal_multiplier",norm-norm_decr)
    var rough: float=water.material_override.get("shader_parameter/roughness")
    water.material_override.set("shader_parameter/roughness",rough+rough_incr)

func lighten_water(col_incr : float = 0.12, norm_incr : float = 0.03, rough_decr : float = 0.03) -> void:
    var col: Color=water.material_override.get("shader_parameter/water_colour")
    water.material_override.set("shader_parameter/water_colour",col.lightened(col_incr))
    var norm: float=water.material_override.get("shader_parameter/normal_multiplier")
    water.material_override.set("shader_parameter/normal_multiplier",norm+norm_incr)
    var rough: float=water.material_override.get("shader_parameter/roughness")
    water.material_override.set("shader_parameter/roughness",rough-rough_decr)

#endregion

#region Set

func set_water_colour(col: Color) -> void:
    water.material_override.set("shader_parameter/water_colour",col)

func set_normal_multiplier(norm: float) -> void:
    water.material_override.set("shader_parameter/normal_multiplier",norm)

func set_roughness(rough: float) -> void:
    water.material_override.set("shader_parameter/roughness",rough)

#endregion

#region Get

func get_water_colour() -> Color:
    return water.material_override.get("shader_parameter/water_colour")

func get_normal_multiplier() -> float:
    return water.material_override.get("shader_parameter/normal_multiplier")

func get_roughness() -> float:
    return water.material_override.get("shader_parameter/roughness")

#endregion

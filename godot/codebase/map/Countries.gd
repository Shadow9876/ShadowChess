class_name Countries
extends Node

#region Properties

@export var path: String : 
	set(val):
		if val!=path:
			path=val
			data.path=path
			generate_countries()

## [codeblock]countries: Dictionary[Color, Country][/codeblock]
var countries: Dictionary={}

var data: MapData=MapData.new()

#endregion

#region Ready

func _ready() -> void:
	add_child(data)
	data.path=path
	generate_countries()

func generate_countries() -> void:
	countries.clear()
	var s:=Util.read_or_crash(path+"data/countries.dat")
	var macros:={}
	for line: String in s.split("\n"):
		var packed:=line.split(": ")
		if packed.size()>=2: macros[packed[0]]=": ".join(packed.slice(1))
	for line: String in s.split("\n"):
		for m: String in macros:
			var r: String=macros[m]
			if m in line: line=line.replace(m,r)
		var packed:=line.split("  ")
		if packed.size()<2: return
		var col:=Color.from_string(packed[1],Color.BLACK)
		var country:=Country.construct(col,Tag.Tag(packed[0]),packed[2],data.owners,packed.slice(3))
		countries[col]=country
		determine_attrs(country)

func determine_attrs(country: Country) -> void:
	if country.provinces.is_empty(): return
	var p:=country.provinces[0]
	Util.discard(country.attr.push_back(country.colour))
	for d: Dictionary in data.attr:
		var col: Color=d[p]
		Util.discard(country.attr.push_back(col))

#endregion

#region Annex

func get_country(tag: String) -> Array[Country]:
	for key: Color in countries:
		var val: Country=countries[key]
		if val.tag.inner==tag:
			return [val]
	return []

func annex_country(idx1: Color, idx2: Color, scripted: bool = false) -> void:
	if !scripted && countries[idx2].strength>countries[idx1].strength:
		var tmp:=idx2; idx2=idx1; idx1=tmp
	var winner:=idx1 if scripted || countries[idx1].strength*SRand.rand2(3,8)/5>=countries[idx2].strength*SRand.rand2(4,7)/5 else idx2
	var loser:=idx2 if winner==idx1 else idx1
	var l_country: Country=countries[loser]
	var w_country: Country=countries[winner]
	if w_country.attr.is_empty(): w_country.attr=l_country.attr
	data.annex_country(l_country.colour,w_country.colour)
	w_country.annex_country(l_country)
	var __:=countries.erase(loser)

func annex_from_region(col: Color, level: int, player: Color) -> void:
	if level==0: return
	var in_countries:=get_from_region(col,level)
	if player in in_countries:
		in_countries.remove_at(in_countries.find(player))
	if in_countries.size()<2: return
	var r:=SRand.rand(in_countries.size())
	var c1:=in_countries[r]
	in_countries.remove_at(r)
	var union:=Util.union(in_countries,get_neighbours(c1))
	if union.is_empty(): return
	r=0 if union.size()<2 else SRand.rand(union.size())
	var c2:=union[r]
	return annex_country(c1,c2)

#endregion

#region Queries

func get_neighbours(col: Color) -> PackedColorArray:
	var neighbours: PackedColorArray=[]
	var country: Country = countries[col]
	for prov in country.provinces:
		var connections: Array=data.connections[prov]
		for con: Color in connections:
			var n: Color=data.owners[con]
			if n not in neighbours:
				Util.discard(neighbours.push_back(n))
	neighbours.sort()
	return neighbours

func get_from_region(col: Color, level: int = 0) -> PackedColorArray:
	var out: PackedColorArray=[]
	for c_col: Color in countries:
		var country: Country=countries[c_col]
		if !country.attr.is_empty() && country.attr[level]==col:
			Util.discard(out.push_back(country.colour))
	out.sort()
	return out

func get_subregions(col: Color, level: int) -> PackedColorArray:
	if level<=1: return []
	var out: PackedColorArray=[]
	var in_countries:=get_from_region(col,level)
	for col2 in in_countries:
		var country: Country=countries[col2]
		var c:=country.attr[level-1]
		if c not in out:
			Util.discard(out.push_back(c))
	out.sort()
	return out

#endregion

#region Save

func save() -> Dictionary:
	var dict:={
		"data": data.save()
	}
	var c:={}
	for col: Color in countries:
		var country: Country=countries[col]
		c[col.to_html(false)]=country.save()
	dict["countries"]=c
	return dict

func load_data(dict: Dictionary) -> bool:
	var key:=Util.missing_key(dict,["data","countries"])
	if !key.is_empty():
		printerr("Loading Countries failed! They're missing '%s'!" % key[0])
		return false
	
	self.countries.clear()
	var d: Dictionary=dict["data"]
	if !self.data.load_data(d): return false
	var c: Dictionary=dict["countries"]
	for k: String in c:
		var val: Dictionary=c[k]
		var opt_country:=Country.load_data(val)
		if opt_country.is_empty(): return false
		self.countries[Color.from_string(k,Color.BLACK)]=opt_country[0]
	return true

#endregion

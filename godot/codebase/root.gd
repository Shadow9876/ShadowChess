class_name Root
extends Node

func _init() -> void:
	# Loading cursor
	var cursor : Image
	match OS.get_name():
		"Windows":
			cursor=preload("res://imports/ui/cursors/cursor-windows.svg").get_image()
		"macOS":
			cursor=preload("res://imports/ui/cursors/cursor-macos.svg").get_image()
		"Linux", "FreeBSD", "NetBSD", "OpenBSD", "BSD":
			cursor=preload("res://imports/ui/cursors/cursor-linux.svg").get_image()
		"Android":
			cursor=preload("res://imports/ui/cursors/cursor-android.svg").get_image()
		"iOS":
			cursor=preload("res://imports/ui/cursors/cursor-ios.svg").get_image()
		"Web":
			cursor=preload("res://imports/ui/cursors/cursor-web.svg").get_image()
	cursor.resize(32,32,Image.INTERPOLATE_LANCZOS)
	Input.set_custom_mouse_cursor(ImageTexture.create_from_image(cursor),Input.CursorShape.CURSOR_ARROW,Vector2(16,16))

var main_menu: MainMenu=preload("res://codebase/main_menu/main_menu.tscn").instantiate()
var game: GameSupervisor=null
var music:=Music.new()
static var scroll_sensitivity:=5
static var played_animations: PackedStringArray=[]
static var environmental_animations: Glib.EnvironmentalAnimation=Glib.EnvironmentalAnimation.ONLYONCE
static var turn_indicator_on_lefthand_side: bool=true

func _ready() -> void:
	DisplayServer.window_set_min_size(Vector2i(800,600))
	add_child(music)
	back_to_menu()
	Util.err_handle(main_menu.load_game_signal.connect(load_and_launch_game))
	Util.err_handle(main_menu.new_game_signal.connect(launch_game))
	Util.err_handle(main_menu.set_music_volume.connect(func (val: int) -> void: music.set_volume_percentage(val)))
	main_menu.select_animation_item(int(environmental_animations))
	load_user_prefs()
	if environmental_animations==Glib.EnvironmentalAnimation.ONCESTART: played_animations.clear()

func remove_all_children() -> void:
	if game!=null && is_instance_valid(game) && game in get_children():
		disconnect_game()
		remove_child(game)
		if game.chessGame!=null && is_instance_valid(game.chessGame) && game.chessGame not in game.get_children():
			game.add_child(game.chessGame)
		if game.map!=null && is_instance_valid(game.map) && game.map not in game.get_children():
			game.add_child(game.map)
		game.queue_free()
		game=null
	if main_menu in get_children():
		remove_child(main_menu)

func safe_back_to_menu() -> void:
	if game==null || !is_instance_valid(game) || game.map!=null && game.map in game.get_children() && !game.interaction.visible: return back_to_menu()
	var warn:=AcceptDialog.new()
	warn.title="SERIOUS WARNING" if game.death_on_loss else "WARNING"
	warn.dialog_text="Warning! All progress will be lost! Proceed?" if game.death_on_loss else "Warning! Progress of the current game will be lost! Proceed?"
	warn.ok_button_text="Proceed"
	var b:=warn.add_cancel_button("Cancel")
	b.text="Cancel"
	add_child(warn)
	warn.popup_centered()
	Util.err_handle(warn.confirmed.connect(func() -> void: back_to_menu(); remove_child(warn); warn.queue_free()))
	Util.err_handle(warn.canceled.connect(func() -> void: remove_child(warn); warn.queue_free()))

func back_to_menu() -> void:
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	var apply:=main_menu not in get_children()
	if main_menu not in get_children():
		music.fadein("siesta")
	remove_all_children()
	add_child(main_menu)
	main_menu.reset(apply)
	set_continue_button()

func set_continue_button() -> void:
	var data:=Util.try_read("user://save.json")
	if data.is_empty() || data[0].is_empty():
		main_menu.disable_continue()
	else:
		main_menu.enable_continue()

func _new_game(game_name: String) -> void:
	if environmental_animations==Glib.EnvironmentalAnimation.ONCEGAME: played_animations.clear()
	music.fadeout()
	remove_all_children()
	game=GameSupervisor.new()
	game.story_name=game_name
	add_child(game)

func launch_game(game_name: String) -> void:
	_new_game(game_name)
	connect_game()

func load_and_launch_game() -> void:
	_new_game("test")
	if !load_game():
		back_to_menu()
	else:
		connect_game()

func connect_game() -> void:
	Util.err_handle(game.save_game.connect(save_game))
	Util.err_handle(game.CampaignLost.connect(back_to_menu))
	Util.err_handle(game.delete_save.connect(delete_save))
	Util.err_handle(game.play_music.connect(music.stream_playlist))
	Util.err_handle(game.stop_music.connect(music.fadeout))

func disconnect_game() -> void:
	game.save_game.disconnect(save_game)
	game.CampaignLost.disconnect(back_to_menu)
	game.delete_save.disconnect(delete_save)
	game.play_music.disconnect(music.stream_playlist)
	game.stop_music.disconnect(music.fadeout)

func save_game() -> void:
	Util.write_to_file(game.save(),"user://save.json")

func delete_save() -> void:
	Util.write_to_file("","user://save.json")

## Save User preferences[br]
## [br]
## Should not be used for anything else than saving user preferences and 
## data about unlocked cosmetics.
func save_user_prefs() -> void:
	var dict:={
		"scroll_sensitivity": scroll_sensitivity,
		"played_animations": played_animations,
		"music_volume": music.volume_percentage,
		"environmental_animations": environmental_animations as int,
		"turn_indicator_position": turn_indicator_on_lefthand_side
	}
	Util.write_to_file(JSON.stringify(dict),"user://user_prefs.json")

func load_user_prefs() -> void:
	var data:=Util.try_read("user://user_prefs.json")
	if data.is_empty(): return
	var dict: Dictionary=JSON.parse_string(data[0])

	var key:=Util.missing_key(dict,["scroll_sensitivity","played_animations","music_volume","environmental_animations","turn_indicator_position"])
	if !key.is_empty():
		printerr("Couldn't load user preferences! It's missing '%s'!" % key[0])
		return
	
	var s: float=dict["scroll_sensitivity"]; main_menu.adjust_scrolling_slider(s)
	played_animations=dict["played_animations"]
	var m: float=dict["music_volume"]; main_menu.adjust_music_slider(m)
	var e: float=dict["environmental_animations"]; main_menu.select_animation_item(int(e))
	var b: bool=dict["turn_indicator_position"]; main_menu.set_turn_indicator_checkbutton(b)

func load_game() -> bool:
	var data:=Util.try_read("user://save.json")
	if data.is_empty() || data[0].is_empty(): return false
	return game.load_data(data[0])

func _input(event: InputEvent) -> void:
	if event.is_action_pressed("toggle_fullscreen"):
		if DisplayServer.window_get_mode()!=DisplayServer.WINDOW_MODE_FULLSCREEN:
			DisplayServer.window_set_mode(DisplayServer.WINDOW_MODE_FULLSCREEN)
		else:
			DisplayServer.window_set_mode(DisplayServer.WINDOW_MODE_MAXIMIZED)
	if event.is_action_pressed("escape"):
		safe_back_to_menu()
